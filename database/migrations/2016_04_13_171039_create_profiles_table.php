<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->longText('cpanel_address')->nullable();
            $table->longText('cpanel_username')->nullable();
            $table->longText('cpanel_password')->nullable();
            $table->longText('ftp_address')->nullable();
            $table->longText('ftp_user')->nullable();
            $table->longText('ftp_password')->nullable();
            $table->longText('wp_admin')->nullable();
            $table->longText('wp_password')->nullable();
            $table->string('about')->nullable();
            $table->string('description')->nullable();            
            $table->string('topic')->nullable();
            $table->longText('email_login_link')->nullable();
            $table->longText('email')->nullable();
            $table->longText('email_password')->nullable();
            $table->string('avatar')->nullable();
            $table->longText('main_username')->nullable();
            $table->longText('main_password')->nullable();
            $table->string('first_name')->nullable();
            $table->string('middle_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('gender')->nullable();
            $table->timestamp('birthday')->nullable();
            $table->string('phone')->nullable();
            $table->string('city_address')->nullable();
            $table->string('tx')->nullable();
            $table->string('zip')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('profiles');
    }
}
