<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUrlsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('urls', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url');
            $table->string('ip');
            $table->integer('host_id')->nullable();
            $table->string('registrar')->nullable();
            $table->timestamp('update_date')->nullable();
            $table->timestamp('create_date')->nullable();
            $table->string('registrant_name')->nullable();
            $table->string('registrant_organization')->nullable();
            $table->string('registrant_street')->nullable();
            $table->string('registrant_city')->nullable();
            $table->string('registrant_state')->nullable();
            $table->string('registrant_zip')->nullable();
            $table->string('registrant_phone')->nullable();
            $table->string('registrant_email')->nullable();
            $table->string('registrant_name_server_1')->nullable();
            $table->string('registrant_name_server_2')->nullable();
            $table->longText('cpanel_login_url')->nullable();
            $table->longText('cpanel_username')->nullable();
            $table->longText('cpanel_password')->nullable();
            $table->longText('ftp_login_address')->nullable();
            $table->longText('ftp_username')->nullable();
            $table->longText('ftp_password')->nullable();
            $table->longText('wp_admin_username')->nullable();
            $table->longText('wp_admin_password')->nullable();
            $table->longText('notes')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('urls');
    }
}
