<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfileSocialAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile_social_accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('profile_id');
            $table->integer('social_network_id');
            $table->longText('username');
            $table->longText('password');
            $table->longText('profile_url')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('profile_social_accounts');
    }
}
