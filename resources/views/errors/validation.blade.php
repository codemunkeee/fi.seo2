@if (isset($errors))
	@if (count($errors) > 0)
	<div class="alert alert-danger alert-dismissable">
    	<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
    	Something went wrong!
    	<hr>
    	<ul>
			@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
    </div>
	@endif
@endif

