   <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/">Fire&IceTech</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                @if(\Auth::check())
                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> {{ \Auth::user()->first_name }} {{ \Auth::user()->last_name }}<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>                            
                            <a href="/users/{{ \Auth::user()->id }}"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li>
                            <a href="/users/{{ \Auth::user()->id }}/edit"><i class="fa fa-fw fa-edit"></i> Edit</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="/auth/logout"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
                @else
                    <li class="dropdown">
                        <a href="/auth/login" class="dropdown-toggle"><i class="fa fa-male"></i>   Login</a>
                    </li>
                @endif
            </ul>
            @if(\Auth::check())
                @if(\Auth::user()->hasRole('admin'))
                    <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                    <div class="collapse navbar-collapse navbar-ex1-collapse">
                        <ul class="nav navbar-nav side-nav">
                            <li><a href="/users"><i class="fa fa-fw fa-dashboard"></i>Users</a></li>
                            <li><a href="/roles"><i class="fa fa-fw fa-dashboard"></i>Roles</a></li>
                            <li><a href="/permissions"><i class="fa fa-fw fa-dashboard"></i>Permissions</a></li>
                        </ul>
                    </div>
                @else
                    <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                    <div class="collapse navbar-collapse navbar-ex1-collapse">
                        <ul class="nav navbar-nav side-nav">
                            <li><a href="/urls" ><i class="fa fa-fw fa-dashboard"></i>URLs</a></li>
                            <li><a href="/hosting" ><i class="fa fa-fw fa-dashboard"></i>Hosting</a></li>
                            <li><a href="/whois" ><i class="fa fa-fw fa-dashboard"></i>Whois</a></li>
                            <li><a href="/personas" ><i class="fa fa-fw fa-dashboard"></i>Personas</a></li> 
                            <li><a href="/social_accounts" ><i class="fa fa-fw fa-dashboard"></i>Social Accounts</a></li>                                     
                        </ul>
                    </div>                    
                @endif
            @endif

            <!-- /.navbar-collapse -->
        </nav>