@if(!\Auth::guest())
<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                    <span>
                        @if (\Auth::user()->avatar)
                            <img alt="image" class="img-circle" src="{{ (\Auth::user()->avatar) }}" height="48" width="48"/>
                        @else
                            <img alt="image" class="img-circle" src="{{ \Gravatar::src(\Auth::user()->email) }}" height="48" width="48"/>
                        @endif
                    </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                    <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">{{ (\Auth::user()->first_name) }} {{ \Auth::user()->last_name }}</strong>
                     </span> <span class="text-muted text-xs block">
                     @if(count(\Auth::user()->roles()->get()) == 1)
                        {{ \Auth::user()->roles()->first()->name }}   
                     @else
                         @foreach(\Auth::user()->roles()->get() as $role)
                            {{ $role->name }} 
                         @endforeach
                     @endif

                    <b class="caret"></b></span> </span> </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a href="/users/{{ \Auth::user()->id }}">Profile</a></li>
                        <li><a href="mailbox.html">Mailbox</a></li>
                        <li class="divider"></li>
                        <li><a href="/auth/logout">Logout</a></li>
                    </ul>
                </div>
                <div class="logo-element">
                    FI
                </div>
            </li>
            @if(\Auth::user()->hasRole('admin'))
                <li>
                    <a href="/"><i class="fa fa-th-large"></i> <span class="nav-label">Admin</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li><a href="/users">Users</a></li>
                        <li><a href="/permissions">Permissions</a></li>
                        <li><a href="/roles">Roles</a></li>
                    </ul>
                </li>                  
            @endif

            <li>
                <a href="/"><i class="fa fa-th-large"></i> <span class="nav-label">Menu</span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li class="active"><a href="/urls">Urls</a></li>
                    <li><a href="/hosts">Hosts</a></li>
                    <li><a href="/whois">Whois</a></li>
                    <li><a href="/profiles">Profiles</a></li>
                    <li><a href="/social_networks">Social Networks</a></li>
                    <li><a href="/social_accounts">Social Accounts</a></li>                    
                </ul>
            </li>
      
        </ul>

    </div>
</nav>
@else

<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element"> 
                    <span>
                        <img alt="image" class="img-circle" src="/assets/img/users/default.jpg" height="48" width="48"/>
                     </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                    <span class="clear"> <span class="block m-t-xs"> 
                    <strong class="font-bold">Guest</strong>
                     </span> <span class="text-muted text-xs block">
                        Welcome, Guest!

                     <b class="caret"></b></span> </span> </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a href="/auth/login">Login</a></li>
                    </ul>
                </div>
                <div class="logo-element">
                    FI
                </div>
            </li>
        </ul>
    </div>
</nav>
@endif