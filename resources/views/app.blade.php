<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">    
		<meta name="author" content="Fire&Ice Tech">
		@yield('title')
	    @yield('meta-tags')
	    
		<title>Fire&IceTech</title>	
	    
	    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
	    <link href="/assets/font-awesome/css/font-awesome.css" rel="stylesheet">

	    <!-- Toastr style -->
	    <link href="/assets/css/plugins/toastr/toastr.min.css" rel="stylesheet">

	    <!-- Gritter -->
<!-- 	<link href="/assets/js/plugins/gritter/jquery.gritter.css" rel="stylesheet"> -->

	    <link href="/assets/css/animate.css" rel="stylesheet">
	    <link href="/assets/css/style.css" rel="stylesheet">

	    <script src="/assets/js/jquery-2.1.1.js"></script>
    	<script src="/assets/js/bootstrap.min.js"></script>

		@yield('header_scripts')

	</head>

	<body>
	 	<div id="wrapper">
	 			@include('core.header')
				@include('errors.validation')			
				@yield('content')
				</div>
			
			@include('core.footer')
			
		</div>

    <!-- Mainly scripts -->
    <script src="/assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="/assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="/assets/js/inspinia.js"></script>
    <script src="/assets/js/plugins/pace/pace.min.js"></script>

    <!-- Toastr -->
    <script src="/assets/js/plugins/toastr/toastr.min.js"></script>

    @yield('footer_scripts')
	</body>
</html>