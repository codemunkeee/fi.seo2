@extends('app')

@section('content')
	<div id="page-wrapper">


	<h1>SEO</h1>
	<br>

	Changelogs:	
	<ul>
		<li><a href="#v1.3">LATEST</a></li>
		<li><a href="#v1.2">v1.2</a></li>
		<li><a href="#v1.1">v1.1</a></li>
	</ul>
	
	
	<hr>
<br><br>
	
	<a name="v1.3"><p>v1.3 Changes:</p></a>
	<hr>
		<h4>API</h4>
	<hr>
		<p> Use the API to directly get data from the database in a json format. </p>
		<p> Filtering data : The API will EXACTLY find the parameters specified on the params </p>
		<p> Searching data : The API will FIND/SEARCH for a keyword based from the specified parameters</p>
		<p> Sorting data   : The API will sort the data on Ascending or Descending order </p>

		<ul>
			<li>To filter data : carlo.fireicetech.com/{table}/api/filter?ParamsHere</li>
			<li>To sort data by column : carlo.fireicetech.com/{table}/api/sort/{field}/{type}</li>
			<li>To search : carlo.fireicetech.com/{table}/api/search?ParamsHere</li>
		</ul>
		<p> WHERE : </p>
		<ul>
			<li>{table} = the table you want to search from, e.g, users, personas, etc </li>
			<li>{field} = the field that you want to search from from a table, e.g., users.first_name, users.id, etc</li>
			<li>{type} = the type of sort, either "Asc" or "Desc" (Ascending/Descending order) </li>
		</ul>

		<p> The Params </p>
		<p> Parameters determine which fields will be used to filter the data. </p>
		<p> Example : </p>
		<ul>
			<li>Filtering data : carlo.fireicetech.com/personas/api/filter?id=1&first_name=John</li>
			<li>Searching data : carlo.fireicetech.com/social_account/api/search?name=Google</li>
			<li>Sorting data : carlo.fireicetech.com/users/api/sort/id/asc</li>			
		</ul>
	<hr>
		<h4> Images</h4>
		<p> Images file names are now parsed in random.
	<hr>
	<hr>
	<hr>
	<hr>
	<hr>
	<hr>
	<hr>	
	<a name="v1.2"><p>v1.2 Changes:</p></a>

	<p>Linking Social Accounts to a Persona</p>

	<p>After adding a social account, the user can then link a social account to a persona.</p>
		<ul>
		<li>1. Navigate to carlo.fireicetech.com/personas then click on the 'edit' button of the selected persona.</li>
		<li>2. On the right side, there is a menu, select the 'Social Accounts' tab</li>
		<li>3. The social accounts will be added by the user by clicking the "Add More" button on the lower right of the screen. </li>
		<li>4. Select the social network to be linked from the select box.</li>
		<li>5. Provide the other details.</li>
		<li>6. click submit.</li>
		</ul>


	<hr>
	<a href="/permissions/"><h4>Permissions</h4></a>
	<hr>
	<p> Permissions are needed for the application for security purposes. Users will need to have permissions to access certain functionalities of the app. Take note that only the ADMIN can access this page. </p>

	<p>Permission's functionalities</p>
	<ul>
	<li>View list  : <a href="/permissions/">carlo.fireicetech.com/permissions</a></li>
	<li>Create/Add : <a href="/permissions/create">carlo.fireicetech.com/permissions/create</a></li>
	<li>Edit       : <a href="/permissions/">carlo.fireicetech.com/permissions</a> (search for the permission then click the edit button (yellow))</li>
	<li>Delete     : <a href="/permissions/">carlo.fireicetech.com/permissions</a> (search for the permission then click the delete button (red))</li>
	</ul>

	<hr>
	<a href="/roles/"><h4>Roles</h4></a>
	<hr>
	<p> Roles organize the users into groups. These groups are then given permissions, depending on the Admin. </p>

	<p>Role's functionalities</p>
	<ul>
	<li>View list  : <a href="/roles/">carlo.fireicetech.com/roles</a></li>
	<li>Create/Add : <a href="/roles/create">carlo.fireicetech.com/roles/create</a></li>
	<li>Edit       : <a href="/roles/">carlo.fireicetech.com/roles</a> (search for the role then click the edit button (yellow))</li>
	<li>Delete     : <a href="/roles/">carlo.fireicetech.com/roles</a> (search for the role then click the delete button (red))</li>
	</ul>

	<p>Linking Permissions to a Role</p>

	<p>It is super easy to link a permission to a role. Simply follow these steps. </p>
		<ul>
		<li>1. Navigate to carlo.fireicetech.com/roles then click on the 'edit' button of the selected role.</li>
		<li>2. On the right side, there is a menu, select the 'Permissions' tab</li>
		<li>3. There is a select box, select or type in the permissions that you wish to link this role.</li>
		<li>4. click submit.</li>
		</ul>

	<hr>
		<h4>API</h4>
	<hr>
		<p> Use the API to directly get data from the database in a json format. </p>
		
		<ul>
			<li>To get paginated data from a table, use : carlo.fireicetech.com/api/{table}</li>
			<li>To filter data : carlo.fireicetech.com/api/{table}/filter/{keyword}</li>
			<li>To sort data by column : carlo.fireicetech.com/api/{table}/sort/{column}/{type}</li>
			<li>To search : carlo.fireicetech.com/api/{table}/search/{keyword}</li>
		</ul>
		<p> WHERE : </p>
		<ul>
			<li>{table} = the table you want to search from, e.g, users, personas, etc </li>
			<li>{keyword} = the search term, e.g., 'test', 'Jasmine', 'Houston Texas' </li>
			<li>{column} = the field that you want to search from from a table, e.g., users.first_name, users.id, etc</li>						
			<li>{type} = the type of sort, either "Asc" or "Desc" (Ascending/Descending order) </li>
		</ul>
	<hr>
	<hr>
	<hr>
	<hr>
	<hr>
	<hr>		
	<a name="v1.1"><p>v1.1 Changes:</p></a>

	<p>There are new features added to the app, the <b>Persona and Social Accounts</b>.</p>

	<hr>
	<a href="/personas/"><h4>Personas</h4></a>
	<hr>

	<p>A user can have multiple personas that he/she can add anytime. Please take note that the First name is required.</p>

	<p>Persona's functionalities</p>
	<ul>
	<li>View list  : <a href="/personas/">carlo.fireicetech.com/personas</a></li>
	<li>Create/Add : <a href="/personas/create">carlo.fireicetech.com/personas/create</a></li>
	<li>Edit       : <a href="/personas/">carlo.fireicetech.com/personas</a> (search for the persona then click the edit button (yellow))</li>
	<li>Delete     : <a href="/personas/">carlo.fireicetech.com/personas</a> (search for the persona then click the delete button (red))</li>
	</ul>
		
	<hr>
		<a href="/social_accounts/"><h4>Social Accounts</h4></a>
	<hr>	
		
	<p>These are the social accounts that will be linked to the Persona (E.g., Facebook, Twitter, Stackoverflow, etc.)</p>

	<p>Social accounts functionalities :</p>
	<ul>
		<li>View list  : <a href="/social_accounts/">carlo.fireicetech.com/social_accounts</a></li>
		<li>Create/Add : <a href="/social_accounts/create">carlo.fireicetech.com/social_accounts/create</a></li>
		<li>Edit       : <a href="/social_accounts/">carlo.fireicetech.com/social_accounts</a>(search for the social account then click the edit button (yellow))</li>
		<li>Delete     : <a href="/social_accounts/">carlo.fireicetech.com/social_accounts</a>(search for the social account then click the delete button (red))</li>
	</ul>

	<p>Linking Social Accounts to a Persona</p>

	<p>After adding a social account, the user can then link a social account to a persona.</p>
		<ul>
		<li>1. Navigate to carlo.fireicetech.com/personas then click on the 'edit' button of the selected persona.</li>
		<li>2. On the right side, there is a menu, select the 'Social Accounts' tab</li>
		<li>3. The social accounts are automatically listed for the user, just add the login credentials for each social account</li>
		<li>4. click submit.</li>
		</ul>

		<p>Linking Websites to a Persona</p>

		<p>After adding a URL (carlo.fireicetech.com/urls), the user can then link the website to a persona.</p>
		<ul>
		<li>1. Navigate to carlo.fireicetech.com/personas then click on the 'edit' button of the selected persona.</li>
		<li>2. On the right side, there is a menu, select the 'Websites' tab</li>
		<li>3. There is a select box, select or type in the websites that you wish to link this persona.</li>
		<li>4. click submit.</li>

		</ul>

	</div>


@endsection


	