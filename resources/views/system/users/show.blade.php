@extends('app')

@section('content')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Users</h2>
        <ol class="breadcrumb">
            <li><a href="/">Home</a></li>
            <li><a href="/users">Users</a></li>
            @if(isset($user))
                @if($method == 'edit')
                    <li><a href="/users/{{ $user->id}}">{{ $user->id}}</a></li>
                    <li class="active"><a href="/roles/{{ $user->id}}/edit"><strong>Edit</strong></a></li>
                @else                        
                    <li><a href="/users/{{ $user->id}}"><strong>{{ $user->id}}</strong></a></li>    
                    <a class="btn btn-warning btn-circle btn-outline pull-right" href="/users/{{ $user->id}}/edit"><i class="fa fa-edit"></i></a>
                        <!-- <a class="btn btn-danger btn-circle btn-outline pull-right" href="/users/{{ $user->id}}/destroy"><i class="fa fa-times"></i></a> -->
                @endif
            @endif
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
            <div class="row m-b-lg m-t-lg">
                <div class="col-md-6">

                    <div class="profile-image">
                        <img src="{{ $user->avatar }}" class="img-circle circle-border m-b-md" alt="profile">
                    </div>
                    <div class="profile-info">
                        <div class="">
                            <div>
                                <h2 class="no-margins">
                                    {{ $user->first_name }} {{ $user->last_name }}
                                </h2>
                                <h4>{{ $user->email }}</h4>
                                <small>
                                    Join Date : {{ $user->created_at }}
                                </small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <table class="table small m-b-xs">
                        <tbody>
                        <tr>
                            <td>
                                <strong>{{ $user->roles()->count() }}</strong> Roles
                            </td>
                            <td>
                                <strong>{{ $user->personas()->count() }}</strong> Personas
                            </td>

                        </tr>
                        <tr>
                            <td>
                                <strong>{{ $user->urls()->count() }}</strong> Urls
                            </td>
                            <td>
                                <strong>{{ $user->hostings()->count() }}</strong> Hosting
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-3">
                    <small>System Role</small>
                    <h2 class="no-margins">{{ $user->roles()->first()->name }}</h2>
                    <div id="sparkline1"><canvas width="247" height="50" style="display: inline-block; width: 247px; height: 50px; vertical-align: top;"></canvas></div>
                </div>


            </div>
            <div class="row">

                <div class="col-lg-3">

                </div>

                <div class="col-lg-5">
                        <!-- social div box here-->
                </div>

                <div class="col-lg-12 m-b-lg">

                </div>

            </div>

        </div>
        
@endsection