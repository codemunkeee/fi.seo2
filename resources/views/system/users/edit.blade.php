@extends('app')

@section('header_scripts')
    <link href="/assets/css/plugins/select2/select2.min.css" rel="stylesheet">
    <link href="/assets/css/plugins/dropzone/basic.css" rel="stylesheet">
    <link href="/assets/css/plugins/dropzone/dropzone.css" rel="stylesheet">

@endsection

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Users</h2>
        <ol class="breadcrumb">
            <li><a href="/">Home</a></li>
            <li><a href="/users">Users</a></li>
            @if(isset($user))
                @if($method == 'edit')
                    <li><a href="/users/{{ $user->id}}">{{ $user->id}}</a></li>
                    <li class="active"><a href="/roles/{{ $user->id}}/edit"><strong>Edit</strong></a></li>
                @else                        
                    <li><a href="/users/{{ $user->id}}"><strong>{{ $user->id}}</strong></a></li>    
                    <a class="btn btn-warning btn-circle btn-outline pull-right" href="/users/{{ $user->id}}/edit"><i class="fa fa-edit"></i></a>
                        <!-- <a class="btn btn-danger btn-circle btn-outline pull-right" href="/users/{{ $user->id}}/destroy"><i class="fa fa-times"></i></a> -->
                @endif
            @endif
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInUp">
    <div class="ibox">
        <div class="ibox-content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="m-b-md">
                        <a href="#" class="btn btn-white btn-xs pull-right">Edit User</a>
                        <h2>{{ $user->first_name }} {{ $user->last_name }}</h2>
                    </div>
                </div>
            </div>
            <div class="row">
<!--             'id' => 'my-awesome-dropzone', 'class'=> 'dropzone dz-clickable',  -->
                {!! Form::model($user,  ['id' => 'my-awesome-dropzone', 'class'=> 'dropzone dz-clickable', 'url'=>'/users/'.$user->id, 'files'=>'true', 'method'=>'PUT']) !!}
                    <div class="col-lg-2">
                    <div class="dropzone-previews"></div>
                    <div class="dz-message">asd</div>
                            
                        <!-- <dt>Image : </dt><dd>{!! Form::file('avatar', ['class'=>'form-control']) !!}</dd> -->        
                    </div>
                    <div class="col-lg-4">
                        <dl class="dl-horizontal">
                            <input type="hidden" value="{{ csrf_token() }}"/>
                            <dt>First Name: </dt><dd>{!! Form::text('first_name', null, ['class'=>'form-control']) !!}</dd>
                            <dt>Last Name:</dt> <dd>{!! Form::text('last_name',null, ['class'=>'form-control']) !!} </dd>
                        </dl>
                    </div>
                {!! Form::close() !!}

                <div class="col-lg-6" id="cluster_info">
                    <dl class="dl-horizontal">
                        <dt>Last Updated:</dt> <dd>{{ $user->updated_at }}</dd>
                        <dt>Created:</dt> <dd>{{ $user->created_at }}</dd>
                    </dl>
                </div>
            </div>                            
        </div>
    </div>
</div>
        
@endsection

@section('footer_scripts')  
    <script src="/assets/js/plugins/select2/select2.full.min.js"></script>
    <script type="text/javascript">
      $('#roles').select2();
    </script>
    <!-- DROPZONE -->
    <script src="/assets/js/plugins/dropzone/dropzone.js"></script>
    <script>
        $(document).ready(function(){

            Dropzone.options.myAwesomeDropzone = {
                // autoProcessQueue: false,
                // uploadMultiple: false,
                // parallelUploads: 100,
                //maxFiles: 100,
                maxFiles:1,
                init: function() {
                      this.on("maxfilesexceeded", function(file) {
                            this.removeAllFiles();
                            this.addFile(file);
                      });
                }  
                // Dropzone settings
                // init: function() {
                //     var myDropzone = this;

                //     this.element.querySelector("button[type=submit]").addEventListener("click", function(e) {
                //         e.preventDefault();
                //         e.stopPropagation();
                //         myDropzone.processQueue();
                //     });
                //     this.on("sendingmultiple", function() {
                //     });
                //     this.on("successmultiple", function(files, response) {
                //     });
                //     this.on("errormultiple", function(files, response) {
                //     });
                // }

            }

       });
    </script>
@endsection