<div ng-show="!isItemSet()">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>All Social Networks</h5>
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>
                <a class="close-link">
                    <i class="fa fa-times"></i>
                </a>
            </div>
        </div>
        <div class="ibox-content">
            <div>
                <div 
                    donut-chart
                    donut-data='chart'
                    donut-formatter="morrisChartFormatter">
                </div>
            </div>
            <div>
                <table class="table small m-b-xs">
                    <tbody>
                        <tr><td><strong>Total Social Accounts:</strong> {{ count($social_accounts) }} </td> </tr>                                    
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<div ng-show="isItemSet()">
    <div class="ibox">
        <div class="ibox-content">
            <div class="tab-content">
                <div class="tab-pane active">
                    <div class="row m-b-lg">
                        <div class="col-lg-12 text-center">
                            <div class="m-b-sm">
                                <div><i class="fa @{{ item_details.logo | lowercase}} fa-4x"></i></div>
                            </div>
                            <h2>@{{ item_details.name }}</h2>
                        </div>
                    </div>
                    <div>
                        <!-- SOCIAL_ACCOUNTS -->
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Social Accounts</h5>
                                <div class="ibox-tools">
                                    <a class="btn btn-default btn-circle collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content">         
                                <div class="ibox-content">
                                    <div id="vertical-timeline" class="vertical-container light-timeline no-margins">
                                        <div class="vertical-timeline-block">
                                            <div class="vertical-timeline-icon blue-bg">
                                                <i class="fa fa-user"></i>
                                            </div>
                                            <div class="vertical-timeline-content" >
                                                <div>
                                                    <div class="table-responsive" >
                                                        <table id="profiles_datatable" name="profiles_datatable" datatable="ng" dt-options="dtOptionsOnClickRightSide10" class="table table-hover">
                                                            <thead>
                                                                <tr>
                                                                    <th>Logo</th>
                                                                    <th>Username</th>
<!--                                                                     <th>Actions</th>  -->
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr ng-repeat="social_account in pivot.data['social_accounts'].items track by $index" ng-click="pivot.method.selected('social_accounts', social_account)" ng-class="{'current': pivot.method.isSelected('social_accounts', social_account) }">
                                                                    <td> <span class="@{{ item_details.logo }}"></span>
                                                                    </td>
                                                                    <td>
                                                                    <a href="@{{ social_account.profile_url}}" target="_blank">@{{ social_account.username }}</a></td>
 <!--                                                                    <td>
                                                                        <a type="button" class="btn btn-danger btn-circle btn-outline" data-toggle="modal" data-target="#profileDeleteModal"> <i class="fa fa-times" alt="Delete" ></i></a>                                                          
                                                                    </td> -->
                                                                </tr>
                                                            </tbody>
                                                           <tfoot>
                                                                <tr>
                                                                    <th>Logo</th>
                                                                    <th>Username</th>
<!--                                                                     <th>Actions</th>  -->
                                                                </tr>
                                                            </tfoot>                                                
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                        <!-- PROFILES -->
                        <div class="ibox float-e-margins collapsed">
                            <div class="ibox-title">
                                <h5>Profiles</h5>
                                <div class="ibox-tools">
                                    <a class="btn btn-default btn-circle collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content">         
                                <div class="ibox-content">
                                    <div id="vertical-timeline" class="vertical-container light-timeline no-margins">
                                        <div class="vertical-timeline-block">
                                            <div class="vertical-timeline-icon blue-bg">
                                                <i class="fa fa-user"></i>
                                            </div>
                                            <div class="vertical-timeline-content" >
                                                <div>
                                                    <div class="table-responsive" >
                                                        <table id="profiles_datatable" name="profiles_datatable" datatable="ng" dt-options="dtOptionsOnClickRightSide10" class="table table-hover">
                                                            <thead>
                                                                <tr>
                                                                    <th></th>
                                                                    <th>Name</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr ng-repeat="profile in pivot.data['profiles'].items track by $index" ng-click="pivot.method.selected('profiles', profile)" ng-class="{'current': pivot.method.isSelected('profiles', profile) }">
                                                                    <td class="client-avatar"><img alt="image" ng-src="@{{ profile.avatar || '/assets/img/users/default.jpg' }}">
                                                                    </td>
                                                                    <td><a>@{{ profile.first_name }} @{{ profile.middle_name }} @{{ profile.last_name }}</a></td>                                                      
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                           <tfoot>
                                                                <tr>
                                                                    <th></th>
                                                                    <th>Name</th>
                                                                </tr>
                                                            </tfoot>                                                
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

