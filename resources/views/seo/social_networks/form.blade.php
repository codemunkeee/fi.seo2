@extends('app')

@section('header_scripts')
    <link href="/assets/css/plugins/select2/select2.min.css" rel="stylesheet">
@endsection

@section('meta-tags')
    <meta name="keywords" content="{{$profile->first_name}} {{$profile->middle_name}} {{$profile->last_name}}, FireIceTech.com, Profile">
    <meta name="description" content="{{$profile->first_name}} {{$profile->middle_name}} {{$profile->last_name}}">
    
    <meta name="twitter:title" content="{{$profile->first_name}} {{$profile->middle_name}} {{$profile->last_name}} - Profile">
    <meta name="twitter:description" content="{{$profile->first_name}} {{$profile->middle_name}} {{$profile->last_name}} public profile.">

    <meta property="og:type" content="profile">
    <meta property="og:site_name" content="FireIceTech">
    <meta property="og:url" content="{{url()->current()}}">
    <meta property="og:title" content="{{$profile->first_name}} {{$profile->middle_name}} {{$profile->last_name}} - Profile">
    <meta property="og:description" content="{{$profile->first_name}} {{$profile->middle_name}} {{$profile->last_name}} public profile.">
    <meta property="og:image" content="{{$profile->avatar}}">

@endsection

@section('content')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Social Accounts</h2>
        <ol class="breadcrumb">
            <li><a href="/">Home</a></li>
            <li><a href="/social_accounts">Social Accounts</a></li>
            @if(isset($social_account))
                @if($method == 'edit')
                    <li><a href="/social_accounts/{{ $social_account->id}}">{{ $social_account->id}}</a></li>
                    <li class="active"><a href="/social_accounts/{{ $social_account->id}}/edit"><strong>Edit</strong></a></li>
                @else                        
                    <li><a href="/social_accounts/{{ $social_account->id}}"><strong>{{ $social_account->id}}</strong></a></li>    
                    <a class="btn btn-warning btn-circle btn-outline pull-right" href="/social_accounts/{{ $social_account->id}}/edit"><i class="fa fa-edit"></i></a>
                @endif
                <a type="button" class="btn btn-danger btn-circle btn-outline pull-right" data-toggle="modal" data-target="#deleteModal"> <i class="fa fa-trash" alt="Delete" ></i></a>
            @endif
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row m-b-lg m-t-lg">
        <div class="col-md-6">
            <div class="profile-info">
                <div class="">
                    <div>
                        <h2 class="no-margins">
                        @if ($method != 'create')
                            {{ $social_account->name }}
                        @else
                        	Create a new Social Account
                        @endif
                        </h2>
                        <small>
                        @if ($method != 'create')
                            Date Created: {{ $social_account->created_at }}
                        @endif
                        </small>
                    </div>
                </div>
            </div>
        </div>
    </div>

  
	<div class="row">
		@include('seo.social_accounts.form_list')
    </div>
	
</div>

	@if ($method != 'create')
	    <div class="modal inmodal fade" id="deleteModal" tabindex="-1" role="dialog"  aria-hidden="true">
	        <div class="modal-dialog modal-sm">
	            <div class="modal-content">
	                <div class="modal-header">
	                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	                    <h4 class="modal-title">Are you sure you want to delete this?</h4>
	                </div>
	                <div class="modal-body">
	                    <p>Once deleted, it will never be retrieved again.<strong> Are you really sure about this?</strong> </p>
	                </div>
	                <div class="modal-footer">
	                    {!! Form::open(['url'=>'social_accounts/'.$social_account->id, 'method'=>'DELETE']) !!}
	                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
	                    	<button type="button" class="btn btn-white" data-dismiss="modal">Close</button> 
	                    	<button type="submit" class="btn btn-danger">Delete</button>
	                    {!! Form::close() !!}   
	                </div>
	            </div>
	        </div>
	    </div>   
    @endif
@endsection

@section('footer_scripts')
    <script src="/assets/js/plugins/select2/select2.full.min.js"></script>

	<script type="text/javascript">
	  	$('#social_accounts').select2();
	</script>


@endsection