        <div class="col-lg-6 m-b-lg">
	            <div id="vertical-timeline" class="vertical-container light-timeline no-margins">

	                <div class="vertical-timeline-block">
	                    <div class="vertical-timeline-icon blue-bg">
	                        <i class="fa fa-file-text"></i>
	                    </div>

	                    <div class="vertical-timeline-content">
	                    <div>
							@if ($method == 'create')
								{!! Form::open(['url'=>'/social_accounts'  , 'method'=>'POST']) !!}
									<div class="form-group"><label>Social Accounts</label>
									{{ Form::select('social_accounts[]', $social_accounts, $user_social_accounts, ['id' => 'social_accounts', 'class' => 'form-control', 'multiple' => 'multiple', $disabled]) }}</div>

							@elseif ($method == 'edit' || $method == 'show')
								{!! Form::model($social_account,  ['url'=>'/social_accounts/' . $social_account->id, 'method'=>'PUT']) !!}

								<div class="form-group"><label>Name: </label>{!! Form::text('name', null, ['placeholder' => 'Social Account Name (Facebook, Twitter, etc)', 'class' => 'form-control', $disabled]) !!}</div>

							@endif                                                                       
	                            <div>
	                            <button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit" {{$disabled}}><strong>Submit</strong></button>
	                            </div>
	                        {!! Form::close() !!}
	                    </div>
			            
			            @if ($method != 'create')
		                    <div>
								<span class="vertical-date">
		                        	<small>Date Created: {{ $social_account->created_at }}</small>
		                        </span>
		                        <br>
								<span class="vertical-date">
		                        	<small>Date Updated: {{ $social_account->updated_at }}</small>
		                        </span>
		                    </div>
	                    @endif
	                </div>
	            </div>
	        </div>  
	    </div>