<div class="modal inmodal" id="socialAccountModal"  role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-md">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <i class="fa fa-clock-o modal-icon"></i>
                <h4 class="modal-title">@{{ pivot.data['social_accounts'].selected.name }} </h4>


            </div>
            <div class="modal-body">
                    <label>Username:</label>
                    <input id="username" name="username" type="text" class="form-control" ng-model="pivot.data['social_accounts'].selected.pivot.username" />
                    <label>Password:</label>
                    <input id="password" name="password" type="text" class="form-control" ng-model="pivot.data['social_accounts'].selected.pivot.password" />
                    <label>Profile URL: </label>
                    <input id="profile_url" name="profile_url" type="text" class="form-control" ng-model="pivot.data['social_accounts'].selected.pivot.profile_url" />
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning">Edit</button>
                <button type="button" class="btn btn-danger">Delete</button> 
                <button type="button" class="btn btn-primary" data-dismiss="modal">Save</button>
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
