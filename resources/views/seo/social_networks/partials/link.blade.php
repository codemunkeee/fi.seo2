<label>Select Social Networks</label>
<select name="profile_social_accounts_select" id="profile_social_accounts_select" ng-model="pivot.data['profile_social_accounts'].multiple.selected" class="form-control" placeholder="Select a URL" multiple>
    <option ng-repeat="social_account in pivot.data['profile_social_accounts'].multiple.unselected" value="@{{social_account.id}}" >@{{social_account.name}}</option>
</select>
