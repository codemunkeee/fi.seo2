<div class="modal inmodal" id="socialAccountModal"  role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-md">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <i class="fa fa-clock-o modal-icon"></i>
                <h4 class="modal-title">@{{ pivot.data['social_accounts'].selected.name }} </h4>
            </div>
            <div class="modal-body">
                @include('seo.social_networks.partials.edit')
            </div>
            <div class="modal-footer">
<!--                 <button type="button" class="btn btn-warning">Edit</button> -->
                <button type="button" class="btn btn-danger" ng-click="pivot.method.delete('profile_social_accounts', pivot.data['profile_social_accounts'].selected.pivot)" data-dismiss="modal">Delete</button> 
                <button type="button" class="btn btn-primary" ng-click="pivot.method.update('profile_social_accounts', pivot.data['profile_social_accounts'].selected)" data-dismiss="modal">Save</button>
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
