<div class="modal inmodal" id="socialAccountCreateModal"  role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-md">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <i class="fa fa-clock-o modal-icon"></i>
                <h4 class="modal-title">Create New Social Account </h4>
            </div>
            <div class="modal-body">
                @include('seo.social_networks.partials.create')
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" ng-click="link(new_item)" data-dismiss="modal">Save</button>
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
