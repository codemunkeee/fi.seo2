@extends('app')

@section('header_scripts')
    <link href="/assets/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
    <link href="/assets/css/plugins/select2/select2.min.css" rel="stylesheet">

    <link href="/assets/css/plugins/summernote/summernote2.min.css" rel="stylesheet">
    <link href="/assets/css/fi/social_networks/social_networks.css" rel="stylesheet">

<!--     <link href="http://webapplayers.com/inspinia_admin/css/plugins/datapicker/datepicker3.css" rel="stylesheet" -->
    <link href="/assets/fi/social-networks/fi_social_networks.css" rel="stylesheet">

    <script src="/assets/js/plugins/clipboard/clipboard.min.js"></script>
    <script src="/assets/js/plugins/dataTables/datatables.min.js"></script>
    <script src="/assets/js/angularjs/angular.min.js"></script>
    <script src="/assets/js/angularjs/angular-sanitize.min.js"></script>
    <script src="/assets/js/angularjs/angular-animate2.min.js"></script>
    <script src="/assets/js/angularjs/oclazyload/oclazyload.min.js"></script>
    <script src="/assets/js/angularjs/select/select.min.js"></script>
    <script src="/assets/js/angularjs/toastr/toastr.min.js"></script>
    <script src="/assets/js/angularjs/ui/ui-bootstrap.min.js"></script>
    <script src="/assets/js/angularjs/clipboard/ngclipboard.min.js"></script>
    <script src="/assets/js/angularjs/datatables/angular-datatables.min.js"></script>
    <script src="/assets/js/angularjs/datatables/angular-datatables.buttons.min.js"></script>
    <script src="/assets/js/angularjs/datatables/angular-datatables.tabletools.min.js"></script>    
    <script src="/assets/js/plugins/raphael/raphael.min.js"></script>    
    <script src="/assets/js/plugins/morris/morris2.min.js"></script>
    <script src="/assets/js/angularjs/morris/morris.min.js"></script>
    <script src="/assets/js/plugins/summernote/summernote2.min.js"></script>
    <script src="/assets/js/angularjs/summernote/summernote.min.js"></script>
<!--     <script src="https://rawgit.com/eight04/angular-datetime/master/dist/datetime.js"></script> -->

    <script src="/assets/js/angularjs/fileupload/ng-file-upload.min.js"></script>
    <script src="/assets/js/angularjs/fileupload/ng-file-upload-shim.min.js"></script>

    <script src="/assets/js/angularjs/common/main.js"></script>
    <script src="/assets/js/angularjs/common/items.js"></script>
    
    <script> angular.module("FI").constant("CSRF_TOKEN", '{{ csrf_token() }}'); </script>
    
    <style>
        .current {
            background-color: #cce7e2;
        }
        .select2-container {
           z-index: 2050;
        } 
        .trans {
          transition:0.5s;
        }
        .hide-div {
            display: none;
        }
        .show-div {
            display:block;
        }

    </style>
@endsection

@section('content')
    <div ng-app="FI" ng-controller="MainCtrl">
    <input type="hidden" ng-init="user_id={{ \Auth::user()->id }};pivot_api={{ json_encode($pivot_api)}};morrisChart={{ json_encode($profile_group) }}"/>
    
<!--     <button class="btn btn-primary" ng-click="test()">test</button> -->
    <div ng-if="is_loading">
     <div id ="center" style="position:fixed;top:50%;left:50%;z-index:99999">    
        <div class="sk-spinner sk-spinner-wandering-cubes">
            <div class="sk-cube1"></div>
            <div class="sk-cube2"></div>
        </div>
     </div>
    </div>
    
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Profiles</h2>
            <ol class="breadcrumb">
                <li><a href="/">Home</a></li>
                <li class="active"><strong>Profiles</strong></li>
            </ol>
        </div>
    </div>
    <!-- ng-if="!is_loading"-->
    <div  ng-cloak ng-show="!is_loading">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div ng-class="{'col-sm-8 trans' : !isItemSet(), 'col-sm-3 trans' : isItemSet()}">
                <div class="ibox">
                    <div class="ibox-content">
                        <!-- <span class="text-muted small pull-right">Last modification: <i class="fa fa-clock-o"></i> 2:10 pm - 12.06.2014</span> -->
                   <!--      <h2>Profiles</h2> -->
                        <div class="clients-list">
                                <div class="pull-right">
                                    <a ng-show="!isItemSet()"" href="" data-toggle="modal" data-target="#profileCreateModal" class="btn btn-outline btn-success" ><i class="fa fa-plus"></i> Add New</a>
                                    <a ng-show="isItemSet()"" href="" data-toggle="modal" data-target="#profileCreateModal" class="btn btn-circle btn-outline btn-success" ><i class="fa fa-plus"></i></a>
                                </div>
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#tab-1" aria-expanded="true"><i class="fa fa-user"></i> Profiles</a></li>
                                <li ng-if="!isItemSet()" class=""><a href="/social_accounts"><i class="fa fa-users"></i> Social Accounts</a></li>                                
                            </ul>

                            <div class="tab-content">
                                <div id="tab-1" class="tab-pane active">
                                    <div class="table-responsive" >
                                        <table id="profiles_datatable" name="profiles_datatable" datatable="ng" dt-options="dtOptions" class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th>Name</th>
                                                    <th>Social</th> 
                                                    <th>PBN</th>
                                                    <th>Actions</th> 
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat="i in items track by $index" 
                                                    ng-click="clickedItemData(i.id)" 
                                                    id="@{{ i.id }}"
                                                    ng-class="{'current': isItemSelected(i) }">
                                                    <td class="client-avatar"><img alt="image" ng-src="@{{ i.avatar || '/assets/img/users/default.jpg' }}">
                                                    </td>
                                                    <td><a>@{{ i.first_name }} @{{ i.middle_name }} @{{ i.last_name }}</a></td>
                                                    <td>@{{ i.social_networks.length }}</td>
                                                    <td>@{{ i.urls.length }}</td>
                                                    <td>
                                                        <a type="button" class="btn btn-danger btn-circle btn-outline" data-toggle="modal" data-target="#profileDeleteModal"> <i class="fa fa-times" alt="Delete" ></i></a>                                                          
                                                    </td>
                                                </tr>
                                            </tbody>
                                           <tfoot>
                                                <tr>
                                                    <th></th>
                                                    <th>Name</th>
                                                    <th>Social</th> 
                                                    <th>PBN</th>
                                                    <th>Actions</th> 
                                                </tr>
                                            </tfoot>                                                
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div ng-class="{'col-sm-4 trans' : !isItemSet(), 'col-sm-9 trans' : isItemSet()}">
                @include('seo.profiles.right-panel')
            </div>
        </div>
    </div>
    </div>
               

    <toaster-container></toaster-container>        

    <!-- Profile Modal -->
    @include('seo.profiles.modals.create')
    @include('seo.profiles.modals.delete')    
    <!-- URL Modal -->
    @include('seo.urls.modals.delete')    
    <!-- Social Account Modal -->
    @include('seo.social_networks.modals.edit')
    @include('seo.social_networks.modals.link')    
    @include('seo.profiles.social_accounts.modals.delete')
    <!-- Note Modal -->
    @include('seo.hosts.partials.note_modal')  
    @include('seo.notes.modals.edit')  
    @include('seo.notes.modals.delete')                            
    <!-- /#page-wrapper -->
        
@endsection

@section('footer_scripts')
    <script src="/assets/js/plugins/select2/select2.full.min.js"></script>
    <script src="http://webapplayers.com/inspinia_admin/js/plugins/datapicker/bootstrap-datepicker.js"></script>

    
    <script type="text/javascript">
        $('#profile_urls_select').select2({
            tags:true,
            width: '100%'            
        });
        $('#gender_select').select2({
            width: '100%'            
        });        

        $('#profile_social_accounts_select').select2({
            tags:true,
            width: '100%'            
        });

        $('#birthday_data .input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true
        });
    </script>


@endsection