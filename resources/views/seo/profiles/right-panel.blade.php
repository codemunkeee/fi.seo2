<div ng-show="!isItemSet()">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>All Profiles</h5>
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>
                <a class="close-link">
                    <i class="fa fa-times"></i>
                </a>
            </div>
        </div>
        <div class="ibox-content">
            <div>
                <div 
                    donut-chart
                    donut-data='chart'
                    donut-formatter="morrisChartFormatter">
                </div>
            </div>
            <div>
                <table class="table small m-b-xs">
                    <tbody>
                        <tr><td><strong>Total Profiles:</strong> {{ count($profiles) }} </td> </tr>                                    
                        <tr><td><strong>Total Social Acct:</strong> {{ count($user_social_accounts) }}  </td></tr>
                    </tbody>
                </table>
                <h5 style="
                    text-align: center;
                    margin-bottom: 5px;
                    border-bottom: 1px solid rgb(103, 106, 108);
                    padding-bottom: 5px;
                ">Gender Diversification</h5>
                <div style="
                    width: 100%;
                ">
                  <h5 style="
                    width: {{ $gender_group['male'] }}%;
                    display: block;
                    float: left;
                    text-align: center;
                ">Men</h5>
                  <h5 style="
                    display: block;
                    width: {{ $gender_group['female'] }}%;
                    float: left;
                    text-align: center;
                ">Women</h5>
                </div>
                <div class="clearfix"></div>
                <div class="progress">
                    <div style="width: {{ $gender_group['male'] }}%" class="progress-bar progress-bar-success">
                        {{ $gender_group['male'] }}%
                    </div>
                    
                    <div style="width: {{ $gender_group['female'] }}%" class="progress-bar progress-bar-danger">
                       {{ $gender_group['female'] }}%
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div ng-show="isItemSet()">
    <div class="ibox">
        <div class="ibox-content">
            <div class="tab-content">
                <div class="tab-pane active">
                    <div class="row m-b-lg">
                        <div class="col-lg-4 text-center">
                            <h2>@{{ item_details.first_name }} @{{ item_details.middle_name }} @{{ item_details.last_name }}</h2>

                            <div class="m-b-sm">
                                <img alt="image" class="img-circle"  style="width: 62px" ng-src="@{{ i.avatar || '/assets/img/users/default.jpg' }}">
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <strong>
                                About me
                            </strong>

                            <p>
                                @{{ item_details.about }}
                            </p>
<!--                             <button type="button" class="btn btn-primary btn-sm btn-block"><i class="fa fa-envelope"></i> Send Message
                            </button> -->
                        </div>
                        <div class="col-lg-1">
                           <a type="button" class="btn btn-danger btn-circle btn-outline" ng-click="unsetItemData()"><i class="fa fa-times" alt="Close" ></i></a>
                        </div>                        
                    </div>
                    <div>

                        <!-- ADDITIONAL PROFILE INFORMATION  -->
                        <div class="ibox float-e-margins collapsed">
                            <div class="ibox-title">
                                <div class="row">
                                    <div class="col-md-10 collapse-link">
                                        <h5>Additional Profile Information</h5>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="ibox-tools" ng-init="is_edit=false">
                                            <a class="btn btn-default btn-circle" ng-click="is_edit=true">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <a class="btn btn-default btn-circle collapse-link">
                                                <i class="fa fa-chevron-up"></i>
                                            </a>
                                        </div>                                     
                                    </div>
                                </div>
                            </div>
                                                                              
                            <div class="ibox-content">
                                <div class="ibox-content">
                                    <div id="vertical-timeline" class="vertical-container light-timeline no-margins">
                                        <div class="vertical-timeline-block">
                                            <div class="vertical-timeline-icon blue-bg">
                                                <i class="fa fa-user"></i>
                                            </div>
                                            <div class="vertical-timeline-content" >
                                                <div>
                                                    <h2>Information</h2>
                                                    <br>
                                                    @include('seo.profiles.form_list')
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>   


                        <!-- PBNS -->
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <div class="row">
                                    <div class="col-md-10 collapse-link">
                                        <h5>PBNs</h5>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="ibox-tools" ng-init="add_url_clicked=false">
                                            <a class="btn btn-default btn-circle" ng-click="add_url_clicked=true">
                                                <i class="fa fa-plus"></i>
                                            </a>                                            
                                            <a class="btn btn-default btn-circle collapse-link">
                                                <i class="fa fa-chevron-up"></i>
                                            </a>                                    
                                        </div>                                                                                
                                    </div>
                                </div>
                            </div>                                  
                               
                            <div class="ibox-content">
                                <div ng-show="add_url_clicked">
                                    <div class="row">
                                        <div class="col-md-10">
                                            <div class="row">
                                                <div class="col-md-1" style="padding-top:5px">
                                                    <label>Add:</label>
                                                </div>
                                                <div class="col-md-11">
                                                    <select name="profile_urls_select" id="profile_urls_select" ng-model="pivot.data['profile_urls'].multiple.selected" class="form-control" placeholder="Select a URL" multiple>
                                                        <option ng-repeat="url in pivot.data['profile_urls'].multiple.unselected" value="@{{url.id}}" >@{{url.url}}</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 btn-group">
                                            <a class="btn btn-primary btn-outline" ng-click="pivot.method.linkMultiple('profile_urls', pivot.data['profile_urls'].multiple.selected); add_url_clicked=false" tooltip-placement="top" uib-tooltip="Add URL" data-container="body"><i class="fa fa-plus"></i></a>
                                            <a class="btn btn-danger btn-outline" ng-click="add_url_clicked=false" tooltip-placement="top" uib-tooltip="Cancel" data-container="body"><i class="fa fa-times"></i></a>                                            
                                        </div>
                                                                        
                                    </div>

                                </div>
                                <table id="url_datatable" name="url_datatable" datatable="ng" dt-options="dtOptionsOnClickRightSide" class="table medium m-b-xs">

                                    <thead>
                                        <tr>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="url in pivot.data['profile_urls'].items track by $index">
                                            <td>
                                                <div class="row" ng-click="pivot.method.selected('profile_urls', url)">
                                                    <div class="col-lg-8"><a href="" target="_blank">@{{ url.url }}</a></div>
                                                    <div class="col-lg-4"><a class="btn btn-danger btn-circle btn-outline pull-right" data-toggle="modal" data-target="#urlDeleteModal"><i class="fa fa-times"></i></a></div> 
                                                </div>
                                            </td>
                                        </tr>                                    
                                    </tbody>
                                </table>    
                            </div>
                        </div>       

                        <!-- SOCIAL ACCOUNTS -->
                        <div class="ibox float-e-margins collapsed">
                            <div class="ibox-title">
                                <div class="row">
                                    <div class="col-md-10 collapse-link">
                                        <h5>Social Accounts</h5>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="ibox-tools">
                                            <a class="btn btn-default btn-circle" data-toggle="modal" data-target="#socialAccountLinkModal">
                                                <i class="fa fa-plus"></i>
                                            </a>
                                            <a class="btn btn-default btn-circle collapse-link">
                                                <i class="fa fa-chevron-up"></i>
                                            </a>                                  
                                        </div>                                                                                
                                    </div>
                                </div>
                            </div>
                            <div class="ibox-content">
                                <table id="social_accounts_datatable" name="social_accounts_datatable" datatable="ng" dt-options="dtOptionsOnClickRightSide10"  class="table medium m-b-xs">                                 
                                    <thead>
                                        <tr>
                                            <th></th>
                                       </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="social_account in pivot.data['profile_social_accounts'].items track by $index" ng-click="pivot.method.selected('profile_social_accounts', social_account)" ng-class="{'current': pivot.method.isSelected('profile_social_accounts', social_account) }">
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-1">
                                                        <div><i class="fa @{{ social_account.logo | lowercase}} fa-2x"></i></div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div><a data-toggle="modal" data-target="#socialAccountModal">@{{social_account.name }}</a></div>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <div><a target="_blank" href="@{{social_account.pivot.profile_url}}">@{{social_account.pivot.username }}</a></div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <a href="@{{social_account.pivot.profile_url}}" target="_blank" type="button">View</a>
                                                        <a data-toggle="modal" data-target="#socialAccountModal" type="button">Edit</a>
                                                        <a type="button" class="btn btn-danger btn-circle btn-outline" data-toggle="modal" data-target="#socialAccountDeleteModal"> <i class="fa fa-times" alt="Delete"></i></a>                                  
                                                    </div>    
                                                </div>                                             
                                            </td>                
                                        </tr>                                    
                                    </tbody>
                                </table>    
                            </div>
                        </div>                        

                        <!-- NOTES -->
                        <div class="ibox float-e-margins collapsed">
                            <div class="ibox-title">
                                <div class="row">
                                    <div class="col-md-10 collapse-link">
                                        <h5>Notes</h5>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="ibox-tools">
                                            <a href="" data-toggle="modal" data-target="#noteCreateModal" class="btn btn-default btn-circle">
                                                <i class="fa fa-plus"></i>
                                            </a>
                                            <a class="btn btn-default btn-circle collapse-link">
                                                <i class="fa fa-chevron-up"></i>
                                            </a>                                 
                                        </div>                                                                                
                                    </div>
                                </div>
                            </div>

                            <div class="ibox-content">
                                <table class="table medium m-b-xs">
                                    <tbody>
                                        <tr ng-repeat="note in pivot.data['notes'].items track by $index" ng-click="pivot.method.selected('notes', note)">
                                            <td>
                                                <div>
                                                    <h2>@{{ note.title }}</h2>
                                                    <div ng-bind-html="note.notes"></div>
                                                      <a type="button" class="btn btn-danger btn-circle btn-outline pull-right" data-toggle="modal" data-target="#noteDeleteModal"> <i class="fa fa-times" alt="Delete"></i></a> 
                                                      <a type="button" class="btn btn-warning btn-circle btn-outline pull-right" data-toggle="modal" data-target="#noteEditModal"> <i class="fa fa-edit" alt="Edit"></i></a>                                                        
                                                    <span class="vertical-date">
                                                        <small>@{{ note.updated_at }}</small>
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>                                    
                                    </tbody>
                                </table>    
                            </div>
                        </div>  

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

