@extends('app')

@section('header_scripts')
    <link href="/assets/css/datatables/jquery.dataTables.min.css" rel="stylesheet">
@endsection

@section('content')
    <div id="page-wrapper">
    @include('errors.validation')
        <div class="container-fluid">
      <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"> Personas <small> Overview </small></h1>
                    <ol class="breadcrumb">
                        <li class="active"> <i class="fa fa-users"></i> Item </li>
                        <li><a href='/personas/create'><i class="fa fa-plus"></i> Create New</a></li>
                    </ol>

                    <table id="urls" class="display" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>First Name</th>
                                <th>Middle Name</th> 
                                <th>Last Name</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>First Name</th>
                                <th>Middle Name</th> 
                                <th>Last Name</th>
                                <th>Actions</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            @foreach($personas as $persona)
                                <tr>    
                                    <td>{{ $persona->first_name }}</td>
                                    <td>{{ $persona->middle_name }}</td> 
                                    <td>{{ $persona->last_name }}</td>

                                    <td>
                                        {!! Form::open(['url'=>'/personas/'.$persona->id, 'method'=>'DELETE', 'onsubmit'=>'return confirm("Are you sure you want to delete this?")']) !!}
                                            <div class="btn-group">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">                                     
                                                <a href="/personas_urls/{{ $persona->id }}" class="btn btn-success"><i class="fa fa-eye" alt="View"></i></a>  
                                                        
                                                <a href="/personas_urls/{{ $persona->id }}/edit" class="btn btn-warning"><i class="fa fa-edit" alt="Edit"></i></a>  
                                                <button type="submit" class="btn btn-danger"><i class="fa fa-times" alt="Delete"></i></button>
                                            </div>
                                                                                      
                                        {!! Form::close() !!}                                   
                                    </td>
                                </tr>
                            @endforeach
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
           
@endsection

@section('footer_scripts')

    <script src="/assets/js/datatables/jquery.dataTables.min.js"></script>

    <script>
        $(document).ready(function() {
            $('#urls').DataTable();
        } );
    </script>

@endsection