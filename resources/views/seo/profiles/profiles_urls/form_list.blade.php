@if($method == 'show')
	@foreach($persona_urls as $url)
		<span class="badge badge-primary">{{ \App\Models\SEO\Url::findOrFail($url)->url }}</span>

	@endforeach
	<br><br>
	<label> Notes: </label>
	<p>
		{{ isset($persona_notes_urls) && $persona_notes_urls !== '' ? $persona_notes_urls : 'No information to show.'}}
	</p>
@elseif ($method == 'edit')
	{!! Form::model($persona,  ['url'=>'/personas_urls/' . $persona->id, 'method'=>'PUT']) !!}
		<div class="form-group"><label>Websites</label>

		{{ Form::select('urls[]', $user_urls, isset($persona_urls) ? $persona_urls : null, ['id' => 'urls', 'class' => 'form-control', 'multiple' => 'multiple', $disabled]) }}</div>
			

		<div class="form-group"><label>Website Notes: </label> {!! Form::textarea('notes_urls',  isset($persona->user(\Auth::user())->notes_urls) ? $persona->user(\Auth::user())->notes_urls : null, ['placeholder' => 'Website Notes', 'class' => 'form-control', $disabled]) !!}</div>
				
		 <button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit" {{$disabled}}><strong>Submit</strong></button>
	{!! Form::close() !!}
@else

@endif
