@extends('app')

@section('header_scripts')
	<link href="/assets/css/select2/select2.min.css" rel="stylesheet" />
	<script src="/assets/js/select2/select2.min.js"></script>
@endsection
@section('content')

	<div id="page-wrapper">
        @include('errors.validation')
        <div class="container-fluid">
        	@include('seo.personas.personas_urls.form_list')     
        </div>
    </div>


@endsection

@section('footer_scripts')
	<script type="text/javascript">
	  $('#urls').select2();
	</script>
@endsection