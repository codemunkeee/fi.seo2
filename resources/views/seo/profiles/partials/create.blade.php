<div class="row">
    <div class="col-md-9">
      <div class="row" style="padding-top:5px">
         <div class="col-md-3" style="padding-top:5px"><label>First Name</label></div>
         <div class="col-md-9"><input type="text" ng-model="new_item.first_name" class="form-control" placeholder="First Name" /></div>
      </div>

      <div class="row" style="padding-top:5px">
         <div class="col-md-3" style="padding-top:5px"><label>Middle Name</label></div>
         <div class="col-md-9"><input type="text" ng-model="new_item.middle_name" class="form-control" placeholder="Middle Name" /></div>
      </div>

      <div class="row" style="padding-top:5px">
         <div class="col-md-3" style="padding-top:5px"><label>Last Name</label></div>
         <div class="col-md-9"><input type="text" ng-model="new_item.last_name" class="form-control" placeholder="Last Name" /></div>
      </div>
      
    </div>
    <div class="col-md-3 text-center">
      <div class="profile-image" tooltip-placement="top" uib-tooltip="Click to Add a Profile Picture" data-container="body">
        <button type="file" ngf-select="" ng-model="new_item.avatar" name="avatar" ngf-accept="'image/*'" required="" class="ng-pristine ng-invalid ng-invalid-required" accept="image/*" style="border: 0; background: transparent">
            <img ng-if="!new_item.avatar" src="/assets/img/default.jpeg" class="img-circle circle-border m-b-md" alt="profile">
            <img ngf-src="new_item.avatar" class="img-circle circle-border m-b-md" alt="profile">
        </button>
      </div>    
    
      <div class="progress" ng-show="new_item.avatar.progress >= 0">
          <div style="width: @{{new_item.avatar.progress}}%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="0" role="progressbar" class="progress-bar progress-bar-success">
              <span class="sr-only">@{{new_item.avatar.progress}}% Complete (success)</span>
          </div>
      </div>  
  </div>
</div>

<hr>
<h3> Additional Details </h3>
<hr>


<div class="row" style="padding-top:5px">
   <div class="col-md-3" style="padding-top:5px"><label>Gender</label></div>
   <div class="col-md-9">                                                   
    <select name="gender_select" id="gender_select" ng-model="new_item.gender" class="form-control" placeholder="Gender">
        <option value="M" selected="selected">Male</option>
        <option value="F">Female</option>        
    </select>
  </div>
</div>

<div class="row" style="padding-top:5px">
    <div class="col-md-3"><label>Birthday</label></div>
    <div class="col-md-9">               
        <div class="form-group" id="birthday_data">
            <div class="input-group date">
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input data-date-format="yyyy/dd/mm" id="birthday" name="birthday" type="text" class=" form-control" ng-model="new_item.birthday">
            </div>
        </div>
    </div>
</div>                                                                           

<div class="row" style="padding-top:5px">
   <div class="col-md-3" style="padding-top:5px"><label>About</label></div>
   <div class="col-md-9"><textarea rows="4" ng-model="new_item.about" class="form-control" placeholder="About" ></textarea></div>
</div>

<div class="row" style="padding-top:5px">
   <div class="col-md-3" style="padding-top:5px"><label>Topic</label></div>
   <div class="col-md-9"><input type="text" ng-model="new_item.topic" class="form-control" placeholder="Topic" /></div>
</div>

<div class="row" style="padding-top:5px">
   <div class="col-md-3" style="padding-top:5px"><label>Description</label></div>
   <div class="col-md-9"><textarea rows="4" ng-model="new_item.description" class="form-control" placeholder="Description" ></textarea></div>
</div>


<div class="row" style="padding-top:5px">
   <div class="col-md-3" style="padding-top:5px"><label>Phone</label></div>
   <div class="col-md-9"><input type="text" ng-model="new_item.phone" class="form-control" placeholder="Phone" /></div>
</div>

<div class="row" style="padding-top:5px">
   <div class="col-md-3" style="padding-top:5px"><label>City Address</label></div>
   <div class="col-md-9"><input type="text" ng-model="new_item.city_address" class="form-control" placeholder="City Address" /></div>
</div>

<div class="row" style="padding-top:5px">
   <div class="col-md-3" style="padding-top:5px"><label>Tx</label></div>
   <div class="col-md-9"><input type="text" ng-model="new_item.tx" class="form-control" placeholder="Tx" /></div>
</div>               

<div class="row" style="padding-top:5px">
   <div class="col-md-3" style="padding-top:5px"><label>Zip</label></div>
   <div class="col-md-9"><input type="text" ng-model="new_item.zip" class="form-control" placeholder="Zip" /></div>
</div>


<hr>
<h3> Login Details </h3>
<hr>


<div class="row" style="padding-top:5px">
   <div class="col-md-3" style="padding-top:5px"><label>Email Login Link</label></div>
   <div class="col-md-9"><input type="text" ng-model="new_item.email_login_link" class="form-control" placeholder="Email Login Link" /></div>
</div>

<div class="row" style="padding-top:5px">
   <div class="col-md-3" style="padding-top:5px"><label>Email</label></div>
   <div class="col-md-9"><input type="text" ng-model="new_item.email" class="form-control" placeholder="Email" /></div>
</div>

<div class="row" style="padding-top:5px">
   <div class="col-md-3" style="padding-top:5px"><label>Email Password</label></div>
   <div class="col-md-9"><input type="text" ng-model="new_item.email_password" class="form-control" placeholder="Email Password" /></div>
</div>

<div class="row" style="padding-top:5px">
   <div class="col-md-3" style="padding-top:5px"><label>Main Username</label></div>
   <div class="col-md-9"><input type="text" ng-model="new_item.main_username" class="form-control" placeholder="Main Username" /></div>
</div>

<div class="row" style="padding-top:5px">
   <div class="col-md-3" style="padding-top:5px"><label>Main Password</label></div>
   <div class="col-md-9"><input type="text" ng-model="new_item.main_password" class="form-control" placeholder="Main Password" /></div>
</div>

<div class="row" style="padding-top:5px">
   <div class="col-md-3" style="padding-top:5px"><label>CPanel Address</label></div>
   <div class="col-md-9"><input type="text" ng-model="new_item.cpanel_address" class="form-control" placeholder="CPanel Address" /></div>
</div>

<div class="row" style="padding-top:5px">
   <div class="col-md-3" style="padding-top:5px"><label>CPanel Username</label></div>
   <div class="col-md-9"><input type="text" ng-model="new_item.cpanel_username" class="form-control" placeholder="CPanel Username" /></div>
</div>

<div class="row" style="padding-top:5px">
   <div class="col-md-3" style="padding-top:5px"><label>CPanel Password</label></div>
   <div class="col-md-9"><input type="text" ng-model="new_item.cpanel_password" class="form-control" placeholder="CPanel Password" /></div>
</div>

<div class="row" style="padding-top:5px">
   <div class="col-md-3" style="padding-top:5px"><label>FTP Address</label></div>
   <div class="col-md-9"><input type="text" ng-model="new_item.ftp_address" class="form-control" placeholder="FTP Address" /></div>
</div>

<div class="row" style="padding-top:5px">
   <div class="col-md-3" style="padding-top:5px"><label>FTP Username</label></div>
   <div class="col-md-9"><input type="text" ng-model="new_item.ftp_user" class="form-control" placeholder="FTP Username" /></div>
</div>

<div class="row" style="padding-top:5px">
   <div class="col-md-3" style="padding-top:5px"><label>FTP Password</label></div>
   <div class="col-md-9"><input type="text" ng-model="new_item.ftp_password" class="form-control" placeholder="FTP Password" /></div>
</div>

<div class="row" style="padding-top:5px">
   <div class="col-md-3" style="padding-top:5px"><label>WP Admin</label></div>
   <div class="col-md-9"><input type="text" ng-model="new_item.wp_admin" class="form-control" placeholder="WP Admin" /></div>
</div>

<div class="row" style="padding-top:5px">
   <div class="col-md-3" style="padding-top:5px"><label>WP Password</label></div>
   <div class="col-md-9"><input type="text" ng-model="new_item.wp_password" class="form-control" placeholder="WP Password" /></div>
</div>

