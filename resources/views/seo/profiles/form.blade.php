@extends('app')

@section('meta-tags')
	<meta name="description" content="{{$profile->first_name}} {{$profile->last_name}}">
	
@endsection

@section('header_scripts')

  	<link href="/assets/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
    <link href="/assets/css/plugins/select2/select2.min.css" rel="stylesheet">
    <link href="/assets/css/plugins/summernote/summernote2.min.css" rel="stylesheet">
    <link href="/assets/css/fi/social_networks/social_networks.css" rel="stylesheet">
    <link href="http://webapplayers.com/inspinia_admin/css/plugins/datapicker/datepicker3.css" rel="stylesheet">    
    <link href="/assets/fi/social-networks/fi_social_networks.css" rel="stylesheet">
   
    <script src="/assets/js/plugins/clipboard/clipboard.min.js"></script>
    <script src="/assets/js/plugins/dataTables/datatables.min.js"></script>
    <script src="/assets/js/angularjs/angular.min.js"></script>
    <script src="/assets/js/angularjs/angular-sanitize.min.js"></script>
    <script src="/assets/js/angularjs/angular-animate2.min.js"></script>
    <script src="/assets/js/angularjs/select/select.min.js"></script>
    <script src="/assets/js/angularjs/toastr/toastr.min.js"></script>
    <script src="/assets/js/angularjs/ui/ui-bootstrap.min.js"></script>
    <script src="/assets/js/angularjs/clipboard/ngclipboard.min.js"></script>
    <script src="/assets/js/angularjs/datatables/angular-datatables.min.js"></script>
    <script src="/assets/js/angularjs/datatables/angular-datatables.buttons.min.js"></script>
    <script src="/assets/js/angularjs/datatables/angular-datatables.tabletools.min.js"></script>    
    <script src="/assets/js/plugins/raphael/raphael.min.js"></script>    
    <script src="/assets/js/plugins/morris/morris2.min.js"></script>
    <script src="/assets/js/angularjs/morris/morris.min.js"></script>
    <script src="/assets/js/plugins/summernote/summernote2.min.js"></script>
    <script src="/assets/js/angularjs/summernote/summernote.min.js"></script>
    <script src="https://rawgit.com/eight04/angular-datetime/master/dist/datetime.js"></script>

    <script src="/assets/js/angularjs/fileupload/ng-file-upload.min.js"></script>
    <script src="/assets/js/angularjs/fileupload/ng-file-upload-shim.min.js"></script>

    <script src="/assets/js/angularjs/common/single/main.js"></script>
    <script src="/assets/js/angularjs/common/single/item.js"></script>

    
    <script> angular.module("FI").constant("CSRF_TOKEN", '{{ csrf_token() }}'); </script>



@endsection


@section('content')

<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element"> 
                	<span>
<!--                     	<img alt="image" class="img-circle" src="/assets/img/users/default.jpeg" height="48" width="48"/> -->
                     </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                    <span class="clear"> <span class="block m-t-xs"> 
                    <strong class="font-bold">Guest</strong>
                     </span> <span class="text-muted text-xs block">
                     	Welcome, Guest!

                     <b class="caret"></b></span> </span> </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a href="/auth/login">Login</a></li>
                    </ul>
                </div>
                <div class="logo-element">
                    FI
                </div>
            </li>
        </ul>
    </div>
</nav>            

<div ng-app="FI" ng-controller="MainCtrl">
    <input type="hidden" ng-init="user_id=0;pivot_api={{ json_encode($pivot_api)}};pivot.data['profile_social_accounts'].items={{ json_encode($profile_social_accounts)}}; pivot.data['profile_urls'].items={{ json_encode($profile_urls) }}"/>
	
	<div id="page-wrapper" class="gray-bg">
		<div class="row border-bottom">
			
		</div>	
		<div class="row wrapper border-bottom white-bg page-heading">
		    <div class="col-lg-10">
		        <h2>Profiles</h2>
		        <ol class="breadcrumb">
		            <li><a href="/">Home</a></li>
		            <li><a href="/profiles">Profiles</a></li>
					<li><a href="/profiles/{{ $sanitized_name }}/{{ $profile->id}}">{{ $profile->first_name }} {{ $profile->middle_name}} {{$profile->last_name}}</a></li>		            
		        </ol>
		    </div>
		</div>

		<div class="wrapper wrapper-content">
		 	<div class="row animated fadeInRight">
			    <div class="col-md-4">
			        <div class="ibox float-e-margins">
			            <div class="ibox-title">
			                <h5>Profile Details</h5>
			            </div>
			            <div>
			                <div class="ibox-content no-padding border-left-right">
			                	<img alt="image" class="img-responsive" src="{{ (isset($profile->avatar) && $profile->avatar !== '') ? $profile->avatar : '/assets/img/default.jpeg' }}">

			                </div>
			                <div class="ibox-content profile-content">
			                    <h4><strong>{{$profile->first_name}} {{$profile->middle_name}} {{$profile->last_name}}</strong></h4>
			                    <p><i class="fa fa-map-marker"></i> {{$profile->city_address}}, {{$profile->tx}}, {{$profile->zip}}</p>
			                    <h5>Topic</h5>
			                    <p>
			                    	{{ (isset($profile->topic) && $profile->topic !== '') ? $profile->topic : 'No information to show' }}
			                    </p>
			                    <h5>
			                        About me
			                    </h5>
			                    <p>
									{{ (isset($profile->about) && $profile->about !== '') ? $profile->about : 'No information to show' }}
			                    </p>
			                    <h5>Description</h5>
			                    <p>
			                    	{{ (isset($profile->description) && $profile->description !== '') ? $profile->description : 'No information to show' }}
			                    </p>
			                    <div class="row m-t-lg">
			                        <div class="col-md-6">
			                            <h5><strong>{{$profile->socialAccounts()->count()}}</strong> Social Accounts</h5>
			                        </div>
									<div class="col-md-6">
			                            <h5><strong>{{$profile->urls()->count()}}</strong> Websites</h5>
			                        </div>	                    
			                    </div>
			                    <div class="user-button">
			                        <div class="row">
			                            <div class="col-md-6">
			                                <button type="button" class="btn btn-primary btn-sm btn-block" disabled><i class="fa fa-envelope"></i> Send Message</button>
			                            </div>
			                            <div class="col-md-6">
			                                <button type="button" class="btn btn-default btn-sm btn-block" disabled><i class="fa fa-coffee"></i> Buy a coffee</button>
			                            </div>
			                        </div>
			                    </div>
			                </div>
			        	</div>
			    	</div>
		   		</div> 
		   		<div class="col-md-8">
		            <div class="ibox float-e-margins">
			            <div class="ibox-title">
			                <h5>Profile information</h5>
			                <div class="ibox-tools">
			                    <a class="collapse-link">
			                        <i class="fa fa-chevron-up"></i>
			                    </a>
			                </div>
			            </div>
			            <div class="ibox-content">
				   			<div id="vertical-timeline" class="vertical-container light-timeline no-margins">
				                <div class="vertical-timeline-block">
				                    <div class="vertical-timeline-icon blue-bg">
				                        <i class="fa fa-user"></i>
				                    </div>
				                    <div class="vertical-timeline-content">
			                        	<div class="row">
										    <div class="col-md-4">
										        <label>First Name</label> <p> {{ $profile->first_name }}</p>
										    </div>
										    <div class="col-md-4">
										        <label>Middle Name</label> <p>{{ $profile->middle_name }} </p>
										    </div>

										    <div class="col-md-4">
										        <label>Last Name</label> <p>{{ $profile->last_name }}</p>
										    </div>
										</div>

										<p><i class="fa fa-birthday-cake"></i> Birthday : {{ $profile->birthday->format('m/d/Y')}}</p>
										<p><i class="fa fa-phone"></i> Phone Number : {{ $profile->phone }}</p>
										<p><i class="fa fa-map-marker"></i> {{ $profile->city_address }}, {{ $profile->tx }}, {{ $profile->zip }}</p>
										<span class="vertical-date">
										     Last Updated: <br>
										    <small>{{ $profile->updated_at->format('m/d/Y')}}</small>
										</span>
				                    </div>

				                </div>
				            </div>
		            	</div>
		   			</div>  

		   			<div class="ibox float-e-margins">
			            <div class="ibox-title">
			                <h5>Social Accounts</h5>
			                <div class="ibox-tools">
			                    <a class="collapse-link">
			                        <i class="fa fa-chevron-up"></i>
			                    </a>
			                </div>
			            </div>
			            <div class="ibox-content">
				   			<div id="vertical-timeline" class="vertical-container light-timeline no-margins">
				                <div class="vertical-timeline-block">
				                    <div class="vertical-timeline-icon blue-bg">
				                        <i class="fa fa-users"></i>
				                    </div>
				                    <div class="vertical-timeline-content">
										<table id="social_accounts_datatable" name="social_accounts_datatable" datatable="ng" dt-options="dtOptions"  class="table medium m-b-xs">
											<thead>
		                                        <tr>
		                                            <th></th>
		                                       </tr>
		                                    </thead>
		                                    <tbody>
		                                        <tr ng-repeat="social_account in pivot.data['profile_social_accounts'].items track by $index" ng-click="pivot.method.selected('profile_social_accounts', social_account)" ng-class="{'current': pivot.method.isSelected('profile_social_accounts', social_account) }">
		                                            <td>
		                                                <div class="row">
		                                                    <div class="col-md-1">
		                                                        <div><i class="fa @{{ social_account.logo | lowercase}} fa-2x"></i></div>
		                                                    </div>
		                                                    <div class="col-md-11">
		                                                        <div><a href="@{{social_account.pivot.profile_url}}">@{{social_account.name }}</a></div>
		                                                    </div>  
		                                                </div>                                             
		                                            </td>                
		                                        </tr>                                    
		                                    </tbody>
		                                </table>    										
									</div>
				                </div>
				            </div>
		            	</div>
		   			</div>   

		   			<div class="ibox float-e-margins">
			            <div class="ibox-title">
			                <h5>Urls</h5>
			                <div class="ibox-tools">
			                    <a class="collapse-link">
			                        <i class="fa fa-chevron-up"></i>
			                    </a>
			                </div>
			            </div>
			            <div class="ibox-content">
				   			<div id="vertical-timeline" class="vertical-container light-timeline no-margins">
				                <div class="vertical-timeline-block">
				                    <div class="vertical-timeline-icon blue-bg">
				                        <i class="fa fa-external-link"></i>
				                    </div>
				                    <div class="vertical-timeline-content">
										 <table id="url_datatable" name="url_datatable" datatable="ng" dt-options="dtOptionsOnClickRightSide" class="table medium m-b-xs">
		                                    <thead>
		                                        <tr>
		                                            <th></th>
		                                        </tr>
		                                    </thead>
		                                    <tbody>
		                                        <tr ng-repeat="url in pivot.data['profile_urls'].items track by $index">
		                                            <td>
		                                                <div class="row" ng-click="pivot.method.selected('profile_urls', url)">
		                                                    <div class="col-lg-8"><a href="" target="_blank">@{{ url.url }}</a></div>
		                                                </div>
		                                            </td>
		                                        </tr>                                    
		                                    </tbody>
		                                </table>    
				                    </div>
				                </div>
				            </div>
		            	</div>
		   			</div>

				</div>
			</div>
		</div>

	</div>

</div>
@endsection

@section('footer_scripts')


@endsection