<div>
	<div class="row">
		<div class="col-xs-9">
			<select class="form-control" id="test" ng-model="social_account" placeholder="select" ng-options="social_network.name disable when isDisabled(social_network) for social_network in social_networks track by social_network.social_account_id">
			</select> 
		</div>
		<div class="col-xs-3">
			<div class="btn-group pull-right">
			<button class="btn btn-success" ng-click="view()"><span class="fa fa-eye"></span></button>
			<button class="btn btn-danger" ng-click="delete()"><span class="fa fa-close"></span></button>
			</div>
		</div>

	</div>
	
	<input ng-model="social_account.profile_url" type="text" placeholder="Profile URL" class="form-control" />
	<input ng-model="social_account.password" type="text" placeholder="Password" class="form-control" />
	<input ng-model="social_account.username" type="text" placeholder="Username" class="form-control" />

	<hr>
</div>