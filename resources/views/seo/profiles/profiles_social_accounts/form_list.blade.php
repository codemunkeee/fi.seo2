@if($method == 'show')
	@foreach($persona_social_accounts as $social_account)
		<div class="row">
		    <div class="col-md-4">
		        <label>Social Account Name</label>
					<p><a href="{{ \Crypt::decrypt($social_account->pivot->profile_url)}}">{{ $social_account->name }}</a></p>
		    </div>
		    <div class="col-md-4">
		    	<label>Username</label> <p> {{ \Crypt::decrypt($social_account->pivot->password) }}</p>
		    </div>

		    <div class="col-md-4">
		    	<label>Password</label> <p> {{ \Crypt::decrypt($social_account->pivot->password) }}</p>
		    </div>
		</div>

	@endforeach
	<br><br>
	<label> Notes: </label>
	<p>
		{{ isset($persona_notes_social_accounts) && $persona_notes_social_accounts !== '' ? $persona_notes_social_accounts : 'No information to show.'}}
	</p>
@elseif ($method == 'edit')
	
	<hr>
	<div ng-init="persona.notes={{ json_encode($persona_notes_social_accounts) }}">
		<div ng-repeat="social_account in social_accounts">
			<div>
				<div class="row">
					<div class="col-xs-4">
						<p> @{{ socialName(social_account) }} </p> 
					</div>
					<div class="col-xs-5">
						<select class="form-control" ng-model="social_account.social_account_id" placeholder="select">
						<option ng-disabled="isDisabled(social_network)" ng-repeat="social_network in social_networks">@{{social_network.name}}</option>
						</select> 

					</div>
					<div class="col-xs-3">
						<div class="btn-group pull-right">
						<button class="btn btn-success" ng-click="view(social_account.profile_url)"><span class="fa fa-eye"></span></button>
						<button class="btn btn-danger" ng-click="delete($index)"><span class="fa fa-close"></span></button>
						</div>
					</div>
				</div>
				
				<input ng-model="social_account.profile_url" type="text" placeholder="Profile URL" class="form-control" />
				<input ng-model="social_account.password" type="text" placeholder="Password" class="form-control" />
				<input ng-model="social_account.username" type="text" placeholder="Username" class="form-control" />
				<!-- <p> For testing:  @{{ social_account }} <p> -->
				<hr>
			</div>
		</div>

		<textarea rows="4" cols="50" ng-model="persona.notes" placeholder="Notes" class="form-control" ></textarea>

	</div>
	@if ($method == 'edit')
	<button class="btn btn-primary pull-right" ng-click="addMore()">Add More..</button>
	<button class="btn btn-primary" ng-click="submit()">Submit</button>
	<button class="btn btn-primary" ng-click="test()">test</button>

@else

@endif
