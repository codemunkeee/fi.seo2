<div class="modal inmodal" id="socialAccountLinkModal"  role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-md">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <i class="fa fa-clock-o modal-icon"></i>
                <h4 class="modal-title">Link a Social Network to this Profile</h4>
            </div>
            <div class="modal-body">
                @include('seo.profiles.social_accounts.partials.link')
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" ng-click="pivot.method.link('social_accounts', item_details)" data-dismiss="modal">Delete</button>
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>