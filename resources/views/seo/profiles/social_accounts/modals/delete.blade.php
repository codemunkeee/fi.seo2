<div class="modal inmodal" id="socialAccountDeleteModal"  role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-md">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <i class="fa fa-clock-o modal-icon"></i>
                <h4 class="modal-title">Delete Social Network</h4>
            </div>
            <div class="modal-body">
                @include('seo.profiles.social_accounts.partials.delete')
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" ng-click="pivot.method.delete('profile_social_accounts', pivot.data['profile_social_accounts'].selected.pivot)" data-dismiss="modal">Delete</button>
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
