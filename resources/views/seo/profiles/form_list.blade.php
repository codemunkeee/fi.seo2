<div ng-show="!is_edit"> 
	<div class="row">
	    <div class="col-md-4">
	        <label>First Name</label> <p> @{{ item_details.first_name }}</p>
	    </div>
	    <div class="col-md-4">
	        <label>Middle Name</label> <p>@{{ item_details.middle_name }} </p>
	    </div>

	    <div class="col-md-4">
	        <label>Last Name</label> <p>@{{ item_details.last_name }}</p>
	    </div>
	</div>

	<p><i class="fa fa-birthday-cake"></i> Birthday : @{{  convertItemDateFromString(item_details.birthday.date) | date:'MM/dd/yyyy' }}</p>
	<p><i class="fa fa-phone"></i> Phone Number : @{{ item_details.phone }}</p>
	<p><i class="fa fa-map-marker"></i> @{{ item_details.city_address }}, @{{ item_details.tx }}, @{{ item_details.zip }}</p>
	<span class="vertical-date">
	     Last Updated: <br>
	    <small>@{{ convertItemDateFromString(item_details.updated_at) | date:'MM/dd/yyyy HH:mm:ss Z' }}</small>
	</span>
</div>

<div ng-show="is_edit">

	<div class="row" style="padding-top:10px">
        <div class="col-md-4" style="padding-top:5px">
            <label>First Name:</label>
        </div>
        <div class="col-md-8">
         <input id="first_name" name="first_name" type="text" class="form-control" ng-model="item_details.first_name" /> 
        </div>
    </div>
    <div class="row" style="padding-top:10px">
        <div class="col-md-4" style="padding-top:5px">
            <label>Middle Name:</label>
        </div>
        <div class="col-md-8">
         <input id="middle_name" name="middle_name" type="text" class="form-control" ng-model="item_details.middle_name" /> 
        </div>
    </div>
    <div class="row" style="padding-top:10px">
        <div class="col-md-4" style="padding-top:5px">
            <label>Last Name:</label>
        </div>
        <div class="col-md-8">
         <input id="last_name" name="last_name" type="text" class="form-control" ng-model="item_details.last_name" /> 
        </div>
    </div>
<!--     <div class="row" style="padding-top:10px">
        <div class="col-md-4" style="padding-top:5px">
            <label>Birthday:</label>
        </div>
        <div class="col-md-8">
	        <div class="form-group" id="birthday_data">
                <div class="input-group date">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input id="birthday" name="birthday" type="text" class="form-control" ng-model="item_details.birthday">
                </div>
            </div>
        </div>
    </div> -->
    <div class="row" style="padding-top:10px">
        <div class="col-md-4" style="padding-top:5px">
            <label>Phone Number:</label>
        </div>
        <div class="col-md-8">
         <input id="phone" name="phone" type="text" class="form-control" ng-model="item_details.phone" /> 
        </div>
    </div>
    <div class="row" style="padding-top:10px">
        <div class="col-md-4" style="padding-top:5px">
            <label>Address</label>
        </div>
        <div class="col-md-8">
         <input id="city_address" name="city_address" type="text" class="form-control" ng-model="item_details.city_address" /> 
        </div>
	</div>
    <div class="row" style="padding-top:10px">
        <div class="col-md-6">
            <div class="row">
            <div class="col-md-4" style="padding-top:5px">
                <label>Tx:</label>
            </div>
            <div class="col-md-8">
             <input id="tx" name="tx" type="text" class="form-control" ng-model="item_details.tx" /> 
            </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="row">
            <div class="col-md-4" style="padding-top:5px">
                <label>Zip:</label>
            </div>
            <div class="col-md-8">
             <input id="zip" name="zip" type="text" class="form-control" ng-model="item_details.zip" /> 
            </div>
            </div>
        </div>                                                      
    </div>

	<div class="pull-right" style="padding-top:20px">
	    <a class="btn btn-danger btn-outline" ng-click="is_edit=false"><i class="fa fa-times"></i> Cancel</a>
	    <a class="btn btn-primary btn-outline" ng-click="update()"><i class="fa fa-edit"></i> Save</a>
	 </div>

</div>
