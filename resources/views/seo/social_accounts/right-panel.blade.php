<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>Breakdown / Diversification</h5>
        <div class="ibox-tools">
            <a class="collapse-link">
                <i class="fa fa-chevron-up"></i>
            </a>
            <a class="close-link">
                <i class="fa fa-times"></i>
            </a>
        </div>
    </div>
    <div class="ibox-content">
        <div>
            <div 
                donut-chart
                donut-data='chart'
                donut-formatter="morrisChartFormatter">
            </div>
        </div>
        <div>
            <table class="table small m-b-xs">
                <tbody>
                    <tr><td><strong>Total Social Accounts:</strong> {{ count($social_accounts)}} </td> </tr>  
                    <tr><td><strong>Total Accounts by Social Network:</strong> 

                    <table class="table small m-b-xs">
                        <tbody>
                            <thead>
                                <tr>
                                    <td>Social Network</td>
                                    <td>Count</td>
                                </tr>
                            </thead>
                            <tr ng-repeat="social_account in social_accounts_group">
                                <td><a ng-click="searchTable(social_account.label)">@{{social_account.label }}</a></td> 
                                <td><strong>@{{social_account.value }}</strong></td> 
                            </tr>                              
                        </tbody>
                    </table>
                    </td> </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>


