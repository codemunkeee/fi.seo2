   
	<label>Select a Profile:</label>
    <select name="profiles_select" id="profiles_select" ng-model="new_item.profile_id" class="form-control" placeholder="Profile">
        <option ng-repeat="profile in profiles" value="@{{profile.id}}">@{{ profile.first_name }} @{{ profile.middle_name }} @{{ profile.last_name }}</option>        
    </select>

	<label>Select a Social Network:</label>
    <select name="social_networks_select" id="social_networks_select" ng-model="new_item.social_network_id" class="form-control" placeholder="Social Network">
        <option ng-repeat="social_network in social_networks" value="@{{social_network.id}}">@{{ social_network.name }}</option>   
    </select>


	<label>Username:</label>
	<input id="username" name="username" type="text" class="form-control" ng-model="new_item.username" />
	<label>Password:</label>
	<input id="password" name="password" type="text" class="form-control" ng-model="new_item.password" />
	<label>Profile URL: </label>
	<input id="profile_url" name="profile_url" type="text" class="form-control" ng-model="new_item.profile_url" />