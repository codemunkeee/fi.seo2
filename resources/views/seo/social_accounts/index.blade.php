@extends('app')

@section('header_scripts')
    <link href="/assets/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
    <link href="/assets/css/plugins/select2/select2.min.css" rel="stylesheet">

    <link href="/assets/css/plugins/summernote/summernote2.min.css" rel="stylesheet">
    <link href="/assets/css/fi/social_networks/social_networks.css" rel="stylesheet">

    <link href="http://webapplayers.com/inspinia_admin/css/plugins/datapicker/datepicker3.css" rel="stylesheet">    
    
    <link href="/assets/fi/social-networks/fi_social_networks.css" rel="stylesheet">
    <script src="/assets/js/plugins/clipboard/clipboard.min.js"></script>
    <script src="/assets/js/plugins/dataTables/datatables.min.js"></script>
    <script src="/assets/js/angularjs/angular.min.js"></script>
    <script src="/assets/js/angularjs/angular-sanitize.min.js"></script>
    <script src="/assets/js/angularjs/angular-animate2.min.js"></script>
    <script src="/assets/js/angularjs/oclazyload/oclazyload.min.js"></script>
    <script src="/assets/js/angularjs/select/select.min.js"></script>
    <script src="/assets/js/angularjs/toastr/toastr.min.js"></script>
    <script src="/assets/js/angularjs/ui/ui-bootstrap.min.js"></script>
    <script src="/assets/js/angularjs/clipboard/ngclipboard.min.js"></script>
    <script src="/assets/js/angularjs/datatables/angular-datatables.min.js"></script>
    <script src="/assets/js/angularjs/datatables/angular-datatables.buttons.min.js"></script>
    <script src="/assets/js/angularjs/datatables/angular-datatables.tabletools.min.js"></script>    
    <script src="/assets/js/plugins/raphael/raphael.min.js"></script>    
    <!-- <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script> -->
    <!-- <script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script> -->
    <script src="/assets/js/plugins/morris/morris2.min.js"></script>
    <script src="/assets/js/angularjs/morris/morris.min.js"></script>
    <script src="/assets/js/plugins/summernote/summernote2.min.js"></script>
<!--     <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.js"></script> -->
    <script src="/assets/js/angularjs/summernote/summernote.min.js"></script>
    <script src="https://rawgit.com/eight04/angular-datetime/master/dist/datetime.js"></script>

    <script src="/assets/js/angularjs/fileupload/ng-file-upload.min.js"></script>
    <script src="/assets/js/angularjs/fileupload/ng-file-upload-shim.min.js"></script>

    <script src="/assets/js/angularjs/common/main.js"></script>
<!--     <script src="/assets/js/angularjs/common/config.js"></script>     -->
    <script src="/assets/js/angularjs/common/items.js"></script>
<!--     <script> angular.module("FI").controller("MainCtrl"){


        } </script> -->
    
    <script> angular.module("FI").constant("CSRF_TOKEN", '{{ csrf_token() }}'); </script>
    
    <style>
        .current {
            background-color: #cce7e2;
        }
        .select2-container {
           z-index: 2050;
        } 
        .trans {
          transition:0.5s;
        }
        .hide-div {
            display: none;
        }
    </style>
@endsection

@section('content')
    <div ng-app="FI" ng-controller="MainCtrl">
    <input type="hidden" ng-init="user_id={{ \Auth::user()->id }};profiles={{ json_encode($profiles) }};social_networks={{ json_encode($social_networks) }};pivot_api={{ json_encode($pivot_api)}};morrisChart={{ json_encode($social_accounts_group) }};social_accounts_group={{ json_encode($social_accounts_group) }}"/>
    
<!--<button class="btn btn-primary" ng-click="test()">test</button> -->
    <div ng-if="is_loading">
     <div id ="center" style="position:fixed;top:50%;left:50%;z-index:99999">    
        <div class="sk-spinner sk-spinner-wandering-cubes">
            <div class="sk-cube1"></div>
            <div class="sk-cube2"></div>
        </div>
     </div>
    </div>
    
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Social Accounts</h2>
            <ol class="breadcrumb">
                <li><a href="/">Home</a></li>
                <li class="active"><strong>Social Accounts</strong></li>
            </ol>
        </div>
    </div>
    <!-- ng-if="!is_loading"-->
 <div  ng-cloak ng-show="!is_loading">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-sm-8 trans">
                <div class="ibox">
                    <div class="ibox-content">
                        <div class="clients-list">
                                <div class="pull-right">
                                    <a href="" data-toggle="modal" data-target="#socialAccountCreateModal" class="btn btn-outline btn-success" ><i class="fa fa-plus"></i> Add New</a>
                                </div>
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#tab-1" aria-expanded="true"><i class="fa fa-user"></i> Social Accounts</a></li>
                            </ul>

                            <div class="tab-content">
                                <div id="tab-1" class="tab-pane active">
                                    <div class="table-responsive" >
                                        <table dt-instance="dtInstanceCallback" id="social_accounts_datatable" name="social_accounts_datatable" datatable="ng" dt-options="dtOptions" class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>URL</th>
                                                    <th>Username</th>
                                                    <th>Password</th>
                                                    <th>Social Network</th>
                                                    <th>Actions</th> 
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat="i in items track by $index" 
                                                    ng-click="clickedItemData(i.id)" 
                                                    id="@{{ i.id }}"
                                                    ng-class="{'current': isItemSelected(i) }">
                                                    <td><a data-toggle="modal" data-target="#socialAccountEditModal">@{{ i.first_name }} @{{ i.middle_name }} @{{ i.last_name }}</a></td>
                                                    <td><a href="@{{ i.profile_url }}" target="_blank">@{{ i.profile_url }}</a></td>
                                                    <td id="username_@{{ i.id }}"
                                                        ngclipboard 
                                                        ngclipboard-success="clipboardSuccess(e);" 
                                                        ngclipboard-error="clipboardError(e);"                              
                                                        data-clipboard-target="#username_@{{ i.id }}"><p>@{{ i.username }}</p></td>
                                                    <td id="password_@{{ i.id }}"
                                                        ngclipboard 
                                                        ngclipboard-success="clipboardSuccess(e);" 
                                                        ngclipboard-error="clipboardError(e);"                              
                                                        data-clipboard-target="#password_@{{ i.id }}"><p">@{{ i.password }}</p></td>
                                                    <td><p>@{{ i.social_network }}</p></td>                                                                                                                                                            
                                                    <td>                                                    
                                                        <a type="button" class="btn btn-danger btn-circle btn-outline" data-toggle="modal" data-target="#socialAccountDeleteModal"> <i class="fa fa-times" alt="Delete" ></i></a>                                                        
                                                    </td>
                                                </tr>
                                            </tbody>
                                           <tfoot>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>URL</th>
                                                    <th>Username</th>
                                                    <th>Password</th>
                                                    <th>Social Network</th>
                                                    <th>Actions</th>
                                                </tr>
                                            </tfoot>                                                
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 trans">
                @include('seo.social_accounts.right-panel')
            </div>
        </div>
    </div>
    </div>
               

    <toaster-container></toaster-container>             

<!--     include('seo.social_networks.modals.create') -->
    
    @include('seo.social_accounts.modals.create')
    @include('seo.social_accounts.modals.edit_modified')
    @include('seo.social_accounts.modals.delete')
    <!-- Note Modal -->
    @include('seo.hosts.partials.note_modal')                    
    
@endsection

@section('footer_scripts')
    <script src="/assets/js/plugins/select2/select2.full.min.js"></script>
    <script src="http://webapplayers.com/inspinia_admin/js/plugins/datapicker/bootstrap-datepicker.js"></script>

    
    <script type="text/javascript">        
        $('#social_accounts_select').select2({
            tags:true,
            width: '100%'            
        });


        $('#social_networks_select').select2({
            width: '100%'            
        });

        $('#profiles_select').select2({
            width: '100%'            
        });
    </script>


@endsection