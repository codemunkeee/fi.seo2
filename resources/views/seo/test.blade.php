@extends('app')

@section('header_scripts')
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular-sanitize.js"></script>

    <script src="http://angular-ui.github.io/ui-select/dist/select.js"></script>
    <link rel="stylesheet" href="http://angular-ui.github.io/ui-select/dist/select.css">

    <script>
      
      'use strict';

      var app = angular.module('demo', ['ngSanitize', 'ui.select']);

      app.controller('DemoCtrl', function ($scope, $http, $timeout, $interval) {

      });
    </script>

<!--     <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.css"> -->
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/select2/3.4.5/select2.css">    
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.8.5/css/selectize.default.css">

    <style>
/*        body {
            padding: 15px;
        }
*/
        .select2 > .select2-choice.ui-select-match {
            /* Because of the inclusion of Bootstrap */
            height: 29px;
        }
        .current {
            background-color: #cce7e2;
        }
        .select2-container {
           z-index: 2050;
        } 
        .trans {
          transition:0.5s;
        }
        .hide-div {
            display: none;
        }

        .selectize-control > .selectize-dropdown {
            top: 36px;
        }
        /* Some additional styling to demonstrate that append-to-body helps achieve the proper z-index layering. */
        .select-box {
          background: #fff;
          position: relative;
          z-index: 1;
        }
        .alert-info.positioned {
          margin-top: 1em;
          position: relative;
          z-index: 10000; /* The select2 dropdown has a z-index of 9999 */
        }
    </style>

@endsection

@section('content')

<div class="ng-cloak" ng-app="demo" ng-controller="DemoCtrl as ctrl">
  
  <div ng-init="social_accounts_current={{ json_encode($social_accounts_current)}}; social_accounts_all={{ json_encode($social_accounts_all)}}"></div>

  <ui-select class="form-control" multiple ng-model="social_accounts_current" theme="bootstrap" sortable="true" style="width: 300px;" title="Choose a color">
    <ui-select-match placeholder="Select colors...">@{{$item.name}}</ui-select-match>
    <ui-select-choices repeat="social_account in social_accounts_all">
      @{{social_account.name}}
    </ui-select-choices>
  </ui-select>
  <p>Selected: @{{social_accounts_current}}</p>
  <hr>

</div>
@endsection
  