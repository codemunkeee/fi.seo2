        <div class="col-lg-6 m-b-lg">
	            <div id="vertical-timeline" class="vertical-container light-timeline no-margins">

	                <div class="vertical-timeline-block">
	                    <div class="vertical-timeline-icon blue-bg">
	                        <i class="fa fa-file-text"></i>
	                    </div>

	                    <div class="vertical-timeline-content">
	                    <div>
	                    	@if ($method == 'create')
	                    		{!! Form::open(['url'=>'/urls', 'method'=>'POST']) !!}
	                    	@elseif ($method == 'edit' || $method=='show')
	                        	{!! Form::model($url,  ['url'=>'/urls/'.$url->id, 'method'=>'PUT']) !!}
	                    	@endif
	                            <div class="form-group">
	                            	<label>Url</label> {!! Form::text('url', null, ['placeholder' => 'URL (required)', 'class' => 'form-control', 'required', $disabled ]) !!} 
	                            </div>


								<div class="form-group"><label>Host name : </label> {!! Form::select('host_id', $host_names, (isset($host_name)) ? $host_name->id : null, ['class' => 'form-control', 'placeholder' => 'Select Host Name', $disabled]) !!}</div>
								<div class="form-group"><label>Cpanel Login URL: </label>{!! Form::text('cpanel_login_url', null, ['placeholder' => 'Cpanel Login URL', 'class' => 'form-control', $disabled]) !!}</div>

								<div class="form-group"><label>Cpanel Username: </label>{!! Form::text('cpanel_username', null, ['placeholder' => 'Cpanel Username', 'class' => 'form-control', $disabled]) !!}</div>
							    <div class="form-group"><label>Cpanel Password: </label>{!! Form::text('cpanel_password', null, ['placeholder' => 'Cpanel Password', 'class' => 'form-control', $disabled]) !!}</div>
							    <div class="form-group"><label>FTP Login URL: </label>{!! Form::text('ftp_login_address', null, ['placeholder' => 'FTP Login URL', 'class' => 'form-control', $disabled]) !!}</div>
								<div class="form-group"><label>FTP Username: </label>{!! Form::text('ftp_username', null, ['placeholder' => 'FTP Username', 'class' => 'form-control', $disabled]) !!}</div>
							    <div class="form-group"><label>FTP Password: </label>{!! Form::text('ftp_password', null, ['placeholder' => 'FTP Password', 'class' => 'form-control', $disabled]) !!}</div>
								<div class="form-group"><label>WP Admin Username: </label>{!! Form::text('wp_admin_username', null, ['placeholder' => 'WP Admin  Username', 'class' => 'form-control', $disabled]) !!}</div>
							    <div class="form-group"><label>WP Admin Password: </label>{!! Form::text('wp_admin_password', null, ['placeholder' => 'WP Admin  Password', 'class' => 'form-control', $disabled]) !!}</div>
							    <div class="form-group"><label>Notes: </label>{!! Form::textarea('notes', null, ['placeholder' => 'Notes', 'class' => 'form-control', $disabled]) !!}</div>
							    
							    @if ($method != 'create')

									<div class="row">
										<div class="col-lg-12">
									    	<h1 class="page-content"><small> Other fields </small><h1>
									    </div>
									</div>

									<div class="form-group"><label>Registrar: </label>{!! Form::text('registrar', null, ['placeholder' => 'Registrar', 'class' => 'form-control', $disabled ]) !!}</div> 

									<div class="form-group"><label>Update Date </label>{!! Form::input('date', 'update_date', $url->update_date ? $url->update_date->format('Y-m-d') : null, ['placeholder' => 'Update date:', 'class' => 'form-control', $disabled ]) !!}</div>

									<div class="form-group"><label>Create Date </label>{!! Form::date('create_date', $url->create_date ? $url->create_date->format('Y-m-d') : null, ['placeholder' => 'Create date:', 'class' => 'form-control', $disabled ]) !!}</div>
									
									<div class="row">
										<div class="col-lg-12">
									    	<h1 class="page-header"><small>Registrant Details</small><h1>
									    </div>
									</div>

									<div class="form-group"><label>Name: </label>{!! Form::text('registrant_name', null, ['placeholder' => 'Name', 'class' => 'form-control', $disabled ]) !!}</div> 

									<div class="form-group"><label>Organization: </label>{!! Form::text('registrant_organization', null, ['placeholder' => 'organization', 'class' => 'form-control' , $disabled ]) !!}</div>
									<div class="form-group"><label>Street </label>{!! Form::text('registrant_street', null, ['placeholder' => 'Street', 'class' => 'form-control', $disabled ]) !!}</div>
									<div class="form-group"><label>City </label>{!! Form::text('registrant_city', null, ['placeholder' => 'City', 'class' => 'form-control', $disabled ]) !!}</div>
									<div class="form-group"><label>State </label>{!! Form::text('registrant_state', null, ['placeholder' => 'State', 'class' => 'form-control', $disabled ]) !!}</div>
									<div class="form-group"><label>Zip </label>{!! Form::text('registrant_zip', null, ['placeholder' => 'Zip', 'class' => 'form-control', $disabled ]) !!}</div>
									<div class="form-group"><label>Phone </label>{!! Form::text('registrant_phone', null, ['placeholder' => 'Phone', 'class' => 'form-control', $disabled ]) !!}</div>
									<div class="form-group"><label>Email </label>{!! Form::text('registrant_email', null, ['placeholder' => 'Email', 'class' => 'form-control', $disabled ]) !!}</div>
									<div class="form-group"><label>Nameserver 1 </label>{!! Form::text('registrant_name_server_1', null, ['placeholder' => 'Name Server 1', 'class' => 'form-control', $disabled ]) !!}</div>
									<div class="form-group"><label>Nameserver 2 </label>{!! Form::text('registrant_name_server_2', null, ['placeholder' => 'Name Server 2', 'class' => 'form-control', $disabled ]) !!}</div>
								
								@endif


	                                                                                
	                            <div>
	                            <button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit" {{$disabled}}><strong>Submit</strong></button>
	                            </div>
	                        {!! Form::close() !!}
	                    </div>
			            
			            @if ($method != 'create')
		                    <div>
								<span class="vertical-date">
		                        	<small>Date Created: {{ $url->created_at }}</small>
		                        </span>
		                        <br>
								<span class="vertical-date">
		                        	<small>Date Updated: {{ $url->updated_at }}</small>
		                        </span>
		                    </div>
	                    @endif
	                </div>
	            </div>
	        </div>  
	    </div>