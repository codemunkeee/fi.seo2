@extends('app')

@section('header_scripts')
    <link href="/assets/css/plugins/select2/select2.min.css" rel="stylesheet">
@endsection

@section('content')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Roles</h2>
        <ol class="breadcrumb">
            <li><a href="/">Home</a></li>
            <li><a href="/roles">Roles</a></li>
            @if(isset($role))
                @if($method == 'edit')
                    <li><a href="/roles/{{ $role->id}}">{{ $role->id}}</a></li>
                    <li class="active"><a href="/roles/{{ $role->id}}/edit"><strong>Edit</strong></a></li>
                @else                        
                    <li><a href="/roles/{{ $role->id}}"><strong>{{ $role->id}}</strong></a></li>    
                    <a class="btn btn-warning btn-circle btn-outline pull-right" href="/roles/{{ $role->id}}/edit"><i class="fa fa-edit"></i></a>
                @endif
                <a type="button" class="btn btn-danger btn-circle btn-outline pull-right" data-toggle="modal" data-target="#deleteModal"> <i class="fa fa-trash" alt="Delete" ></i></a>
            @endif
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row m-b-lg m-t-lg">
        <div class="col-md-6">
            <div class="profile-info">
                <div class="">
                    <div>
                        <h2 class="no-margins">
                        @if ($method != 'create')
                            {{ $role->name }}
                        @else
                        	Create a new Role
                        @endif
                        </h2>
                        <small>
                        @if ($method != 'create')
                            Date Created: {{ $role->created_at }}
                        @endif
                        </small>
                    </div>
                </div>
            </div>
        </div>
    </div>

  
	    <div class="row">
	    	@include('seo.roles.form_list')
		
		@if ($method != 'create')
		<!-- tab start -->
        <div class="col-lg-6 m-b-lg">
            <div id="vertical-timeline" class="vertical-container light-timeline no-margins">
                <div class="vertical-timeline-block">
                    <div class="vertical-timeline-icon yellow-bg">
                    	<i class="fa fa-file-text"></i>
                    </div>
                        
                    <div class="vertical-timeline-content">
                    	<h2>Related Fields</h2>
		                <div class="tabs-container">
		                	<ul class="nav nav-tabs">
		                    	<li class="active"><a data-toggle="tab" href="#tab_permissions" aria-expanded="true">Permissions</a></li>		                  
		                    </ul>
		                    <div class="tab-content">
		                    	<div id="tab_permissions" class="tab-pane active">
		                        	<div class="panel-body">  
		                   				@include('seo.roles.roles_permissions.form_list')
		                            </div>
		                        </div>
		                    </div>
		                </div>

                    </div>
                </div>
            </div>  
        </div>
        <!-- end of tab page -->
    	@endif
    </div>
	
</div>

	@if ($method != 'create')
	    <div class="modal inmodal fade" id="deleteModal" tabindex="-1" role="dialog"  aria-hidden="true">
	        <div class="modal-dialog modal-sm">
	            <div class="modal-content">
	                <div class="modal-header">
	                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	                    <h4 class="modal-title">Are you sure you want to delete this?</h4>
	                </div>
	                <div class="modal-body">
	                    <p>Once deleted, it will never be retrieved again.<strong> Are you really sure about this?</strong> </p>
	                </div>
	                <div class="modal-footer">
	                    {!! Form::open(['url'=>'roles/'.$role->id, 'method'=>'DELETE']) !!}
	                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
	                    	<button type="button" class="btn btn-white" data-dismiss="modal">Close</button> 
	                    	<button type="submit" class="btn btn-danger">Delete</button>
	                    {!! Form::close() !!}   
	                </div>
	            </div>
	        </div>
	    </div>   
    @endif
@endsection

@section('footer_scripts')
    <script src="/assets/js/plugins/select2/select2.full.min.js"></script>

	<script type="text/javascript">
	  	$('#permissions').select2();
	</script>


@endsection