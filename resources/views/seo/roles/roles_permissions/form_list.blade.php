@if ($method == 'edit')
	{!! Form::model($role,  ['url'=>'/roles_permissions/' . $role->id, 'method'=>'PUT', 'class' => 'form-control']) !!}
		{{ Form::select('permissions[]', $permissions, isset($permission_role) ? $permission_role : null, ['id' => 'permissions', 'class' => 'form-control', 'multiple' => 'multiple', $disabled]) }}
		{!! Form::submit('Submit',['class'=> 'btn btn-sm btn-primary pull-right m-t-n-xs', $disabled]) !!}	
	{!! Form::close() !!}
@elseif($method == 'show')
	@foreach($permission_role as $p_r)
	<span class="badge badge-primary">{{ \App\Models\Users\Permission::findOrFail($p_r)->name }}</span>
		
	@endforeach
@endif