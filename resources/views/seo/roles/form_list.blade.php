        <div class="col-lg-6 m-b-lg">
	            <div id="vertical-timeline" class="vertical-container light-timeline no-margins">

	                <div class="vertical-timeline-block">
	                    <div class="vertical-timeline-icon blue-bg">
	                        <i class="fa fa-file-text"></i>
	                    </div>

	                    <div class="vertical-timeline-content">
	                    <div>
	                    	@if ($method == 'create')
	                    		{!! Form::open(['url'=>'/roles', 'method'=>'POST']) !!}
	                    	@elseif ($method == 'edit' || $method=='show')
	                        	{!! Form::model($role,  ['url'=>'/roles/'.$role->id, 'method'=>'PUT']) !!}
	                    	@endif
	                            <div class="form-group">
	                            	<label>Name</label> {!! Form::text('name', null, ['class'=>'form-control', $disabled]) !!}
	                            </div>
	                            <div class="form-group">
	                            	<label>Slug</label> {!! Form::text('slug', null, ['class'=>'form-control', $disabled]) !!}
	                            </div>                                    
	                            <div class="form-group">
	                            	<label>Description</label> {!! Form::textarea('description', null, ['class'=>'form-control', $disabled]) !!}
	                            </div>                                                                        
	                            <div>
	                            <button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit" {{$disabled}}><strong>Submit</strong></button>
	                            </div>
	                        {!! Form::close() !!}
	                    </div>
			            
			            @if ($method != 'create')
		                    <div>
								<span class="vertical-date">
		                        	<small>Date Created: {{ $role->created_at }}</small>
		                        </span>
		                        <br>
								<span class="vertical-date">
		                        	<small>Date Updated: {{ $role->updated_at }}</small>
		                        </span>
		                    </div>
	                    @endif
	                </div>
	            </div>
	        </div>  
	    </div>