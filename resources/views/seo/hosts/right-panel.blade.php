<div ng-show="!isItemSet()">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>All Hosts</h5>
            <div class="ibox-tools">
                <a href="" data-toggle="modal" data-target="#hostCreateModal" class="btn btn-outline btn-success">
                    <i class="fa fa-plus"></i> Add New
                </a>
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>
                <a class="close-link">
                    <i class="fa fa-times"></i>
                </a>
            </div>
        </div>
        <div class="ibox-content">
            <div ng-init="morrisChart={{ json_encode($host_group) }}">
                <div
                    donut-chart
                    donut-data='chart'
                    donut-formatter="morrisChartFormatter">
                </div>
            </div>
            <div>
                <table class="table small m-b-xs">
                    <tbody>
                        <tr><td><strong>Total Acct:</strong> {{ count($hosts) }} </td> </tr>                                    
                        <tr><td><strong>Monthly Exp:</strong> ${{ $hosts->sum('hosting_cost') }} </td></tr>
                        <tr><td><strong>Total IPs:</strong> {{ count($ips) }} </td></tr>
                        <tr><td><strong>Total URLs:</strong> {{ count($urls) }} </td></tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div ng-show="isItemSet()">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <div class="row">
                <div class="col-lg-8">
                    <div class="row">
                        <div class="col-lg-12"><h5>@{{ item_details.friendly_name}}</h5></div>
                        <div class="col-lg-12">@{{ item_details.host_name}} - $@{{ item_details.hosting_cost}} / mo. </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div ng-init="edit_item = false" class="ibox-tools">
                        <a class="btn btn-default btn-circle" data-toggle="dropdown" ng-click="edit_item = true">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <a class="btn btn-default btn-circle close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>            
                </div>                
            </div>
    
        </div>
        <div class="ibox-content">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Login Details
                </div>
                <div class="panel-body">
                    <div ng-if="edit_item">
                        <div class="form-group"><label>Login URL: </label>
                            <input placeholder="Login URL" type="text" name="login_url" class="form-control" ng-model="item_details.login_url" >
                        </div>
                        <div class="form-group"><label>Username: </label>
                            <input id="username" 
                            placeholder="Username" type="text" name="username" class="form-control" ng-model="item_details.username">
                        </div>
                        <div class="form-group"><label>Password: </label>
                            <input id="password" 
                            placeholder="Password" type="password" name="password" class="form-control" ng-model="item_details.password">
                        </div>
                        <button type="button" class="btn btn-w-m btn-danger" ng-click="edit_item = false">Cancel</button>
                        <button type="button" class="btn btn-w-m btn-primary pull-right" ng-click="update()">Update</button>
                    </div>
                    <div ng-if="!edit_item">
                        <table class="table medium m-b-xs">
                            <tbody>
                                <tr><td><strong>Login URL:</strong><a href="@{{ item_details.login_url }}" target="_blank">@{{ item_details.login_url }}</a> </td> </tr>                                    
                                <tr><td     ngclipboard 
                                            ngclipboard-success="clipboardSuccess(e);" 
                                            ngclipboard-error="clipboardError(e);"                              
                                            data-clipboard-target="#username_@{{ item_details.id }}">
                                            <strong>Username:</strong> @{{ item_details.username }}</td></tr>
                                <tr><td     ngclipboard 
                                            ngclipboard-success="clipboardSuccess(e);" 
                                            ngclipboard-error="clipboardError(e);"                              
                                            data-clipboard-target="#username_@{{ item_details.id }}">
                                            <strong>Password:</strong> @{{ item_details.password }} </td></tr>
                            </tbody>
                        </table>         
                       
                    </div>
                </div>
            </div>
            <!-- PBN/URLS -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-lg-8">
                            PBN/URLS
                        </div>
                        <div class="col-lg-4">
                              <a href="" data-toggle="modal" data-target="#urlCreateModal" class="btn btn-outline btn-success">
                                <i class="fa fa-plus"></i> Add New
                            </a>
                        </div>                
                    </div>
                </div>
                <div class="panel-body">
                    <div>
                        <table class="table medium m-b-xs">
                            <tbody>
                                <tr ng-repeat="url in pivot.data['urls'].items track by $index">
                                    <td>
                                        <div class="row" ng-click="pivot.method.selected('urls', url)">
                                            <div class="col-lg-8"><a href="@{{ url.url }}" target="_blank">@{{ url.url }}</a></div>
                                            <div class="col-lg-4"><a class="btn btn-danger btn-circle btn-outline pull-right" data-toggle="modal" data-target="#urlDeleteModal"><i class="fa fa-times"></i></a></div> 
                                        </div>

                                    </td>
                                </tr>                                    
                            </tbody>
                        </table>         
                    </div>
                </div>
            </div>


            <!-- IPS -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-lg-8">
                            IPs
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div>
                        <table class="table small m-b-xs table-striped">
                            <tbody>
                                <tr ng-repeat="url in pivot.data['urls'].items track by $index">
                                    <td>@{{ url.ip }}</td>
                                </tr>                                    
                            </tbody>
                        </table>         

                    </div>
                </div>
            </div>

            <!-- NOTES -->
            <div class="panel panel-default">
                <div class="panel-heading">
                <div class="row">
                        <div class="col-lg-8">
                            Notes
                        </div>
                        <div class="col-lg-4">
                              <a href="" data-toggle="modal" data-target="#noteCreateModal" class="btn btn-outline btn-success">
                                <i class="fa fa-plus"></i> Add New
                            </a>
                        </div>                
                    </div>
                </div>
                <div class="panel-body">
                    <div>
                        <table class="table medium m-b-xs">
                            <tbody>
                                <tr ng-repeat="note in pivot.data['notes'].items track by $index" ng-click="pivot.method.selected('notes', note)">
                                    <td>
                                        <div class="row">
                                            <div>
                                                <h2>@{{ note.title }}</h2>
                                                <div ng-bind-html="note.notes"></div>
                                                  <a type="button" class="btn btn-danger btn-circle btn-outline pull-right" data-toggle="modal" data-target="#noteDeleteModal"> <i class="fa fa-times" alt="Delete"></i></a> 
                                                  <a type="button" class="btn btn-warning btn-circle btn-outline pull-right" data-toggle="modal" data-target="#noteEditModal"> <i class="fa fa-edit" alt="Edit"></i></a>                                                        
                                                <span class="vertical-date">
                                                    <small>@{{ note.updated_at }}</small>
                                                </span>
                                            </div>
                                        </div>
                                    </td>
                                </tr>                                    
                            </tbody>
                        </table>         

                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
