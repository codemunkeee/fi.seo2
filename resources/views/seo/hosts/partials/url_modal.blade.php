   <div class="modal inmodal fade" id="urlCreateModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title">Add Url</h4>
                    </div>
                    <div class="modal-body">
                   
                        <div class="form-group">
                            <label>Url</label> {!! Form::text('url', null, ['placeholder' => 'URL (required)', 'class' => 'form-control', 'required', 'ng-model'=>'pivot.data["urls"].new_item.url' ]) !!} 
                        </div>


                        <div class="form-group"><label>Host name : </label> {!! Form::select('host_id', $hosts->lists('friendly_name','id'), null, ['class' => 'form-control', 'placeholder' => 'Select Host Name', 'ng-model'=>'pivot.data["urls"].new_item.host_id']) !!}</div>

                        <div class="form-group"><label>Cpanel Login URL: </label>{!! Form::text('cpanel_login_url', null, ['placeholder' => 'Cpanel Login URL', 'class' => 'form-control', 'ng-model'=>'pivot.data["urls"].new_item.cpanel_login_url']) !!}</div>

                        <div class="form-group"><label>Cpanel Username: </label>{!! Form::text('cpanel_username', null, ['placeholder' => 'Cpanel Username', 'class' => 'form-control', 'ng-model'=>'pivot.data["urls"].new_item.cpanel_username']) !!}</div>
                        <div class="form-group"><label>Cpanel Password: </label>{!! Form::text('cpanel_password', null, ['placeholder' => 'Cpanel Password', 'class' => 'form-control', 'ng-model'=>'pivot.data["urls"].new_item.cpanel_password']) !!}</div>
                        <div class="form-group"><label>FTP Login URL: </label>{!! Form::text('ftp_login_address', null, ['placeholder' => 'FTP Login URL', 'class' => 'form-control', 'ng-model'=>'pivot.data["urls"].new_item.ftp_login_address']) !!}</div>
                        <div class="form-group"><label>FTP Username: </label>{!! Form::text('ftp_username', null, ['placeholder' => 'FTP Username', 'class' => 'form-control', 'ng-model'=>'pivot.data["urls"].new_item.ftp_username']) !!}</div>
                        <div class="form-group"><label>FTP Password: </label>{!! Form::text('ftp_password', null, ['placeholder' => 'FTP Password', 'class' => 'form-control', 'ng-model'=>'pivot.data["urls"].new_item.ftp_password']) !!}</div>
                        <div class="form-group"><label>WP Admin Username: </label>{!! Form::text('wp_admin_username', null, ['placeholder' => 'WP Admin  Username', 'class' => 'form-control', 'ng-model'=>'pivot.data["urls"].new_item.wp_admin_username']) !!}</div>
                        <div class="form-group"><label>WP Admin Password: </label>{!! Form::text('wp_admin_password', null, ['placeholder' => 'WP Admin  Password', 'class' => 'form-control', 'ng-model'=>'pivot.data["urls"].new_item.wp_admin_password']) !!}</div>
                        <div class="form-group"><label>Notes: </label>{!! Form::textarea('notes', null, ['placeholder' => 'Notes', 'class' => 'form-control', 'ng-model'=>'pivot.data["urls"].new_item.notes']) !!}</div>

                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" data-dismiss="modal" ng-click="pivot.method.store('urls')">Save</button>
                    </div>
                </div>
            </div>
        </div>
    