
<div class="modal inmodal" id="hostCreateModal"  role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-md">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <i class="fa fa-clock-o modal-icon"></i>
                <h4 class="modal-title">Add Host</h4>
            </div>
            <div class="modal-body">
                
                <div class="form-group"><label>Host Company: </label>
                    {{ Form::select('host_company', $host_companies_list, null, ['id' => 'host_company', 'class' => 'form-control', 'ng-model'=>'new_item.host_company', 'placeholder'=>'Select a Hosting Company']) }}
                </div>
                <div class="form-group"><label>Hosting Friendly Name: </label>
                    <input placeholder="Friendly Name" type="text" name="friendly_name" class="form-control" ng-model="new_item.friendly_name" >
                </div>
                <div class="form-group"><label>Hosting Name: </label>
                    <input placeholder="Hosting Name" type="text" name="host_name" class="form-control" ng-model="new_item.host_name" >
                </div>
                <div class="form-group"><label>Hosting Cost: </label>
                    <div class="input-group">
                        <span class="input-group-addon">$</span> <input type="number" ng-model="new_item.hosting_cost" class="form-control">
                            <div class="input-group-btn">
                                <button tabindex="-1" class="btn btn-white" type="button">@{{ new_item.payment_type.type }}</button>
                                <button data-toggle="dropdown" class="btn btn-white dropdown-toggle" type="button"><span class="caret"></span></button>
                                <ul class="dropdown-menu pull-right">
                                    <li ng-repeat="payment_type in payment_types" ng-click="setPaymentType(payment_type)"><a href="">@{{ payment_type.type }}</a></li>
                                </ul>
                            </div>
                    </div>
                </div>

                <div class="form-group"><label>Login URL: </label>
                    <input placeholder="Login URL" type="text" name="login_url" class="form-control" ng-model="new_item.login_url" >
                </div>
                <div class="form-group"><label>Username: </label>
                    <input id="username" 
                    placeholder="Username" type="text" name="username" class="form-control" ng-model="new_item.username">
                </div>
                <div class="form-group"><label>Password: </label>
                    <input id="password" 
                    placeholder="Password" type="password" name="password" class="form-control" ng-model="new_item.password">
                </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal" ng-click="submit()">Save</button>
            </div>
        </div>
    </div>
</div>
