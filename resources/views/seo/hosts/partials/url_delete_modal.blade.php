<div class="modal inmodal fade" id="urlDeleteModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">What would you like to do?</h4>
                </div>
                <div class="modal-body" ng-init="">
                    <div class="text-center">   
                        <button type="button" class="btn btn-primary" data-dismiss="modal" ng-click="pivot.method.unlink('urls')"> Remove the URL from this Host</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal" ng-click="pivot.method.delete('urls', pivot.data['urls'].selected)"> Permanently Delete The URL</button>
                    </div>                        
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                    
                </div>
            </div>
        </div>
    </div>
