<div class="modal inmodal" id="noteCreateModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <i class="fa fa-clock-o modal-icon"></i>
                <h4 class="modal-title">Add Notes</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-1"><label>Title:</label></div>
                    <div class="col-lg-11"><input type="text" class="form-control" placeholder="Enter Note Title" ng-model="pivot.data['notes'].new_item.title" /></div>
                </div>

                <summernote ng-model="pivot.data['notes'].new_item.notes"  height="300"></summernote>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal" ng-click="pivot.method.store('notes')">Save</button>
            </div>
        </div>
    </div>
</div>
