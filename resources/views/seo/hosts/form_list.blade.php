@if ($method == 'edit' || $method == 'show')
    <div class="form-group"><label>Hosting Name: </label>
        <input placeholder="Hosting Name" type="text" name="host_name" class="form-control" ng-model="item_details.host_name" required {{$disabled}}>
    </div>
    <div class="form-group"><label>Login URL: </label>
        <input placeholder="Login URL" type="text" name="login_url" class="form-control" ng-model="item_details.login_url" required {{$disabled}}>
    </div>
    <div class="form-group"><label>Username: </label>
        <input id="username" 
        tooltip-placement="top" 
        uib-tooltip="Click to copy" 
        ngclipboard 
        ngclipboard-success="clipboardSuccess(e);" 
        ngclipboard-error="clipboardError(e);"                              
        data-clipboard-target="#username" 
        placeholder="Username" type="text" name="username" class="form-control" ng-model="item_details.username" required {{$disabled}}>
    </div>
    <div class="form-group"><label>Password: </label>
        <input id="password" 
        tooltip-placement="top" 
        uib-tooltip="Click to copy" 
        ngclipboard 
        ngclipboard-success="clipboardSuccess(e);" 
        ngclipboard-error="clipboardError(e);"                                                              
        data-clipboard-target="#password" 
        placeholder="Password" type="text" name="password" class="form-control" ng-model="item_details.password" required {{$disabled}}>
    </div>
    <div>
        <button class="btn btn-sm btn-primary pull-right m-t-n-xs" ng-click="submit()" type="submit" {{$disabled}}><strong>Submit</strong></button>
    </div>
@elseif ($method == 'create')
        <div class="form-group"><label>Hosting Name: </label>
        <input placeholder="Hosting Name" type="text" name="host_name" class="form-control" ng-model="item_details.host_name" required {{$disabled}}>
    </div>
    <div class="form-group"><label>Login URL: </label>
        <input placeholder="Login URL" type="text" name="login_url" class="form-control" ng-model="item_details.login_url" required {{$disabled}}>
    </div>
    <div class="form-group"><label>Username: </label>
        <input id="username" 
        tooltip-placement="top" 
        uib-tooltip="Click to copy" 
        ngclipboard 
        ngclipboard-success="clipboardSuccess(e);" 
        ngclipboard-error="clipboardError(e);"                              
        data-clipboard-target="#username" 
        placeholder="Username" type="text" name="username" class="form-control" ng-model="item_details.username" required {{$disabled}}>
    </div>
    <div class="form-group"><label>Password: </label>
        <input id="password" 
        tooltip-placement="top" 
        uib-tooltip="Click to copy" 
        ngclipboard 
        ngclipboard-success="clipboardSuccess(e);" 
        ngclipboard-error="clipboardError(e);"                                                              
        data-clipboard-target="#password" 
        placeholder="Password" type="text" name="password" class="form-control" ng-model="item_details.password" required {{$disabled}}>
    </div>
    <div>
        <button class="btn btn-sm btn-primary pull-right m-t-n-xs" ng-click="submit()" type="submit" {{$disabled}}><strong>Submit</strong></button>
    </div>
@endif