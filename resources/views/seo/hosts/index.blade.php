@extends('app')

@section('title')
    <title>Fireicetech - Hosts</title>
@endsection

@section('header_scripts')
    <link href="/assets/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
    <link href="/assets/css/plugins/select2/select2.min.css" rel="stylesheet">

    <link href="/assets/css/plugins/summernote/summernote2.min.css" rel="stylesheet">
    
    <script src="/assets/js/plugins/clipboard/clipboard.min.js"></script>
    <script src="/assets/js/plugins/dataTables/datatables.min.js"></script>
    <script src="/assets/js/angularjs/angular.min.js"></script>
    <script src="/assets/js/angularjs/angular-sanitize.min.js"></script>
    <script src="/assets/js/angularjs/angular-animate2.min.js"></script>
    <script src="/assets/js/angularjs/oclazyload/oclazyload.min.js"></script>
    <script src="/assets/js/angularjs/select/select.min.js"></script>
    <script src="/assets/js/angularjs/toastr/toastr.min.js"></script>
    <script src="/assets/js/angularjs/ui/ui-bootstrap.min.js"></script>
    <script src="/assets/js/angularjs/clipboard/ngclipboard.min.js"></script>
    <script src="/assets/js/angularjs/datatables/angular-datatables.min.js"></script>
    <script src="/assets/js/angularjs/datatables/angular-datatables.buttons.min.js"></script>
    <script src="/assets/js/angularjs/datatables/angular-datatables.tabletools.min.js"></script>    
    <script src="/assets/js/plugins/raphael/raphael.min.js"></script>    
    <!-- <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script> -->
    <!-- <script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script> -->
    <script src="/assets/js/plugins/morris/morris2.min.js"></script>
    <script src="/assets/js/angularjs/morris/morris.min.js"></script>
    <script src="/assets/js/plugins/summernote/summernote2.min.js"></script>
<!--     <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.js"></script> -->
    <script src="/assets/js/angularjs/summernote/summernote.min.js"></script>
    <script src="/assets/js/angularjs/fileupload/ng-file-upload.min.js"></script>
    <script src="/assets/js/angularjs/fileupload/ng-file-upload-shim.min.js"></script>


    <script src="/assets/js/angularjs/common/main.js"></script>
    <script src="/assets/js/angularjs/common/items.js"></script>
    <script> angular.module("FI").constant("CSRF_TOKEN", '{{ csrf_token() }}'); </script>
    
    <style>
        .current {
            background-color: #cce7e2;
        }
        .select2-container {
           z-index: 2050;
        } 
    </style>
@endsection

@section('meta-tags')
    <meta name="description" content="Free Web tutorials">
    <meta name="keywords" content="HTML,CSS,XML,JavaScript">
@endsection

@section('content')
    

    <div ng-app="FI" ng-controller="MainCtrl">
    
    <input type="hidden" ng-init="user_id={{ \Auth::user()->id }};pivot_api={{ json_encode($pivot_api)}}"/>
    
    <div ng-if="is_loading">
     <div id ="center" style="position:fixed;top:50%;left:50%;z-index:99999">    
        <div class="sk-spinner sk-spinner-wandering-cubes">
            <div class="sk-cube1"></div>
            <div class="sk-cube2"></div>
        </div>
     </div>
    </div>

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Hosts</h2>
            <ol class="breadcrumb">
                <li><a href="/">Home</a></li>
                <li class="active"><strong>Hosts</strong></li>
            </ol>
        </div>
    </div>

    <div ng-cloak ng-show="!is_loading">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-8">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>All Hosts</h5>
                        <div class="ibox-tools">
                            <a href="" data-toggle="modal" data-target="#hostCreateModal" class="btn btn-outline btn-success" >
                                <i class="fa fa-plus"></i> Add New
                            </a>
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table datatable="ng" dt-options="dtOptions" class="table table-hover">
                                <thead>
                                <tr>
                                    <th>Friendly Name</th>
                                    <th>Hosts</th>
                                    <th>Login URL</th> 
                                    <th>Username</th>
                                    <th>Password</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                      <tr ng-repeat="i in items track by $index" 
                                        ng-click="clickedItemData(i.id)" 
                                        id="@{{ i.id }}"
                                        ng-class="{'current': isItemSelected(i) }"
                                        >   

                                        <td class="tableHide" >@{{ i.friendly_name }}</td>
                                        <td class="tableHide" >@{{ i.host_name }}</td>
                                        <td class="tableHide" ><a href="@{{ i.login_url }}" target="_blank">@{{ i.login_url }}</a></td>
                                        <td  id="username_@{{ i.id }}"
                                            class="tableHide" 
                                            ngclipboard 
                                            ngclipboard-success="clipboardSuccess(e);" 
                                            ngclipboard-error="clipboardError(e);"                              
                                            data-clipboard-target="#username_@{{ i.id }}" >
                                            <p tooltip-placement="top" uib-tooltip="Click to copy" data-container="body">@{{ i.username }}</p>
                                        </td>
                                        <td  class="tableHide"
                                            id="password_@{{ i.id }}"
                                            ngclipboard 
                                            ngclipboard-success="clipboardSuccess(e);" 
                                            ngclipboard-error="clipboardError(e);"                              
                                            data-clipboard-target="#password_@{{ i.id }}" > <p tooltip-placement="top" uib-tooltip="Click to copy" data-container="body">@{{ i.password }}</p></td>
                                        <td class="tableHide" >                                     
                                            <!-- <a href="/hosts/@{{ i.id }}" class="btn btn-primary btn-circle btn-outline" type="button"><i class="fa fa-eye"></i></a>
                                            <a href="/hosts/@{{ i.id }}/edit" class="btn btn-warning btn-circle btn-outline" type="button"><i class="fa fa-edit"></i></a> -->
                                            <a type="button" class="btn btn-danger btn-circle btn-outline" data-toggle="modal" data-target="#hostDeleteModal"> <i class="fa fa-times" alt="Delete" ></i></a>                                            
                                        </td>
                                      </tr>
                                    </tbody>
                                <tfoot>
                                <tr>
                                    <th>Actions</th>
                                    <th>Hosts</th>
                                    <th>Login URL</th> 
                                    <th>Username</th>
                                    <th>Password</th>
                                </tr>
                                </tfoot>
                            </table>

                        </div>

                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                @include('seo.hosts.right-panel')
            </div>
        </div>
    </div>
    </div>
                
    <toaster-container></toaster-container>        

    
    <!-- Host Modal -->
    @include('seo.hosts.partials.host_modal')
    @include('seo.hosts.modals.delete')    
    <!-- Url Modal -->
    @include('seo.hosts.partials.url_modal')
    <!-- Url Delete/Unlink Modal -->
    @include('seo.hosts.partials.url_delete_modal')
    <!-- Note Modal -->
    @include('seo.notes.modals.create')     
    @include('seo.notes.modals.edit')  
    @include('seo.notes.modals.delete')       
    

    <!-- /#page-wrapper -->
        
@endsection

@section('footer_scripts')
    <script src="/assets/js/plugins/select2/select2.full.min.js"></script>

    <script type="text/javascript">
        $('#host_company').select2({
            tags:true,
            width: '100%'            
        });
    </script>

@endsection