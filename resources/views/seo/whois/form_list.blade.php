        <div class="col-lg-6 m-b-lg">
	            <div id="vertical-timeline" class="vertical-container light-timeline no-margins">

	                <div class="vertical-timeline-block">
	                    <div class="vertical-timeline-icon blue-bg">
	                        <i class="fa fa-file-text"></i>
	                    </div>

	                    <div class="vertical-timeline-content">
	                    <div>
	                    	@if ($method == 'create')
	                    		{!! Form::open(['url'=>'/whois', 'method'=>'POST']) !!}
	                    	@elseif ($method == 'edit' || $method=='show')
	                        	{!! Form::model($whois,  ['url'=>'/whois/'.$whois->id, 'method'=>'PUT']) !!}
	                    	@endif
					        @if (\Auth::user()->hasWhois && $method == 'create')
					        	<div>Cannot add another Whois</div>

						    @else
						    	<label>Whois Username: </label>{!! Form::text('username', null, ['placeholder' => 'Whois Username', 'class' => 'form-control', $disabled]) !!}
							    <label>Whois Password: </label>{!! Form::text('password', null, ['placeholder' => 'Whois Password', 'class' => 'form-control', $disabled]) !!}
						    @endif                                                                     
								<div>
	                            	<button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit" {{$disabled}}><strong>Submit</strong></button>
	                            </div>
	                        {!! Form::close() !!}
	                    </div>
			            
			            @if ($method != 'create')
		                    <div>
								<span class="vertical-date">
		                        	<small>Date Created: {{ $whois->created_at }}</small>
		                        </span>
		                        <br>
								<span class="vertical-date">
		                        	<small>Date Updated: {{ $whois->updated_at }}</small>
		                        </span>
		                    </div>
	                    @endif
	                </div>
	            </div>
	        </div>  
	    </div>