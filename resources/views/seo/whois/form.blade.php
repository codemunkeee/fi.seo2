@extends('app')

@section('content')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>whois</h2>
        <ol class="breadcrumb">
            <li><a href="/">Home</a></li>
            <li><a href="/whois">whois</a></li>
            @if(isset($whois))
                @if($method == 'edit')
                    <li><a href="/whois/{{ $whois->id}}">{{ $whois->id}}</a></li>
                    <li class="active"><a href="/whois/{{ $whois->id}}/edit"><strong>Edit</strong></a></li>
                @else                        
                    <li><a href="/whois/{{ $whois->id}}"><strong>{{ $whois->id}}</strong></a></li>    
                    <a class="btn btn-warning btn-circle btn-outline pull-right" href="/whois/{{ $whois->id}}/edit"><i class="fa fa-edit"></i></a>
                @endif
                <a type="button" class="btn btn-danger btn-circle btn-outline pull-right" data-toggle="modal" data-target="#deleteModal"> <i class="fa fa-trash" alt="Delete" ></i></a>
            @endif
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row m-b-lg m-t-lg">
        <div class="col-md-6">
            <div class="profile-info">
                <div class="">
                    <div>
                        <h2 class="no-margins">
                        @if ($method != 'create')
                            {{ $whois->name }}
                        @else
                        	Create a new Whois
                        @endif
                        </h2>
                        <small>
                        @if ($method != 'create')
                            Date Created: {{ $whois->created_at }}
                        @endif
                        </small>
                    </div>
                </div>
            </div>
        </div>
    </div>

  
	<div class="row">
		@include('seo.whois.form_list')
    </div>
	
</div>

	@if ($method != 'create')
	    <div class="modal inmodal fade" id="deleteModal" tabindex="-1" role="dialog"  aria-hidden="true">
	        <div class="modal-dialog modal-sm">
	            <div class="modal-content">
	                <div class="modal-header">
	                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	                    <h4 class="modal-title">Are you sure you want to delete this?</h4>
	                </div>
	                <div class="modal-body">
	                    <p>Once deleted, it will never be retrieved again.<strong> Are you really sure about this?</strong> </p>
	                </div>
	                <div class="modal-footer">
	                    {!! Form::open(['url'=>'whois/'.$whois->id, 'method'=>'DELETE']) !!}
	                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
	                    	<button type="button" class="btn btn-white" data-dismiss="modal">Close</button> 
	                    	<button type="submit" class="btn btn-danger">Delete</button>
	                    {!! Form::close() !!}   
	                </div>
	            </div>
	        </div>
	    </div>   
    @endif
@endsection
