@extends('app')

@section('header_scripts')
    <link href="/assets/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
@endsection

@section('content')
  
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Whois</h2>
            <ol class="breadcrumb">
                <li><a href="/">Home</a></li>
                <li class="active"><strong>Whois</strong></li>
                <a class="btn btn-success btn-circle btn-outline pull-right" href="/whois/create"><i class="fa fa-plus"></i></a>                
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>All Whois</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                    <table id="whoiss" class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                        <th>Actions</th>
                        <th>Username</th>
                        <th>Password</th> 
                    </tr>
                    </thead>
                    <tbody>
                      @foreach($whoiss as $whois) 
                            <tr>    
                                <td>
                                    {!! Form::open(['url'=>'whois/'.$whois->id, 'method'=>'DELETE', 'onsubmit'=>'return confirm("Are you sure you want to delete this?")']) !!}
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">                                     
                                            <a href="/whois/{{ $whois->id }}" class="btn btn-primary btn-circle btn-outline" type="button"><i class="fa fa-eye"></i></a>
                                            <a href="/whois/{{ $whois->id }}/edit" class="btn btn-warning btn-circle btn-outline" type="button"><i class="fa fa-edit"></i></a>
                                            <!-- <a type="button" class="btn btn-danger btn-circle btn-outline" data-toggle="modal" data-target="#deleteModal"> <i class="fa fa-times" alt="Delete" ></i></a>-->              
                                            <button type="submit" class="btn btn-danger btn-circle btn-outline"><i class="fa fa-times" alt="Delete"></i></button>
                                    {!! Form::close() !!}    
                                </td>
                                <td>{{ $whois->username }}</td>
                                <td>{{ $whois->password }}</td> 
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>Actions</th>
                        <th>Username</th>
                        <th>Password</th> 
                    </tr>
                    </tfoot>
                    </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
                  
    <div class="modal inmodal fade" id="deleteModal" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">Are you sure you want to delete this?</h4>
                </div>
                <div class="modal-body">
                    <p>Once deleted, it will never be retrieved again.<strong> Are you really sure about this?</strong> </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>  
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Delete</button>     
                </div>
            </div>
        </div>
    </div>                      
           
    <!-- /#page-wrapper -->
        
@endsection

@section('footer_scripts')

    <script src="/assets/js/plugins/dataTables/datatables.min.js"></script>

    <script>
        $(document).ready(function(){
            $('#whoiss').DataTable({
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'whois-list'},
                    {extend: 'pdf', title: 'whois-list'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

            function changeAttribute(id) {
                console.log(id);
            };

            $('table[data-form="deleteForm"]').on('click', '.form-delete', function(e){
                e.preventDefault();
                var $form=$(this);
                $('#deleteModal').modal({ backdrop: 'static', keyboard: false })
                    .on('click', '#delete-btn', function(){
                        $form.submit();
                    });
            });

            // function delete() {
            //     var request = $.ajax({
            //         url: "ajax.php",
            //         type: "GET",            
            //         dataType: "html"
            //     });

            //     request.done(function(msg) {
            //         $("#mybox").html(msg);          
            //     });

            //     request.fail(function(jqXHR, textStatus) {
            //         alert( "Request failed: " + textStatus );
            //     });
            // };

        });            
    </script>
    <script>
        // function deleteUser(id){
        //     var result  = confirm("Are you sure you want to delete this user?");
        //     var token   = $(this).data('token');
        //     if (result) {
        //         console.log(window.location.href + "/" + id);
        //         $.ajax({
        //           method: "POST",
        //           url: window.location.href + "/" + id,
        //           data: { _method : 'delete', _token : token }
        //         })
        //           .done(function() {
        //             alert( "User Deleted " );
        //         });
        //     }       
        // }
    </script>

@endsection