<div class="modal inmodal" id="noteEditModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <i class="fa fa-clock-o modal-icon"></i>
                <h4 class="modal-title">Edit Note</h4>
            </div>
            <div class="modal-body">
                @include('seo.notes.partials.edit')
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-warning" data-dismiss="modal" ng-click="pivot.method.update('notes', pivot.data['notes'].selected)">Save</button>
            </div>
        </div>
    </div>
</div>
