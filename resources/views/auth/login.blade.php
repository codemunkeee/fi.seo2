<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
                
        <title>Fire&IceTech</title> 
        
        <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="/assets/font-awesome/css/font-awesome.css" rel="stylesheet">

        <!-- Toastr style -->
        <link href="/assets/css/plugins/toastr/toastr.min.css" rel="stylesheet">

        <!-- Gritter -->
<!--        <link href="/assets/js/plugins/gritter/jquery.gritter.css" rel="stylesheet"> -->

        <link href="/assets/css/animate.css" rel="stylesheet">
        <link href="/assets/css/style.css" rel="stylesheet">

        <script src="/assets/js/jquery-2.1.1.js"></script>
        <script src="/assets/js/bootstrap.min.js"></script>

        
    </head>

    <body>
        <div id="wrapper">

             <div class=" middle-box text-center loginscreen animated fadeInDown">
                <div>
                    <div>
                        <h1 class="logo-name">FI</h1>
                    </div>
                        <p>Login in. To see it in action.</p>


                        <form class="m-t" role="form" method="POST" action="{{ url('/auth/login') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group">
                                <input type="email" class="form-control" placeholder="Email Address" required="" name="email" value="{{ old('email') }}">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" placeholder="Password" required="" name="password">
                            </div>
                            <div class="form-group">
                            	<div class="checkbox">
        							<label><input type="checkbox" name="remember"> Remember Me</label>
        						</div>
                            </div>
                            <button type="submit" class="btn btn-primary block full-width m-b">Login</button>

                            <a href="{{ url('/auth/password/email') }}"><small>Forgot password?</small></a>
                            <p class="text-muted text-center"><small>Do not have an account?</small></p>
                            <a class="btn btn-sm btn-white btn-block" href="{{ url('/auth/register') }}">Create an account</a>
        					<p class="text-muted text-center"><small>Or login using</small></p>
                            <div class="row">
        	                    <div class="col-md-6">
        	                    <a class="btn btn-success btn-facebook btn-outline" href="{{ url('/redirect/facebook') }}"><i class="fa fa-facebook"> </i>Facebook</a>
        	                    </div>
        	                    <div class="col-md-6">
        	                    <a class="btn btn-danger btn-google btn-outline" href="{{ url('/redirect/google') }}"><i class="fa fa-google"> </i>Google+</a>
        						</div>
        					</div>				
                        </form>
                    
                    <p class="m-t"> <small>Powered by Fire&IceTech</small> </p>
                </div>
            </div>
        </div>
        
        <!-- Mainly scripts -->
        <script src="/assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
        <script src="/assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

        <!-- Custom and plugin javascript -->
        <script src="/assets/js/inspinia.js"></script>
        <script src="/assets/js/plugins/pace/pace.min.js"></script>

        <!-- Toastr -->
        <script src="/assets/js/plugins/toastr/toastr.min.js"></script>        
    </body>
