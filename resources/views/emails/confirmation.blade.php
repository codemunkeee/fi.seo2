<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <h2>Confirm account</h2>

        <div>
            {!! Html::link('/auth/confirm/'.$confirmation_code, 'Click here') !!} to verify your account.
        </div>

    </body>
</html>