<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('/', function () {

//     return view('welcome');
// });

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

// Route::controllers([
//     'auth' => '\App\Http\Controllers\Auth\AuthController',
//     'password' => '\App\Http\Controllers\Auth\PasswordController',
// ]);

//use App\Models\SEO\SocialAccount;

	Route::group(['middleware' => ['web']], function () {
		Route::get('test', function(){
			$user = \App\Models\User::select('id')->where('id', '=', 3)->first();
			dd(\Gravatar::src(\Auth::user()->email));
			return 'test';
			// $social_accounts_all = SocialAccount::all();

			// //$social_accounts_current = ["1", "23", "25"];
	  //       $social_accounts_current = \Auth::user()->socialAccounts()->get();//->lists('id')->all();

			// return view('seo.test', compact('social_accounts_all', 'social_accounts_current'));
		});

	    Route::get('/', function(){
	    	return \Auth::check() ? view('app') : redirect('/auth/login');
	    });

	    //Social authentication
	    Route::get('/redirect/{provider}', 'SocialAuthController@redirect');
		Route::get('/callback/{provider}', 'SocialAuthController@callback');

	});


	Route::group(['prefix' => 'auth','middleware' => ['web']], function() {
		// email registration
		Route::auth();
		//password resets
		Route::get('/password/email', '\App\Http\Controllers\Auth\PasswordController@getEmail');
		Route::post('/password/email', '\App\Http\Controllers\Auth\PasswordController@postEmail');
		
		Route::get('/password/reset/{token}', '\App\Http\Controllers\Auth\PasswordController@getReset');
		Route::post('/password/reset/{token}', '\App\Http\Controllers\Auth\PasswordController@postReset');
		
		//confirm user
		Route::get('/confirm/resend/{id}', '\App\Http\Controllers\Auth\ConfirmEmailController@resendEmail');
		Route::get('/confirm/{token}', '\App\Http\Controllers\Auth\ConfirmEmailController@show');
	});

	/**
	 * USERS
	 */
	Route::group(['prefix' => 'users','middleware' => ['web']], function() {
		Route::get('api/filter', '\App\Http\Controllers\Users\UsersController@api_filter');
		Route::get('api/sort/{field}/{type}', '\App\Http\Controllers\Users\UsersController@api_sort');
		Route::get('api/search', '\App\Http\Controllers\Users\UsersController@api_search');

		Route::get('/', '\App\Http\Controllers\Users\UsersController@index');
		Route::get('/{id}', '\App\Http\Controllers\Users\UsersController@show');
		Route::get('/{id}/edit', '\App\Http\Controllers\Users\UsersController@edit');
		Route::put('/{id}', '\App\Http\Controllers\Users\UsersController@update');
		Route::delete('/{id}', '\App\Http\Controllers\Users\UsersController@destroy');
	});


	/**
	 * URLS
	 */
	Route::group(['prefix' => 'urls', 'middleware' => ['web']], function () {
		Route::get('test/host_urls/{host_id}', '\App\Http\Controllers\Users\UsersUrlController@host_urls');
		Route::post('link_multiple/{profile_id}', '\App\Http\Controllers\Users\UsersUrlController@linkMultiple');

		Route::get('api/filter', '\App\Http\Controllers\Users\UsersUrlController@api_filter');
		Route::get('api/sort/{field}/{type}', '\App\Http\Controllers\Users\UsersUrlController@api_sort');
		Route::get('api/search', '\App\Http\Controllers\Users\UsersUrlController@api_search');
		
		Route::post('unlink/{pivot_table_name}/{pivot_id}', '\App\Http\Controllers\Users\UsersUrlController@unlink');

		Route::get('/', '\App\Http\Controllers\Users\UsersUrlController@index');
		Route::post('/', '\App\Http\Controllers\Users\UsersUrlController@store');
		Route::get('create', '\App\Http\Controllers\Users\UsersUrlController@create');		
		Route::get('{urls}', '\App\Http\Controllers\Users\UsersUrlController@show');
		Route::get('{urls}/edit', '\App\Http\Controllers\Users\UsersUrlController@edit');
		Route::put('{urls}', '\App\Http\Controllers\Users\UsersUrlController@update');
		Route::delete('{urls}', '\App\Http\Controllers\Users\UsersUrlController@destroy');

	});


	/**
	 * HOSTS
	 */
	Route::group(['prefix' => 'hosts','middleware' => ['web']], function() {
		Route::get('api/filter', '\App\Http\Controllers\Users\UsersHostController@api_filter');
		Route::get('api/sort/{field}/{type}', '\App\Http\Controllers\Users\UsersHostController@api_sort');
		Route::get('api/search', '\App\Http\Controllers\Users\UsersHostController@api_search');
						

		Route::get('/', '\App\Http\Controllers\Users\UsersHostController@index');
		Route::post('/', '\App\Http\Controllers\Users\UsersHostController@store');
		Route::get('create', '\App\Http\Controllers\Users\UsersHostController@create');		
		Route::get('{hosts}', '\App\Http\Controllers\Users\UsersHostController@show');
		Route::get('{hosts}/edit', '\App\Http\Controllers\Users\UsersHostController@edit');
		Route::put('{hosts}', '\App\Http\Controllers\Users\UsersHostController@update');
		Route::delete('{hosts}', '\App\Http\Controllers\Users\UsersHostController@destroy');
	});

	/**
	 * WHOIS
	 */
	Route::group(['prefix' => 'whois','middleware' => ['web']], function() {
		Route::get('api/filter', '\App\Http\Controllers\Users\UsersWhoisController@api_filter');
		Route::get('api/sort/{field}/{type}', '\App\Http\Controllers\Users\UsersWhoisController@api_sort');
		Route::get('api/search', '\App\Http\Controllers\Users\UsersWhoisController@api_search');
								
		Route::get('/', '\App\Http\Controllers\Users\UsersWhoisController@index');
		Route::post('/', '\App\Http\Controllers\Users\UsersWhoisController@store');
		Route::get('create', '\App\Http\Controllers\Users\UsersWhoisController@create');		
		Route::get('{whois}', '\App\Http\Controllers\Users\UsersWhoisController@show');
		Route::get('{whois}/edit', '\App\Http\Controllers\Users\UsersWhoisController@edit');
		Route::put('{whois}', '\App\Http\Controllers\Users\UsersWhoisController@update');
		Route::delete('{whois}', '\App\Http\Controllers\Users\UsersWhoisController@destroy');
	});

	/**
	 * PERMISSIONS
	 */
	Route::group(['prefix' => 'permissions','middleware' => ['web']], function() {
		Route::get('api/filter', '\App\Http\Controllers\Users\UsersPermissionsController@api_filter');
		Route::get('api/sort/{field}/{type}', '\App\Http\Controllers\Users\UsersPermissionsController@api_sort');
		Route::get('api/search', '\App\Http\Controllers\Users\UsersPermissionsController@api_search');
						
		Route::get('/', '\App\Http\Controllers\Users\UsersPermissionsController@index');
		Route::post('/', '\App\Http\Controllers\Users\UsersPermissionsController@store');
		Route::get('create', '\App\Http\Controllers\Users\UsersPermissionsController@create');		
		Route::get('{permissions}', '\App\Http\Controllers\Users\UsersPermissionsController@show');
		Route::get('{permissions}/edit', '\App\Http\Controllers\Users\UsersPermissionsController@edit');
		Route::put('{permissions}', '\App\Http\Controllers\Users\UsersPermissionsController@update');
		Route::delete('{permissions}', '\App\Http\Controllers\Users\UsersPermissionsController@destroy');

	});

	/**
	 * ROLES
	 */
	Route::group(['prefix' => 'roles','middleware' => ['web']], function() {
		Route::get('api/filter', '\App\Http\Controllers\Users\UsersRolesController@api_filter');
		Route::get('api/sort/{field}/{type}', '\App\Http\Controllers\Users\UsersRolesController@api_sort');
		Route::get('api/search', '\App\Http\Controllers\Users\UsersRolesController@api_search');
						
		Route::get('/', '\App\Http\Controllers\Users\UsersRolesController@index');
		Route::post('/', '\App\Http\Controllers\Users\UsersRolesController@store');
		Route::get('/create', '\App\Http\Controllers\Users\UsersRolesController@create');		
		Route::get('{roles}', '\App\Http\Controllers\Users\UsersRolesController@show');
		Route::get('{roles}/edit', '\App\Http\Controllers\Users\UsersRolesController@edit');
		Route::put('{roles}', '\App\Http\Controllers\Users\UsersRolesController@update');
		Route::delete('{roles}', '\App\Http\Controllers\Users\UsersRolesController@destroy');

	});

	/**
	 * SOCIAL_NETWORKS
	 */
	Route::group(['prefix' => 'social_networks','middleware' => ['web']], function() {
		// Route::get('profile_social_accounts/{profile_id}', '\App\Http\Controllers\Users\UsersSocialAccountsController@profiles_social_accounts');
		
		Route::get('api/filter', '\App\Http\Controllers\Users\UsersSocialNetworksController@api_filter');
		Route::get('api/sort/{field}/{type}', '\App\Http\Controllers\Users\UsersSocialNetworksController@api_sort');
		Route::get('api/search', '\App\Http\Controllers\Users\UsersSocialNetworksController@api_search');
		
		Route::get('/', '\App\Http\Controllers\Users\UsersSocialNetworksController@index');
		Route::post('/', '\App\Http\Controllers\Users\UsersSocialNetworksController@store');
		Route::get('get', '\App\Http\Controllers\Users\UsersSocialNetworksController@get');			
		Route::get('create', '\App\Http\Controllers\Users\UsersSocialNetworksController@create');		
		Route::get('{social_networks}', '\App\Http\Controllers\Users\UsersSocialNetworksController@show');
		Route::get('{social_networks}/edit', '\App\Http\Controllers\Users\UsersSocialNetworksController@edit');
		Route::put('{social_networks}', '\App\Http\Controllers\Users\UsersSocialNetworksController@update');
		Route::delete('{social_networks}', '\App\Http\Controllers\Users\UsersSocialNetworksController@destroy');

		
		// Route::get('{social_networks}/social_accounts', '\App\Http\Controllers\SocialNetworks\SocialNetworksProfilesController@show');
		Route::get('{social_networks}/social_accounts/get', '\App\Http\Controllers\SocialNetworks\SocialNetworksAccountsController@get');


		Route::get('{social_networks}/profiles', '\App\Http\Controllers\SocialNetworks\SocialNetworksProfilesController@show');
		Route::get('{social_networks}/profiles/get', '\App\Http\Controllers\SocialNetworks\SocialNetworksProfilesController@get');
		// Route::get('{profiles}/social_accounts', '\App\Http\Controllers\Profiles\ProfilesSocialAccountsController@get');	
	});

	/**
	 * SOCIAL_ACCOUNTS
	 */
	Route::group(['prefix' => 'social_accounts','middleware' => ['web']], function() {
		Route::get('api/filter', '\App\Http\Controllers\Users\UsersSocialAccountsController@api_filter');
		
		Route::get('/', '\App\Http\Controllers\Users\UsersSocialAccountsController@index');
		Route::post('/', '\App\Http\Controllers\Users\UsersSocialAccountsController@store');		
		Route::put('{social_accounts}', '\App\Http\Controllers\Users\UsersSocialAccountsController@update');
		Route::delete('{social_accounts}', '\App\Http\Controllers\Users\UsersSocialAccountsController@destroy');
		Route::get('get', '\App\Http\Controllers\Users\UsersSocialAccountsController@get');			
		
	});


	/**
	 * PROFILES
	 */
	Route::group(['prefix' => 'profiles','middleware' => ['web']], function() {
		Route::get('api/filter', '\App\Http\Controllers\Users\UsersProfilesController@api_filter');
		Route::get('api/sort/{field}/{type}', '\App\Http\Controllers\Users\UsersProfilesController@api_sort');
		Route::get('api/search', '\App\Http\Controllers\Users\UsersProfilesController@api_search');
						

		Route::get('/', '\App\Http\Controllers\Users\UsersProfilesController@index');
		Route::post('/', '\App\Http\Controllers\Users\UsersProfilesController@store');
		Route::get('create', '\App\Http\Controllers\Users\UsersProfilesController@create');		
		Route::get('{profiles}', '\App\Http\Controllers\Users\UsersProfilesController@show');
		Route::get('{profiles}/get', '\App\Http\Controllers\Users\UsersProfilesController@get');
		Route::get('{profiles}/edit', '\App\Http\Controllers\Users\UsersProfilesController@edit');
		Route::put('{profiles}', '\App\Http\Controllers\Users\UsersProfilesController@update');
		Route::delete('{profiles}', '\App\Http\Controllers\Users\UsersProfilesController@destroy');

		Route::get('{profiles}/social_accounts', '\App\Http\Controllers\Profiles\ProfilesSocialAccountsController@get');		
		Route::get('{profiles}/social_accounts/unselected_list', '\App\Http\Controllers\Profiles\ProfilesSocialAccountsController@unselectedList');
		Route::post('{profiles}/social_accounts/link_multiple', '\App\Http\Controllers\Profiles\ProfilesSocialAccountsController@linkMultiple');		

		Route::get('{profiles}/urls', '\App\Http\Controllers\Profiles\ProfilesUrlsController@get');		
		Route::get('{profiles}/urls/unselected_list', '\App\Http\Controllers\Profiles\ProfilesUrlsController@unselectedList');
		Route::post('{profiles}/urls/link_multiple', '\App\Http\Controllers\Profiles\ProfilesUrlsController@linkMultiple');
		
	});

	/**
	 * PROFILE 
	 * for single profile only
	 */
	Route::group(['prefix' => 'profile','middleware' => ['web']], function() {
		Route::get('{profile_name}/{profile_id}', '\App\Http\Controllers\Profiles\ProfileController@show');
		Route::get('{profile_name}/{profile_id}/get', '\App\Http\Controllers\Profiles\ProfileController@get');
	});


	/**
	 * NOTES
	 */
	Route::group(['prefix' => 'notes','middleware' => ['web']], function() {
		Route::get('/{parent}/{parent_id}', '\App\Http\Controllers\Users\UsersNotesController@api');
		Route::post('/', '\App\Http\Controllers\Users\UsersNotesController@store');
		Route::put('/{notes}', '\App\Http\Controllers\Users\UsersNotesController@update');
		Route::delete('/{notes}', '\App\Http\Controllers\Users\UsersNotesController@destroy');				

	});



	Route::group(['middleware' => ['web']], function () {

		Route::resource('roles_permissions', '\App\Http\Controllers\Roles\RolesPermissionsController');	

		//Route::get('profiles_social_accounts/get/{profile_id}', '\App\Http\Controllers\Profiles\ProfilesSocialAccountsController@get');
		Route::resource('profiles_social_accounts', '\App\Http\Controllers\Profiles\ProfilesSocialAccountsController');

		Route::resource('profile_urls', '\App\Http\Controllers\Profiles\ProfilesUrlsController');	

	});

	Route::group(['prefix' => 'partial_views','middleware' => ['web']], function() {
		Route::get('/profiles/{id}/{field}', '\App\Http\Controllers\Users\UsersProfilesController@test');

	});

	Route::group(['prefix' => 'api','middleware' => ['web']], function() {


		Route::get('/{table}', '\App\Http\Controllers\Admin\ApiController@api');
		Route::get('/{table}/select', '\App\Http\Controllers\Admin\ApiController@api_select');
		Route::get('/{table}/filter/{keyword}', '\App\Http\Controllers\Admin\ApiController@filter');
		Route::get('/{table}/sort/{column}/{type}', '\App\Http\Controllers\Admin\ApiController@sort');
		Route::get('/{table}/search/{keyword}', '\App\Http\Controllers\Admin\ApiController@search');

		Route::get('/{table}/{id}/decrypted', '\App\Http\Controllers\Admin\ApiController@api_decrypted');
		Route::get('/{table}/{column}/{id}', '\App\Http\Controllers\Admin\ApiController@api_filter');
		//Route::get('/{table}/{field?}', '\App\Http\Controllers\Admin\ApiController@api_test');

		///api/{table}?params

	});	

	
	Route::get('help', '\App\Http\Controllers\Others\HelpController@index');

	