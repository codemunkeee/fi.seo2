<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/', function () {

//    return view('welcome');
//});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

// Route::controllers([
//     'auth' => '\App\Http\Controllers\Auth\AuthController',
//     'password' => '\App\Http\Controllers\Auth\PasswordController',
// ]);

Route::group(['middleware' => ['web']], function () {
    
    Route::get('/', function(){
    	return view('app');
    });

    //Social authentication
    Route::get('/redirect/{provider}', 'SocialAuthController@redirect');
	Route::get('/callback/{provider}', 'SocialAuthController@callback');

});

Route::group(['prefix' => 'users','middleware' => ['web']], function() {

	Route::get('/', '\App\Http\Controllers\Users\UsersController@index');
	Route::get('/{id}', '\App\Http\Controllers\Users\UsersController@show');
	Route::get('/{id}/edit', '\App\Http\Controllers\Users\UsersController@edit');
	Route::put('/{id}', '\App\Http\Controllers\Users\UsersController@update');
	Route::delete('/{id}', '\App\Http\Controllers\Users\UsersController@destroy');
	
});


Route::group(['prefix' => 'auth','middleware' => ['web']], function() {
	// email registration
	Route::auth();
	//password resets
	Route::get('/password/email', '\App\Http\Controllers\Auth\PasswordController@getEmail');
	Route::post('/password/email', '\App\Http\Controllers\Auth\PasswordController@postEmail');
	Route::get('/password/reset/{token}', '\App\Http\Controllers\Auth\PasswordController@getReset');
	Route::post('/password/reset/{token}', '\App\Http\Controllers\Auth\PasswordController@postReset');
	
	//confirm user
	Route::get('/confirm/resend/{id}', '\App\Http\Controllers\Auth\ConfirmEmailController@resendEmail');
	Route::get('/confirm/{token}', '\App\Http\Controllers\Auth\ConfirmEmailController@show');

});
// Route::group(['middleware' => 'web'], function () {
//     Route::auth();

//     Route::get('/home', 'HomeController@index');
// });
