<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Models\UserConfirmation;
use App\Http\Controllers\Controller;

class ConfirmEmailController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the confirm email.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($token)
    {
        $user = \Auth::user();

        if ($user->confirm()->first()->confirmation_code === $token){
            \DB::table('user_confirmations')->where('user_id', $user->id)->update(['confirmed' => 1]);//UserConfirmation::whereUserId(12)->first();

        }

        \Flash::success('User confirmed!');
        return redirect('/users/'.$user->id);
    }


    public function resendEmail($id){
        $user = \Auth::user();

        //send confirmation email
        \Mail::send('emails.confirmation', ['confirmation_code'=>$user->confirm()->first()->confirmation_code], function ($m) use($user) {
            $m->from('tryzccount@gmail.com', 'Fire&IceTech');
            $m->to($user->email, $user->first_name . ' ' . $user->last_name)->subject('Confirm your email!');
        }); 
        
        \Flash::success('Email confirmation sent!');
        return redirect('/users/'.$user->id);
    }
}
