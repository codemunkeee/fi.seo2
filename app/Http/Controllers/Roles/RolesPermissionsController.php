<?php

namespace App\Http\Controllers\Roles;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Role;
use DB;
use Gate;

class RolesPermissionsController extends Controller
{

	public function __construct(){
		  $this->middleware('auth');
      $this->user = \Auth::user();
	}

    public function update(Request $request, $id){
       if (Gate::denies('admin')) {
            return redirect('/')->withErrors('Permission denied.');
      }
       $role = Role::findOrFail($id);

       if (isset($request['permissions'])) {
           $role->permissions()->sync($request['permissions']);
       } else {
            $role->permissions()->detach();
       }

        \Flash::success('Persona was updated!');
        return redirect('/roles/'.$role->id . '/edit');
    
    }

}
