<?php

namespace App\Http\Controllers\Profiles;

use DB;
use App\Http\Requests;
use App\Models\SEO\Profile;
use Illuminate\Http\Request;
use App\Models\SEO\ProfileUrl;
use App\Http\Controllers\Controller;

//use App\Http\Requests\Users\UsersPersonasRequest;

//use App\Packages\Images\Image;


class ProfilesUrlsController extends Controller
{
    /*
     * Initialize the controller 
     */
    public function __construct(){
        $this->middleware('auth');
        $this->user = \Auth::user();   
    }

    public function get($profile_id){

        return DB::table('profile_urls')
            ->select("urls.*", "urls.id as url_id", "profile_urls.id as id")
            ->join('profiles', 'profile_urls.profile_id', '=', 'profiles.id')
            ->join('urls', 'profile_urls.url_id', '=', 'urls.id')
            ->where('profiles.id', '=', $profile_id)
            ->where('urls.ip', '!=', 'No IP Found')
            ->get();
    }
 
    /**
     * Update a linked url notes to a persona
     * 
     * @param  Request $request : data sent by the user to be updated
     * @param  int $id : the persona id
     * @return view
     */
    // public function update(Request $request, $id){
    //     $persona      = Persona::findOrFail($id);
    //      DB::transaction(function () use($request, $persona, $id) {
    //         is_null($request['urls']) ? $persona->urls()->sync([]) : $persona->urls()->sync($request['urls']);
            
    //         $this->user->personas()->updateExistingPivot($id, ['notes_urls' => $request['notes_urls']]);

    //     });

    //     \Flash::success('Persona was updated!');
    //     return redirect('/personas/'.$persona->id . '/edit');
    // }

    public function destroy($id){
        ProfileUrl::destroy($id);
        return 'success';
    }

    public function unselectedList($profile_id){
        $profile = Profile::findOrFail($profile_id);
        return $profile->unselectedUrls()->get(); 
    }

    /**
     * Link multiple URLs to a PROFILE
     * @param  [type] $profile_id [description]
     * @return [type]             [description]
     */
    public function linkMultiple(Request $request, $profile_id){
        //needs error checking
        $profile = Profile::findOrFail($profile_id);
        $profile->urls()->attach($request->all());
        return "success";

    }

    public function urls_test(){
        
    }
}