<?php

namespace App\Http\Controllers\Profiles;

use DB;
use Gate;
use App\Http\Requests;
use App\Models\SEO\Profile;
use Illuminate\Http\Request;
use App\Models\SEO\SocialAccount;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\SEO\ProfileRequest;


class ProfileController extends Controller
{
    /*
     * Initialize the controller 
     */  
	public function __construct(){
        $this->middleware('auth');
        //$this->middleware('auth');
        //$this->user = \Auth::user();       
	}

    public function get($name, $id){
        return Profile::findOrFail($id);
    }
    
    /**
     * Show details for profile
     *   
     * @param  int $id : the profile id
     * @return view
     */
    public function show($name, $id){

        $method   = 'show';
        $disabled = 'disabled';

        $disabled = '';
        $profile  = Profile::findOrFail($id);

        $pivot_api = [
                        'parent_name' => 'profiles',
                        'profile_social_accounts' => [ 'get' => '/profiles/{id}/social_accounts'],     
                        'profile_urls' => [ 'get' => '/profiles/{id}/urls']             
                    ];

        $sanitized_name = $this->sanitizeName($profile->first_name . $profile->middle_name . $profile->last_name);
        //check if profile_name is equal to the retrieved profile 
        if (!($name === $sanitized_name)) {
            return redirect('/')->withErrors('You are not allowed to view this page.');
        }

        $profile_social_accounts = $profile->socialAccountsDecrypted();
        //dd(count($profile_social_accounts));
        $profile_urls = $profile->urls()->get();        

        return view ('seo.profile.form', compact('method', 'disabled', 'profile', 'profile_social_accounts', 'profile_urls', 'pivot_api', 'sanitized_name'));        

    }

    public function sanitizeName($name){
        $name = strtolower($name);
        $name = str_replace(' ', '', $name);
        return $name;
    }


}