<?php

namespace App\Http\Controllers\Profiles;

use App\Http\Requests\Users\UsersProfilesRequest;
use App\Models\SEO\ProfileSocialAccount;
use App\Packages\Helpers\RequestParser;
use App\Http\Controllers\Controller;
use App\Models\SEO\SocialAccount;
use Illuminate\Http\Request;
use App\Models\SEO\Profile;
use App\Http\Requests;
use Carbon\Carbon;
use DB;

class ProfilesSocialAccountsController extends Controller
{

	public function __construct(){
		$this->middleware('auth');
        $this->user = \Auth::user();    
	}


    public function get($profile_id){
        $profile = Profile::findOrFail($profile_id);

        return $profile->socialAccountsDecrypted();  
    }
    

    /**
     * Link a SOCIAL_ACCOUNT to a PROFILE
     * @param  Request $request : User's request
     * @param  [type]  $id      : PROFILE Id
     * @return [type]           [description]
     */
    public function link(Request $request, $id){

    }

    /**
     * Link multiple SOCIAL_ACCOUNTs to a PROFILE
     * @param  Request $request : User's request
     * @param  [type]  $id      : PROFILE Id
     * @return [type]           [description]
     */
    public function linkMultiple(Request $request, $profile_id){
        $profile = Profile::findOrFail($profile_id);
        $profile->socialNetworks()->attach($request->all());
        return "success";
    }

    public function update(Request $request, $id){
        $profile_social_account = ProfileSocialAccount::findOrFail($request['pivot']['id']);
        $profile_social_account->username    = $request['pivot']['username'];
        $profile_social_account->password    = $request['pivot']['password'];
        $profile_social_account->profile_url = $request['pivot']['profile_url'];
        $profile_social_account->save();
        return $profile_social_account;
    }

    public function updateMultiple(Request $request, $id){
        
        $profile = Profile::findOrFail($id);            
        
        DB::transaction(function () use($request, $profile, $id) {
            $now                        = Carbon::now();  
            $profile_social_accounts    = $profile->socialAccounts()->get()->keyBy('id');
            $user_social_accounts       = $this->user->socialAccounts()->get()->keyBy('id');
            $social_accounts_insert     = [];

            foreach ($request->items as $key=>$value){
                //check if social account already exists on the profile
                $check = $profile_social_accounts->get($value['social_network_id']);
                if (is_null($check)){ // insert 
                    $profile_social_account = [
                        'profile_id'        => $profile->id,
                        'social_network_id' => $value['social_network_id'],
                        'username'          => \Crypt::encrypt($value['username']),
                        'password'          => \Crypt::encrypt($value['password']),
                        'profile_url'       => \Crypt::encrypt($value['profile_url']),                        
                        'created_at'        => $now,
                        'updated_at'        => $now
                    ];
                    array_push($social_accounts_insert, $profile_social_account); 
                } else { //update
                    $profile_social_account = [
                        'username'          => \Crypt::encrypt($value['username']),
                        'password'          => \Crypt::encrypt($value['password']),
                        'profile_url'       => \Crypt::encrypt($value['profile_url'])  
                    ];

                    // update social accounts for this profile
                    $profile->socialAccounts()->updateExistingPivot($value['social_network_id'], $profile_social_account);
                }
            }

            if (count($social_accounts_insert)>0){
                \App\Models\SEO\ProfileSocialAccount::insert($social_accounts_insert);
            }

            $this->user->profiles()->updateExistingPivot($id, ['notes_social_networks' => $request->notes]);

        });

    	// \Flash::success('profile was updated!');
    	// return redirect('/profiles/'.$profile->id . '/edit');
    }

    public function destroy($id){
        ProfileSocialAccount::destroy($id);
        return $id;
    }

    public function unselectedList($profile_id){
        $profile = Profile::findOrFail($profile_id);
        $id = $profile_id;

       try {
            return DB::table('user_social_networks')
                ->whereNotIn('user_social_networks.social_network_id', function($q) use ($profile_id) {
                    $q->select('profile_social_accounts.social_network_id as social_network_id')->from('profile_social_accounts')
                    ->where('profile_social_accounts.profile_id', '=', $profile_id);
                })->join('social_networks', 'user_social_networks.social_network_id', '=', 'social_networks.id')
                ->where('user_social_networks.user_id', '=', \Auth::user()->id)
                ->select('social_networks.*')->get();

            //return $profile->unselectedSocialNetworks()->get();       
        } 
        catch(\Exception $e){
            return $e;
        }        

    }

}