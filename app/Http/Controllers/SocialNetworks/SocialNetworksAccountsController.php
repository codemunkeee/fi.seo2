<?php

namespace App\Http\Controllers\SocialNetworks;

use App\Packages\Helpers\RequestParser;
use App\Http\Controllers\Controller;
use App\Models\SEO\SocialNetwork;
use Illuminate\Http\Request;
use App\Http\Requests;
use Carbon\Carbon;
use DB;

class SocialNetworksAccountsController extends Controller
{

	public function __construct(){
		$this->middleware('auth');
        $this->user = \Auth::user();    
	}

    /**
     * Get the PROFILES_SOCIAL_ACCOUNTS of a SOCIAL_NETWORK
     * @param  Request $request : User's request
     * @param  Int  $id      : SOCIAL NETWORK Id
     * @return Array
     */ 
    public function get($social_network_id){
        $social_network = SocialNetwork::findOrFail($social_network_id);
        return $social_network->socialAccountsDecrypted();
    }
    
  

}
