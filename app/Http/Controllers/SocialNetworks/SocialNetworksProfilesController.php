<?php

namespace App\Http\Controllers\SocialNetworks;

use App\Packages\Helpers\RequestParser;
use App\Http\Controllers\Controller;
use App\Models\SEO\SocialNetwork;
use Illuminate\Http\Request;
use App\Http\Requests;
use Carbon\Carbon;
use DB;

class SocialNetworksProfilesController extends Controller
{

	public function __construct(){
		$this->middleware('auth');
        $this->user = \Auth::user();    
	}

    /**
     * Get the PROFILES of a SOCIAL_ACCOUNT
     * @param  Request $request : User's request
     * @param  Int  $id      : SOCIAL ACCOUNT Id
     * @return Illuminate\Database\Eloquent\Collection
     */ 
    public function get($social_network_id){
        $social_account_profiles = SocialNetwork::findOrFail($social_network_id);
        $social_account_profiles = $social_account_profiles->userProfiles()->get();
        return $social_account_profiles;
    }
    


}
