<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Socialite;
use App\Models\User;
use App\Models\UserSocial;
use App\Models\Role;
use App\Models\UserConfirmation;
use Carbon\Carbon;

class SocialAuthController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('welcome');
    }

    public function login(){
        return view('login');
    }

    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return Response
     */
    public function redirect($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from a provider.
     * 
     * @param  Provider $provider ;the provider that is used to authenticate, e.g, Facebook, Twitter, Google, etc.
     * @return View           	  ;returns the `user's account
     */
    public function callback($provider)
    {
    	$social_info = Socialite::driver($provider)->user();

        //check if email is null
        if (!$social_info->getEmail()){
            \Flash::error('Invalid account, no email found.');
            return redirect('/');
        }
        //get user's social account, if it exists, by social id and provider. 
        $social = UserSocial::whereUserSocialId($social_info->getId())->whereSocial($provider)->first();
        
        //if the social account exists, return it. 
        if ($social){
        	$user = User::whereId($social->user_id)->first();
        }
        else {
        	$user = User::whereEmail($social_info->getEmail())->first();
        	//create a new user
        	if(!$user) {
                $name = $this->getName($provider, $social_info);

        		$user = new User;
        		$user->first_name   = $name['first_name'];
	            $user->last_name    = $name['last_name'];
                $user->email        = $social_info->getEmail();
	            $user->avatar       = $social_info->getAvatar();
	            $user->save();
        	}

        	//create social account for this user.
	        $social = new UserSocial;
	        $social->user_id 		= $user->id;
	        $social->user_social_id = $social_info->getId();
	        $social->social 		= $provider;
	        $social->save();

	        //add role as a 'user'
	        $role = Role::whereName('User')->first();
	        $user->roles()->sync([$role->id]);            
        	
            //add confirmation to the user
            if (!count($user->confirm)){
                 $confirmation_code = UserConfirmation::create([
                    'user_id'           => $user->id,
                    'confirmation_code' => str_random(8),
                    'confirmed'         => 1
                ]);        
            }

        }

  		$user->last_login = Carbon::now();
  		$user->save();
  		\Auth::loginUsingId($user->id);
  		return redirect('/users/'.$user->id);
    }
    
    /**
     * Get the full name of the user.
     * 
     * @param  String       $provider           ;the provider of the request, e.g., facebook, google, etc
     * @param  Socialite    $social_info        ;the user information retrieved from the provider
     * @return Array        $user               ;user's full name.      
     */
    public function getName($provider, $social_info){
        
        if ($provider == 'facebook')
        {
            $user['first_name'] = $social_info->user['first_name'];
            $user['last_name']  = $social_info->user['last_name'];  
        } 
        else {
            $user['first_name'] = $social_info->user['name']['givenName'];
            $user['last_name']  = $social_info->user['name']['familyName'];  
        }

        return $user;
    }
}
