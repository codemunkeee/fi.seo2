<?php

namespace App\Http\Controllers\Others;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class HelpController extends Controller
{
    public function index(){
    	return view('others.help.index');
    }
}
