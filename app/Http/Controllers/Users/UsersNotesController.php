<?php

namespace App\Http\Controllers\Users;

use Illuminate\Http\Request;

use DB;
use App\Http\Requests;
use App\Models\SEO\Note;
use App\Http\Controllers\Controller;

class UsersNotesController extends Controller
{
	public function __construct(){
		//$this->user = \Auth::user();
	}

	public function store(Request $request){
		
		return Note::create($request->all());

	}
    public function api($parent, $parent_id){

    	return DB::table('notes')
    		->where('user_id', '=', \Auth::user()->id)
    		->where('parent_type', '=', $parent)
    		->where('parent_id', '=', $parent_id)
    		->orderBy('updated_at', 'desc')   
    		->get();
    }

    public function update(Request $request, $id){
        $note = Note::findOrFail($id);
        $note->title = $request['title'];
        $note->notes = $request['notes'];
        $note->save();
        return $note;
    }

    public function destroy($id){
        $note = Note::findOrFail($id);
        $note->delete();
        return 'success';
    }
}
