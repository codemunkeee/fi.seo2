<?php

namespace App\Http\Controllers\Users;

use DB;
use Gate;
use App\Models\Role;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Models\Users\Permission;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\Users\UsersRolesRequest;


class UsersRolesController extends Controller
{

    public function __construct()
    {
        //only admins can see this page
        $this->middleware('auth');
    }

    /**
     * Shows all roles
     * 
     * @return Illuminate\Contracts\View\Factory
     */
    public function index(){
        if (Gate::denies('admin')) {
            return redirect('/')->withErrors('You are not allowed to view this page.');
        }
        $roles  = Role::orderBy('updated_at', 'desc')->get();
        
        return view('seo.roles.index', compact('roles'));

    }

     /**
     * Shows a form to add a new role
     * 
     * @return Illuminate\Contracts\View\Factory
     */
    public function create(){
        if (Gate::denies('admin')) {
            return redirect('/')->withErrors('You are not allowed to view this page.');
        }

        $method     = 'create';
        $disabled   = '';

        return view('seo.roles.form', compact('method', 'disabled'));
    }

  
    /**
     * Stores/adds a new role 
     * 
     * @param  UsersRolesRequest : the data to be stored on the db
     * @return Illuminate\Contracts\View\Factory
     */
    public function store(UsersRolesRequest $request){
        if (Gate::denies('admin')) {
            return redirect('/')->withErrors('Permission denied.');
        }

        DB::transaction(function () use($request) {
            $role = Role::create([
                'name'          => $request['name'],
                'slug'          => $request['slug'],
                'description'   => $request['description']
            ]);

        });

        \Flash::success('Added successfully!');
        return redirect('roles');
    }


    /**
     * Shows the details of the role
     * 
     * @param  int  : the role id
     * @return Illuminate\Contracts\View\Factory
     */
    public function show($id){
        if (Gate::denies('admin')) {
            return redirect('/')->withErrors('You are not allowed to view this page.');
        }
        $method     = 'show';
        $disabled   = 'disabled';

        $role = Role::findOrFail($id);
     
        //for pivot select
        $permissions = Permission::lists('name', 'id')->all();

        //permissions for this role
        $permission_role = $role->permissions()->get()->lists('id')->all();
        //dd($permission_role);
        return view('seo.roles.form', compact('role', 'method', 'disabled', 'permissions', 'permission_role'));
    }

     /**
     * Shows the editable details of the role
     * 
     * @param  int : the role id
     * @return Illuminate\Contracts\View\Factory
     */
    public function edit($id){
        if (Gate::denies('admin')) {
            return redirect('/')->withErrors('You are not allowed to view this page.');
        }
        $method     = 'edit';
        $disabled   = '';

        $role = Role::findOrFail($id);
        
        //for pivot select
        $permissions = Permission::lists('name', 'id')->all();

        //permissions for this role
        $permission_role = $role->permissions()->get()->lists('id')->all();

        return view('seo.roles.form', compact('role', 'method', 'disabled', 'permissions', 'permission_role'));
    }

     /**
     * Updates an existing roles record
     * 
     * @param  UsersRolesRequest : contains the data that was sent by the user
     * @param  int : the role id
     * @return Illuminate\Contracts\View\Factory
     */
    public function update(UsersRolesRequest $request, $id){
        if (Gate::denies('admin')) {
            return redirect('/')->withErrors('Permission denied.');
        }

        $role = Role::findOrFail($id);
        $role->update($request->all());

        \Flash::success('Updated successfully!');
        return redirect('/roles/'.$role->id.'/edit');
    }

    /**
     * Deletes a role
     * 
     * @param  int  : the role's id
     * @return Illuminate\Contracts\View\Factory
     */
    public function destroy($id){
        if (Gate::denies('admin')) {
            return redirect('/')->withErrors('Permission denied.');
        }
        
        DB::transaction(function () use($id) { 
            //delete pivots
            DB::table('permission_role')
                ->where('role_id', '=', $id)
                ->delete();
            DB::table('role_user')
                ->where('role_id', '=', $id)
                ->delete();
            Role::destroy($id);
        });

        return redirect('/roles');        
    }

   /**
     * Filter a set of data
     * 
     * @return \Illuminate\Pagination\LengthAwarePaginator : returns a paginator instance
     */
    public function api_filter(){
        if (Gate::denies('api')) {
            return redirect('/')->withErrors('Permission denied.');
        } 

        $params = Input::all();
        $data   = Role::with('permissions');
        $input  = [
            'path'  => Input::url(),
            'query' => Input::query(),
            'page'  => Input::has('page') ? Input::get('page') : 1
        ];

        foreach ($params as $key => $value){ $key != 'page' ? $data->where($key, '=', $value) : ''; }

        return $this->api_paginate($data, $input);

    }

    /**
     * Sort a set of data.
     * 
     * @param  string : the field to be sorted
     * @param  string : the sorting type
     * @return \Illuminate\Pagination\LengthAwarePaginator : returns a paginator instance
     */
    public function api_sort($field, $type){
        if (Gate::denies('api')) {
            return redirect('/')->withErrors('Permission denied.');
        } 

        $data = Role::with('permissions')->orderBy($field, $type)->paginate(env('ITEMS_PER_PAGE'));

        return $data;
    }

    /**
     * Search for a keyword then return a filtered set of data.
     * 
     * @return \Illuminate\Pagination\LengthAwarePaginator : returns a paginator instance
     */
    public function api_search(){
        if (Gate::denies('api')) {
            return redirect('/')->withErrors('Permission denied.');
        } 
        
        $params = Input::all();
        $data   = Role::with('permissions');
        $input  = [
            'path'  => Input::url(),
            'query' => Input::query(),
            'page'  => Input::has('page') ? Input::get('page') : 1
        ];

        foreach ($params as $key => $value){ $key != 'page' ? $data->orWhere($key, 'LIKE', '%'.$value.'%') : ''; }

        return $this->api_paginate($data, $input);

    }

    /**
     * Paginate a data source.
     * 
     * @param  array : data to be paginated
     * @param  array : the input
     * @return \Illuminate\Pagination\LengthAwarePaginator : returns a paginator instance
     */
    public function api_paginate($data, $input){
        $items_all          = $data->get()->all();
        $items_per_page     = env('API_ITEMS_PER_PAGE');
        $items_retrieved    = $input['page'] * $items_per_page;
        $offset             = $items_retrieved - $items_per_page;         
       
        $current_items      = array_slice($items_all, $offset , $items_per_page);

        $items = new \Illuminate\Pagination\LengthAwarePaginator($current_items, count($items_all), $items_per_page, $input['page'], [
            'path'  => $input['path'],
            'query' => $input['query'],
        ]);

        return $items;
    }

}
