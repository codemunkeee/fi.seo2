<?php

namespace App\Http\Controllers\Users;

use DB;
use Gate;
use App\Http\Requests;
use App\Models\SEO\Whois;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Validation\Users\UsersWhoisValidator;
use App\Http\Requests\Users\UsersWhoisRequest;


class UsersWhoisController extends Controller
{   
    /**
     * Initialize the controller
     */
    public function __construct(){
        $this->middleware('auth');
        $this->user = \Auth::user();
    }

    /**
     * Shows all whois data
     * 
     * @return view
     */
    public function index(){
        if (Gate::denies('whois-all')) {
            return redirect('/')->withErrors('You are not allowed to view this page.');
        } 
  
        $whoiss  = $this->user->whois()->orderBy('updated_at', 'desc')->get();

        return view('seo.whois.index', compact('whoiss'));
    }

    /**
     * Shows a form to add a new whois
     * @return view
     */
    public function create(){
        if (Gate::denies('whois-create')) {
            return redirect('/')->withErrors('You are not allowed to view this page.');
        } 

        $method     = 'create';
        $disabled   = '';

        return view('seo.whois.form', compact('method', 'disabled'));
    }

    /**
     * Stores/adds a new whois data to be assigned on the authenticated user
     * 
     * @param  UsersWhoisRequest $request : the whois data to be stored on the db
     * @return view
     */
    public function store(UsersWhoisRequest $request){
        if (Gate::denies('whois-store')) {
            return redirect('/')->withErrors('Permission denied.');
        } 

        //add new whois to the database and add a pivot table
        DB::transaction(function () use($request) {
            $whois = Whois::create([
                'username' => $request['username'],
                'password' => $request['password']
            ]);

            $whois->users()->attach($whois->id, ['user_id' => $this->user->id]);
        });

        return redirect('whois');
    }

    /**
     * Shows the details of the whois
     * 
     * @param  int $id : the whois id
     * @return view
     */
    public function show($id){
        if (Gate::denies('whois-show')) {
            return redirect('/')->withErrors('You are not allowed to view this page.');
        } 

        $method     = 'show';
        $disabled   = 'disabled';

        $whois = Whois::findOrFail($id);
     
        return view('seo.whois.form', compact('whois', 'method', 'disabled'));
    }

    /**
     * Shows the editable details of the whois
     * 
     * @param  int $id : the whois id
     * @return view
     */
    public function edit($id){
        $method     = 'edit';
        $disabled   = '';

        $whois = Whois::findOrFail($id);
        
        return view('seo.whois.form', compact('whois', 'method', 'disabled'));
    }

    /**
     * Updates an existing whois record
     * 
     * @param  UsersWhoisRequest $request : contains the data that was sent by the user
     * @param  int $id : the whois id
     * @return view
     */
    public function update(UsersWhoisRequest $request, $id){
        $whois = Whois::findOrFail($id);
        $whois->update($request->all());
        \Flash::success('Updated successfully!');
        return redirect('/whois/'.$whois->id.'/edit');
    }

    /**
     * Filter a set of data
     * 
     * @return \Illuminate\Pagination\LengthAwarePaginator : returns a paginator instance
     */
    public function api_filter(){
        if (Gate::denies('api')) {
            return redirect('/')->withErrors('Permission denied.');
        } 

        $params = Input::all();
        $data   = Whois::with('users');
        $input  = [
            'path'  => Input::url(),
            'query' => Input::query(),
            'page'  => Input::has('page') ? Input::get('page') : 1
        ];

        foreach ($params as $key => $value){ $key != 'page' ? $data->where($key, '=', $value) : ''; }

        return $this->api_paginate($data, $input);

    }

    /**
     * Sort a set of data.
     * 
     * @param  string : the field to be sorted
     * @param  string : the sorting type
     * @return \Illuminate\Pagination\LengthAwarePaginator : returns a paginator instance
     */
    public function api_sort($field, $type){
        if (Gate::denies('api')) {
            return redirect('/')->withErrors('Permission denied.');
        } 

        $data = Whois::with('users')->orderBy($field, $type)->paginate(env('ITEMS_PER_PAGE'));

        return $data;
    }

    /**
     * Search for a keyword then return a filtered set of data.
     * 
     * @return \Illuminate\Pagination\LengthAwarePaginator : returns a paginator instance
     */
    public function api_search(){
        if (Gate::denies('api')) {
            return redirect('/')->withErrors('Permission denied.');
        } 
        
        $params = Input::all();
        $data   = Whois::with('users');
        $input  = [
            'path'  => Input::url(),
            'query' => Input::query(),
            'page'  => Input::has('page') ? Input::get('page') : 1
        ];

        foreach ($params as $key => $value){ $key != 'page' ? $data->orWhere($key, 'LIKE', '%'.$value.'%') : ''; }

        return $this->api_paginate($data, $input);

    }

    /**
     * Paginate a data source.
     * 
     * @param  array : data to be paginated
     * @param  array : the input
     * @return \Illuminate\Pagination\LengthAwarePaginator : returns a paginator instance
     */
    public function api_paginate($data, $input){
        $items_all          = $data->get()->all();
        $items_per_page     = env('API_ITEMS_PER_PAGE');
        $items_retrieved    = $input['page'] * $items_per_page;
        $offset             = $items_retrieved - $items_per_page;         
       
        $current_items      = array_slice($items_all, $offset , $items_per_page);

        $items = new \Illuminate\Pagination\LengthAwarePaginator($current_items, count($items_all), $items_per_page, $input['page'], [
            'path'  => $input['path'],
            'query' => $input['query'],
        ]);

        return $items;
    }

}