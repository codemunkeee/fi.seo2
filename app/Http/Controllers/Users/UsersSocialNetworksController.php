<?php

namespace App\Http\Controllers\Users;

use DB;
use Gate;
use Validator;
use App\Http\Requests;
use App\Models\SEO\Profile;
use Illuminate\Http\Request;
use App\Models\SEO\SocialNetwork;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\SEO\SocialNetworksRequest;


class UsersSocialNetworksController extends Controller
{

	public function __construct(){
		$this->middleware('auth');
        $this->user = \Auth::user();
	}

    public function index(){
        if (Gate::denies('social-network-all')) {
            return redirect('/')->withErrors('You are not allowed to view this page.');
        } 

        $social_networks_all = SocialNetwork::all();

        $social_networks_current = $this->user->socialNetworks()->get()->lists('id')->all();

    	$social_networks = $this->user->socialNetworks()->orderBy('updated_at', 'desc')->get();

        $social_networks_group = $this->user->socialAccountsCount()->get();

        $social_accounts = $this->user->socialAccounts()->get();

        $pivot_api = [
                        'parent_name' => 'social_networks',
                        'social_networks' => [ 'get' => '/social_networks/get'],
                        'social_accounts' => ['get' => '/social_networks/{id}/social_accounts/get'],
                        'profiles' => [ 'get' => '/social_networks/{id}/profiles/get'],                                    
                        'notes' => [ 'get' => '/notes/social_networks/{id}', 
                                    'post' => '/notes/post',
                                    'store' => '/notes',
                                    ]
                    ];

    	return view('seo.social_networks.index', compact('social_networks', 'pivot_api', 'social_networks_group', 'social_networks_all', 'social_networks_current', 'social_accounts'));
    }

    public function show($id){
        if (Gate::denies('social-network-show')) {
            return redirect('/')->withErrors('You are not allowed to view this page.');
        } 

    	$method   = 'show';
    	$disabled = 'disabled';
    	$social_network 	  = SocialNetwork::findOrFail($id);

    	return view('seo.social_networks.form', compact('method', 'disabled', 'social_network'));
    }

    public function create(){
        if (Gate::denies('social-network-create')) {
            return redirect('/')->withErrors('You are not allowed to view this page.');
        } 

    	$method   = 'create';
    	$disabled = '';

        $ids = $this->user->socialNetworks()->get()->lists('id')->all();

        $social_networks = SocialNetwork::get()->lists('name', 'id')->all();

        $user_social_networks = $this->user->socialNetworks()->get()->lists('id')->all();

        return view('seo.social_networks.form', compact('method', 'disabled', 'social_networks', 'user_social_networks'));
    }

    public function store(Request $request){
        if (Gate::denies('social-network-store')) {
            return redirect('/')->withErrors('Permission denied.');
        } 

        $social_networks = $request['social_networks'];

        foreach ($social_networks as $social_network) {
            if (is_numeric($social_network))
            {
                $social_network_array[] =  intval($social_network);
            }   
            else
            {
                $validator = Validator::make(['name'=>$social_network], [
                    'name' => 'required|unique:social_networks'
                ]);

                if ($validator->fails()) {
                    return redirect('social_networks')
                                ->withErrors($validator)
                                ->withInput();
                }

                $social_network_new = SocialNetwork::create(['name'=>$social_network, 'logo' => 'fa-question']);

                $social_network_array[] = $social_network_new->id;
            }   
        }

        // get all profiles from this user
        $user_profiles = $this->user->profiles()->get()->all();

        // returns an array of "attached", "detached", "updated" with sub-arrays 
        $sync_result = $this->user->socialNetworks()->sync($social_network_array);

        // get all detached social networks from this user
        foreach($sync_result['detached'] as $detached){
            //get each profile from this user then detach all relations
            foreach($user_profiles as $profile){
                $profile->socialNetworks()->detach($detached);
            }
        }

    	\Flash::success('New Social Network added!');
    	return redirect('/social_networks/');
    }

    public function edit($id){
    	$method   = 'edit';
    	$disabled = '';
    	$social_network 	  = SocialNetwork::findOrFail($id);

        if (Gate::denies('social-network-edit')) {
            return redirect('/')->withErrors('You are not allowed to view this page.');
        } 


    	return view ('seo.social_networks.form', compact('method', 'disabled', 'social_network'));
    }

    public function update(SocialNetworksRequest $request, $id){
        if (Gate::denies('social-network-update')) {
            return redirect('/')->withErrors('Permission denied.');
        } 

		$social_network 	  = SocialNetwork::findOrFail($id);
        $social_network->update($request->all());

    	\Flash::success('Social Network was updated!');
    	return redirect('/social_networks/'.$social_network->id);
    }

    public function destroy(SocialNetworksRequest $request, $id){
        if (Gate::denies('social-network-destroy')) {
            return redirect('/')->withErrors('Permission denied.');
        } 

        DB::transaction(function () use($request, $id) {
            $social_network = SocialNetwork::findOrFail($id);
            // delete related social networks for each profile
            $social_network->profiles()->detach();
            // delete related social networks for this user
            $this->user->socialNetworks()->detach($social_network);      
            // delete the social network
            SocialNetwork::destroy($id);
        });

    	\Flash::success('Social Network was deleted!');
    	return redirect('/social_networks/');
    }

    /**
     * Returns all social networks associated with the currently logged in user.
     * @return [type] [description]
     */
    public function get(){
        //return SocialNetwork::all()->with('profiles')->get();
        return $this->user->socialNetworks()->with('profiles')->get();
    }

    /**
     * Filter a set of data
     * 
     * @return \Illuminate\Pagination\LengthAwarePaginator : returns a paginator instance
     */
    public function api_filter(){
        if (Gate::denies('api')) {
            return redirect('/')->withErrors('Permission denied.');
        } 

        $params = Input::all();
        $data   = new SocialNetwork;//$this->user->socialNetworks()->with('profiles');
        
        $input  = [
            'path'  => Input::url(),
            'query' => Input::query(),
            'page'  => Input::has('page') ? Input::get('page') : 1
        ];

        foreach ($params as $key => $value){ $key != 'page' ? $data->where($key, '=', $value) : ''; }

        return $this->api_paginate($data, $input);

    }

    /**
     * Sort a set of data.
     * 
     * @param  string : the field to be sorted
     * @param  string : the sorting type
     * @return \Illuminate\Pagination\LengthAwarePaginator : returns a paginator instance
     */
    public function api_sort($field, $type){
        if (Gate::denies('api')) {
            return redirect('/')->withErrors('Permission denied.');
        } 

        $data = SocialNetwork::with('profiles')->orderBy($field, $type)->paginate(env('ITEMS_PER_PAGE'));

        return $data;
    }

    /**
     * Search for a keyword then return a filtered set of data.
     * 
     * @return \Illuminate\Pagination\LengthAwarePaginator : returns a paginator instance
     */
    public function api_search(){
        if (Gate::denies('api')) {
            return redirect('/')->withErrors('Permission denied.');
        } 
        
        $params = Input::all();
        $data   = new SocialNetwork;

        $input  = [
            'path'  => Input::url(),
            'query' => Input::query(),
            'page'  => Input::has('page') ? Input::get('page') : 1
        ];

        foreach ($params as $key => $value){ $key != 'page' ? $data->orWhere($key, 'LIKE', '%'.$value.'%') : ''; }

        return $this->api_paginate($data, $input);

    }

    /**
     * Paginate a data source.
     * 
     * @param  array : data to be paginated
     * @param  array : the input
     * @return \Illuminate\Pagination\LengthAwarePaginator : returns a paginator instance
     */
    public function api_paginate($data, $input){
        $items_all          = $data->get()->all();
        $items_per_page     = 100;
        $items_retrieved    = $input['page'] * $items_per_page;
        $offset             = $items_retrieved - $items_per_page;         
       

        $current_items      = array_slice($items_all, $offset , $items_per_page);

        $items = new \Illuminate\Pagination\LengthAwarePaginator($current_items, count($items_all), $items_per_page, $input['page'], [
            'path'  => $input['path'],
            'query' => $input['query'],
        ]);

        return $items;
    }

    // public function profiles_social_networks($profile_id){
    //     $profile = Profile::findOrFail($profile_id);

    //     return $profile->socialNetworksDecrypted();
    //     // $test =  DB::table('profile_social_networks')
    //     //     ->select("social_networks.id", "social_networks.name", 'profile_social_networks.username')
    //     //     ->join('profiles', 'profile_social_networks.profile_id', '=', 'profiles.id')
    //     //     ->join('social_networks', 'profile_social_networks.social_network_id', '=', 'social_networks.id')
    //     //     ->where('profiles.id', '=', $profile_id)
    //     //     ->get();    
    // }

}
