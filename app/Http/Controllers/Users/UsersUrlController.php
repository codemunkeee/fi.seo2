<?php

namespace App\Http\Controllers\Users;

use DB;
use Gate;
use App\Http\Requests;
use App\Models\SEO\Url;
use App\Models\SEO\Host;
use App\Packages\SEO\SEO;
use App\Models\SEO\Profile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\Users\UsersUrlRequest;

class UsersUrlController extends Controller
{   
    /**
     * Initialize the controller 
     * 
     */
    public function __construct(){
        $this->middleware('auth'); 
        $this->user = \Auth::user();       
    }

    /**
     * Returns all urls 
     * 
     * @return view
     */
    public function index(){
        if (Gate::denies('url-all')) {
            return redirect('/')->withErrors('You are not allowed to view this page.');
        } 

        $title  = 'urls';
        $urls   = $this->user->urls()->orderBy('updated_at', 'desc')->get();

        return view('seo.urls.index', compact('urls', 'title'));
    }

    /**
     * Returns a view to create a new url
     * 
     * @return view
     */
    public function create(){
        if (Gate::denies('url-create')) {
            return redirect('/')->withErrors('You are not allowed to view this page.');
        } 

        $method     = 'create';
        $disabled   = '';

        // get the list of host names for this user
        $host_names = DB::table('user_hosts')
            ->select('hosts.*')
            ->join('users', 'user_hosts.user_id', '=', 'users.id')
            ->join('hosts', 'user_hosts.host_id', '=', 'hosts.id')
            ->where('users.id', '=', $this->user->id)
            ->orderBy('updated_at')
            ->lists('hosts.friendly_name', 'hosts.id');

        // get the first instance of host name
        $host_name = $this->user->hosts()->first();

        return view('seo.urls.form', compact('method', 'disabled', 'host_names', 'host_name'));
    }

    /**
     * Stores the new url on the database.
     * 
     * @param  UsersUrlRequest $request : contains the data that was sent by the user
     * @return view
     */
    public function store(UsersUrlRequest $request){
        if (Gate::denies('url-store')) {
            return redirect('/')->withErrors('Permission denied.');
        } 

        // redirect the user to the create page when there is no whois data detected
        if (!isset($this->user->whois)) {
            return redirect('urls/create')->withErrors(['Whois data is not set up with this user.']);
        }

        // prepare the params to be sent to the SEO class
        $params = [
            'domain_name' => $request['url'],
            'username'    => $this->user->whois->username,
            'password'    => $this->user->whois->password
        ];
        
        //for testing :
        //$SEO = new SEO($params, true);
        // initialize the SEO, then get all data from whoisxmlapi
        $SEO = new SEO($params);
        
        $whois = $SEO->getWhois();
        if (is_null($whois)){
            \Flash::error($SEO->getErrorMessage());
            return redirect('/urls/create');
        } 
        //add new url to the database and add a pivot table
        DB::transaction(function () use($request, $SEO) {
  
            $url = URL::create([
                'url'                       => $request['url'], 
                'ip'                        => $SEO->getIP($request['url']),
                'host_id'                   => (isset($request['host_id'])) ? intval($request['host_id']) : null,
                'registrar'                 => $SEO->getRegistrar(), 
                'update_date'               => $SEO->getUpdateDate(),
                'create_date'               => $SEO->getCreateDate(),
                'registrant_name'           => $SEO->getRegistrantName(),
                'registrant_organization'   => $SEO->getRegistrantOrganization(),
                'registrant_street'         => $SEO->getRegistrantStreet(),
                'registrant_city'           => $SEO->getRegistrantCity(),
                'registrant_state'          => $SEO->getRegistrantState(),
                'registrant_zip'            => $SEO->getRegistrantZip(),
                'registrant_phone'          => $SEO->getRegistrantPhone(),
                'registrant_email'          => $SEO->getRegistrantEmail(),
                'registrant_name_server_1'  => (null !== ($SEO->getNameServers())) ? $SEO->getNameServers()[0] : null,
                'registrant_name_server_2'  => (null !== ($SEO->getNameServers())) ? $SEO->getNameServers()[1] : null,
                'cpanel_login_url'          => (isset($request['cpanel_login_url'])) ? $request['cpanel_login_url'] : null,
                'cpanel_username'           => (isset($request['cpanel_username'])) ? $request['cpanel_username'] : null,
                'cpanel_password'           => (isset($request['cpanel_password'])) ? $request['cpanel_password'] : null,
                'ftp_login_address'         => (isset($request['ftp_login_address'])) ? $request['ftp_login_address'] : null,
                'ftp_username'              => (isset($request['ftp_username'])) ? $request['ftp_username'] : null,
                'ftp_password'              => (isset($request['ftp_password'])) ? $request['ftp_password'] : null,
                'wp_admin_username'         => (isset($request['wp_admin_username'])) ? $request['wp_admin_username'] : null,
                'wp_admin_password'         => (isset($request['wp_admin_password'])) ? $request['wp_admin_password'] : null,
                'notes'                     => (isset($request['notes'])) ? $request['notes'] : null
            ]);

            $url->users()->attach($url->id, ['user_id' => $this->user->id]);

        });

        return redirect('urls');
    }

    /**
     * Returns a view that shows the details of the url
     * 
     * @param  int $id : the url's id
     * @return view
     */
    public function show($id){
        if (Gate::denies('url-show')) {
            return redirect('/')->withErrors('You are not allowed to view this page.');
        } 

        $method     = 'show';
        $disabled   = 'disabled';

        //get the url
        $url = URL::findOrFail($id);
        
        // get the list of host names for this user
        $host_names = DB::table('user_hosts')
            ->select('hosts.*')
            ->join('users', 'user_hosts.user_id', '=', 'users.id')
            ->join('hosts', 'user_hosts.host_id', '=', 'hosts.id')
            ->where('users.id', '=', $this->user->id)
            ->orderBy('updated_at')
            ->lists('hosts.friendly_name', 'hosts.id');

        // get the selected host name for this url
        $host_name = $url->host;

        return view('seo.urls.form', compact('url', 'method', 'disabled', 'host_names', 'host_name'));

    }

    /**
     * A view to edit the url's details
     * 
     * @param  int $id : the url's id
     * @return view
     */
    public function edit($id){
        $method     = 'edit';
        $disabled   = '';
        $url        = URL::findOrFail($id);
        
        if (!(Gate::allows('url-edit') && $this->user->owns($url))) {
            return redirect('/')->withErrors('You are not allowed to view this page.');
        } 
        
        
        // get the list of host names for this user
        $host_names = DB::table('user_hosts')
            ->select('hosts.*')
            ->join('users', 'user_hosts.user_id', '=', 'users.id')
            ->join('hosts', 'user_hosts.host_id', '=', 'hosts.id')
            ->where('users.id', '=', $this->user->id)
            ->orderBy('updated_at')
            ->lists('hosts.friendly_name', 'hosts.id');

        // get the selected host name for this url
        $host_name = $url->host;
        
        return view('seo.urls.form', compact('url', 'method', 'disabled', 'host_names', 'host_name'));
    }

    /**
     * Updates the url details
     * 
     * @param  UsersUrlRequest $request : the data sent by the user
     * @param  int $id : the url's id
     * @return view
     */
    public function update(UsersUrlRequest $request, $id){
        $URL = URL::findOrFail($id);

        if (!(Gate::allows('url-update') && $this->user->owns($URL))) {
            return redirect('/')->withErrors('Permission denied.');
        } 
        
        $URL->update($request->all());

        \Flash::success('Updated successfully!');
        return redirect('/urls/'.$URL->id.'/edit');
        
    }   
    /**
     * Deletes a url
     * 
     * @param  int $id : the url's id
     * @return view
     */
    public function destroy($id){
        $url = URL::findOrFail($id);

        if (!(Gate::allows('url-destroy') && $this->user->owns($url))) {
            return redirect('/')->withErrors('Permission denied.');
        } 

        DB::transaction(function () use($id) { 
            //delete pivot
            DB::table('user_urls')
                ->where('user_id', '=', $this->user->id)
                ->where('url_id', '=', $id)
                ->delete();

            //delete urls in persona
            DB::table('persona_urls')
                ->where('url_id', '=', $id)
                ->delete();

            URL::destroy($id);
        });
        return $url;
        //return redirect('/urls');        
    }

    public function unlink(Request $request, $parent_table_name, $pivot_id){
        $url = Url::findOrFail($pivot_id);
        $url->unlinkHost($pivot_id);
        return $url;
    }

    /**
     * Filter a set of data
     * 
     * @return \Illuminate\Pagination\LengthAwarePaginator : returns a paginator instance
     */
    public function api_filter(){
        if (Gate::denies('api')) {
            return redirect('/')->withErrors('Permission denied.');
        } 

        $params = Input::all();
        $data   = $this->user->qb_urls();  
        
        dd($data);

        $input  = [
            'path'  => Input::url(),
            'query' => Input::query(),
            'page'  => Input::has('page') ? Input::get('page') : 1
        ];

        foreach ($params as $key => $value){ $key != 'page' ? $data->where($key, '=', $value) : ''; }

        return $this->api_paginate($data, $input);

    }
 
    /**
     * Sort a set of data.
     * 
     * @param  string : the field to be sorted
     * @param  string : the sorting type
     * @return \Illuminate\Pagination\LengthAwarePaginator : returns a paginator instance
     */
    public function api_sort($field, $type){
        if (Gate::denies('api')) {
            return redirect('/')->withErrors('Permission denied.');
        } 

        $data = $this->user->urls()->orderBy($field,$type)->paginate(env('ITEMS_PER_PAGE'));
        //Url::with('users')->orderBy($field, $type)->paginate(env('ITEMS_PER_PAGE'));

        return $data;
    }

    /**
     * Search for a keyword then return a filtered set of data.
     * 
     * @return \Illuminate\Pagination\LengthAwarePaginator : returns a paginator instance
     */
    public function api_search(){
        if (Gate::denies('api')) {
            return redirect('/')->withErrors('Permission denied.');
        } 
        
        $params = Input::all();
        $data   = $this->user->urls();//Url::with('users');
        //return \App\Models\SEO\Host::all();
        return $data->get();
        $input  = [
            'path'  => Input::url(),
            'query' => Input::query(),
            'page'  => Input::has('page') ? Input::get('page') : 1
        ];

        foreach ($params as $key => $value){ $key != 'page' ? $data->orWhere($key, 'LIKE', '%'.$value.'%') : ''; }

        return $this->api_paginate($data, $input);

    }

    /**
     * Paginate a data source.
     * 
     * @param  array : data to be paginated
     * @param  array : the input
     * @return \Illuminate\Pagination\LengthAwarePaginator : returns a paginator instance
     */
    public function api_paginate($data, $input){
        $items_all          = $data->get()->all();
        $items_per_page     = env('API_ITEMS_PER_PAGE');
        $items_retrieved    = $input['page'] * $items_per_page;
        $offset             = $items_retrieved - $items_per_page;         

        $current_items      = array_slice($items_all, $offset , $items_per_page);

        $items = new \Illuminate\Pagination\LengthAwarePaginator($current_items, count($items_all), $items_per_page, $input['page'], [
            'path'  => $input['path'],
            'query' => $input['query'],
        ]);
        //dd($items);
        return $items;
    }


    public function host_urls($host_id){
        return DB::table('user_urls')
            ->select("urls.*")
            ->join('users', 'user_urls.user_id', '=', 'users.id')
            ->join('urls', 'user_urls.url_id', '=', 'urls.id')
            ->join('hosts', 'urls.host_id', '=', 'hosts.id')
            ->where('users.id', '=', $this->user->id)
            ->where('hosts.id', '=', $host_id)
            ->where('urls.ip', '!=', 'No IP Found')
            ->get();
    }

    // public function profile_urls($profile_id){
    //     return DB::table('profile_urls')
    //         ->select("urls.*", "urls.id as url_id", "profile_urls.id as id")
    //         ->join('profiles', 'profile_urls.profile_id', '=', 'profiles.id')
    //         ->join('urls', 'profile_urls.url_id', '=', 'urls.id')
    //         ->where('profiles.id', '=', $profile_id)
    //         ->where('urls.ip', '!=', 'No IP Found')
    //         ->get();
    // }

    // public function unselected_list($profile_id){
    //     $profile = Profile::findOrFail($profile_id);
    //     return $profile->unselectedUrls()->get();
    // }

  

}
