<?php

namespace App\Http\Controllers\Users;

use DB;
use Gate;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Models\Users\Permission;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\Users\UsersPermissionsRequest;

class UsersPermissionsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Shows all permissions
     * @return view : returns a view
     */
    public function index(){
        if (Gate::denies('admin')) {
            return redirect('/')->withErrors('You are not allowed to view this page.');
        }

        $permissions  = Permission::orderBy('updated_at', 'desc')->get();
        
        return view('seo.permissions.index', compact('permissions'));

    }

     /**
     * Shows a form to add a new permission
     * @return view
     */
    public function create(){
        if (Gate::denies('admin')) {
            return redirect('/')->withErrors('You are not allowed to view this page.');
        }
        $method     = 'create';
        $disabled   = '';

        return view('seo.permissions.form', compact('method', 'disabled'));
    }

    /**
     * Stores/adds a new permission 
     * 
     * @param  UsersPermissionsRequest : the data to be stored on the db
     * @return view
     */
    public function store(UsersPermissionsRequest $request){
        if (Gate::denies('admin')) {
            return redirect('/')->withErrors('Permission denied.');
        }        
        \DB::transaction(function () use($request) {
            $permission = Permission::create([
                'name'          => $request['name'],
                'slug'          => $request['slug'],
                'description'   => $request['description']
            ]);

        });

        \Flash::success('Added successfully!');
        return redirect('permissions');
    }

     /**
     * Shows the details of the permission
     * 
     * @param  int  : the permission id
     * @return view
     */
    public function show($id){
        if (Gate::denies('admin')) {
            return redirect('/')->withErrors('You are not allowed to view this page.');
        }

        $method     = 'show';
        $disabled   = 'disabled';

        $permission = Permission::findOrFail($id);
     
        return view('seo.permissions.form', compact('permission', 'method', 'disabled'));
    }

    /**
     * Shows the editable details of the permission
     * 
     * @param  int : the permission id
     * @return view
     */
    public function edit($id){
        if (Gate::denies('admin')) {
            return redirect('/')->withErrors('You are not allowed to view this page.');
        }

        $method     = 'edit';
        $disabled   = '';

        $permission = Permission::findOrFail($id);
        
        return view('seo.permissions.form', compact('permission', 'method', 'disabled'));
    }

     /**
     * Updates an existing permission record
     * 
     * @param  UsersPermissionsRequest : contains the data that was sent by the user
     * @param  int : the permission id
     * @return view
     */
    public function update(UsersPermissionsRequest $request, $id){
        if (Gate::denies('admin')) {
            return redirect('/')->withErrors('Permission denied.');
        }

        $permission = Permission::findOrFail($id);
        $permission->update($request->all());

        \Flash::success('Updated successfully!');
        return redirect('/permissions/'.$permission->id.'/edit');
    }

      /**
     * Deletes a permission
     * 
     * @param  int  : the permission's id
     * @return view
     */
    public function destroy($id){
        if (Gate::denies('admin')) {
            return redirect('/')->withErrors('Permission denied.');
        }

        \DB::transaction(function () use($id) { 
            //delete pivots
            \DB::table('permission_role')
                ->where('permission_id', '=', $id)
                ->delete();

            Permission::destroy($id);
        });

        return redirect('/permissions');        
    }

    /**
     * Filter a set of data
     * 
     * @return \Illuminate\Pagination\LengthAwarePaginator : returns a paginator instance
     */
    public function api_filter(){
        if (Gate::denies('api')) {
            return redirect('/')->withErrors('Permission denied.');
        } 

        $params = Input::all();
        $data   = Permission::with('roles');
        $input  = [
            'path'  => Input::url(),
            'query' => Input::query(),
            'page'  => Input::has('page') ? Input::get('page') : 1
        ];

        foreach ($params as $key => $value){ $key != 'page' ? $data->where($key, '=', $value) : ''; }

        return $this->api_paginate($data, $input);

    }

    /**
     * Sort a set of data.
     * 
     * @param  string : the field to be sorted
     * @param  string : the sorting type
     * @return \Illuminate\Pagination\LengthAwarePaginator : returns a paginator instance
     */
    public function api_sort($field, $type){
        if (Gate::denies('api')) {
            return redirect('/')->withErrors('Permission denied.');
        } 

        $data = Permission::with('roles')->orderBy($field, $type)->paginate(env('ITEMS_PER_PAGE'));

        return $data;
    }

    /**
     * Search for a keyword then return a filtered set of data.
     * 
     * @return \Illuminate\Pagination\LengthAwarePaginator : returns a paginator instance
     */
    public function api_search(){
        if (Gate::denies('api')) {
            return redirect('/')->withErrors('Permission denied.');
        } 
        
        $params = Input::all();
        $data   = Permission::with('roles');
        $input  = [
            'path'  => Input::url(),
            'query' => Input::query(),
            'page'  => Input::has('page') ? Input::get('page') : 1
        ];

        foreach ($params as $key => $value){ $key != 'page' ? $data->orWhere($key, 'LIKE', '%'.$value.'%') : ''; }

        return $this->api_paginate($data, $input);

    }

    /**
     * Paginate a data source.
     * 
     * @param  array : data to be paginated
     * @param  array : the input
     * @return \Illuminate\Pagination\LengthAwarePaginator : returns a paginator instance
     */
    public function api_paginate($data, $input){
        $items_all          = $data->get()->all();
        $items_per_page     = env('API_ITEMS_PER_PAGE');
        $items_retrieved    = $input['page'] * $items_per_page;
        $offset             = $items_retrieved - $items_per_page;         
       
        $current_items      = array_slice($items_all, $offset , $items_per_page);

        $items = new \Illuminate\Pagination\LengthAwarePaginator($current_items, count($items_all), $items_per_page, $input['page'], [
            'path'  => $input['path'],
            'query' => $input['query'],
        ]);

        return $items;
    }
}
