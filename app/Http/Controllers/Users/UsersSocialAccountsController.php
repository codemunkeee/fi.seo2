<?php

namespace App\Http\Controllers\Users;

use DB;
use Gate;
use Validator;
use App\Http\Requests;
use App\Models\SEO\Profile;
use Illuminate\Http\Request;
use App\Models\SEO\SocialAccount;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;


class UsersSocialAccountsController extends Controller
{

	public function __construct(){
		$this->middleware('auth');
        $this->user = \Auth::user();
	}

    public function index(){
		$social_accounts = $this->user->socialAccounts()->get();
    	
    	$social_accounts_group = $this->user->socialAccountsCount()->get();

        $profiles = $this->user->profiles()->get();

        $social_networks = $this->user->socialNetworks()->get();

    	//dd(SocialAccount::with('profiles')->get());
        $pivot_api = [
                        'parent_name' => 'social_accounts',
                        'social_accounts' => ['get' => '/social_accounts/get',
                                                'store' => '/social_accounts',
                                                'update' => '/social_accounts/{id}',
                                                'delete' => '/social_accounts/{id}'
                        ],
                        'notes' => [ 'get' => '/notes/social_networks/{id}', 
                                    'post' => '/notes/post',
                                    'store' => '/notes',
                                    ]
                    ];

    	return view('seo.social_accounts.index', compact('pivot_api', 'social_accounts_group', 'social_accounts', 'profiles', 'social_networks'));
    }

    /**
     * Returns all social accounts  associated with the currently logged in user.
     * @return [type] [description]
     */
    public function get(){
        return $this->user->socialAccounts()->get();
    }    


 /**
     * Adds/Creates a new SOCIAL_ACCOUNT
     * 
     * @param   $request : the data sent by the user to be added on the database
     * @return view
     */
    public function store(Request $request){
        $social_account = new SocialAccount;

        $social_account->profile_id         = $request['profile_id'];
        $social_account->social_network_id  = $request['social_network_id'];
        $social_account->username           = $request['username'];
        $social_account->password           = $request['password'];
        $social_account->profile_url        = $request['profile_url'];
        
        $social_account->save();
        return 'success';
    }

    /**
     * Update a SOCIAL_ACCOUNT
     * 
     * @param  Request $request : data sent by the user to be updated
     * @param  int $id : the SOCIAL_ACCOUNT id
     * @return json
     */
    public function update(Request $request, $id){
        $social_account      = SocialAccount::findOrFail($id);

        $social_account->username = $request['username'];
        $social_account->password = $request['password'];
        $social_account->profile_url = $request['profile_url'];

        $social_account->save();

        return "success";
    }

    /**
     * Delete a SOCIAL_ACCOUNT and all its related relationships.
     * 
     * @param  int $id : SOCIAL_ACCOUNT id
     * @return view
     */
    public function destroy($id){
        $social_account = SocialAccount::findOrFail($id);

        $social_account->delete();
        return "success";
    }    

   /**
     * Filter a set of data
     * 
     * @return \Illuminate\Pagination\LengthAwarePaginator : returns a paginator instance
     */
    public function api_filter(){
        if (Gate::denies('api')) {
            return redirect('/')->withErrors('Permission denied.');
        } 

        $params = Input::all();
        $data   = $this->user->socialAccountsDecrypted();//$this->user->socialNetworks()->with('profiles');
     
        $input  = [
            'path'  => Input::url(),
            'query' => Input::query(),
            'page'  => Input::has('page') ? Input::get('page') : 1
        ];

        foreach ($params as $key => $value){ $key != 'page' ? $data->where($key, '=', $value) : ''; }

        return $this->api_paginate($data, $input);

    }

    /**
     * Sort a set of data.
     * 
     * @param  string : the field to be sorted
     * @param  string : the sorting type
     * @return \Illuminate\Pagination\LengthAwarePaginator : returns a paginator instance
     */
    public function api_sort($field, $type){
        if (Gate::denies('api')) {
            return redirect('/')->withErrors('Permission denied.');
        } 

        $data = SocialNetwork::with('profiles')->orderBy($field, $type)->paginate(env('ITEMS_PER_PAGE'));

        return $data;
    }

    /**
     * Search for a keyword then return a filtered set of data.
     * 
     * @return \Illuminate\Pagination\LengthAwarePaginator : retur+ns a paginator instance
     */
    public function api_search(){
        if (Gate::denies('api')) {
            return redirect('/')->withErrors('Permission denied.');
        } 
        
        $params = Input::all();
        $data   = $this->user->socialAccountsDecrypted();

        $input  = [
            'path'  => Input::url(),
            'query' => Input::query(),
            'page'  => Input::has('page') ? Input::get('page') : 1
        ];

        foreach ($params as $key => $value){ $key != 'page' ? $data->orWhere($key, 'LIKE', '%'.$value.'%') : ''; }

        return $this->api_paginate($data, $input);

    }

    /**
     * Paginate a data source.
     * 
     * @param  array : data to be paginated
     * @param  array : the input
     * @return \Illuminate\Pagination\LengthAwarePaginator : returns a paginator instance
     */
    public function api_paginate($data, $input){
        $items_all          = $data;
        $items_per_page     = 1000;
        $items_retrieved    = $input['page'] * $items_per_page;
        $offset             = $items_retrieved - $items_per_page;         
       

        $current_items      = array_slice($items_all, $offset , $items_per_page);

        $items = new \Illuminate\Pagination\LengthAwarePaginator($current_items, count($items_all), $items_per_page, $input['page'], [
            'path'  => $input['path'],
            'query' => $input['query'],
        ]);

        return $items;
    }    
}
