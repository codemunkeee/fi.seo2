<?php

namespace App\Http\Controllers\Users;

use DB;
use Gate;
use App\Http\Requests;
use App\Models\SEO\Profile;
use Illuminate\Http\Request;
use App\Models\SEO\SocialNetwork;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\SEO\ProfileRequest;


class UsersProfilesController extends Controller
{
    /*
     * Initialize the controller 
     */
	public function __construct(){
        $this->middleware('auth', ['except' => ['showPublic', 'getPublic']]);
        //$this->middleware('auth');
        $this->user = \Auth::user();       
	}

    public function get($id){
        return Profile::findOrFail($id);
    }

    public function getPublic($name, $id){
        return Profile::findOrFail($id);
    }
    /**
     * Shows all profiles
     * 
     * @return view
     */
    public function index(){
        if (Gate::denies('profile-all')) {
            return redirect('/')->withErrors('You are not allowed to view this page.');
        } 

        $urls_list = $this->user->urls()->lists('url', 'id');

    	$profiles = $this->user->profiles()->orderBy('updated_at', 'desc')->get();

        //for morris chart
        $profile_group = DB::table('profile_social_accounts')
            ->select('social_networks.name AS label', DB::raw('COUNT("profile_social_accounts.id") as value'))
            ->leftJoin('social_networks', 'profile_social_accounts.social_network_id', '=', 'social_networks.id')
            ->leftJoin('user_social_networks', 'profile_social_accounts.social_network_id', '=', 'user_social_networks.social_network_id')
            ->where('user_social_networks.user_id', '=', $this->user->id)
            ->groupBy('social_networks.id')->get();

        $gender_group = $this->getGenderGroup();
        
        $user_social_accounts = $this->user->socialAccounts()->get();

        $pivot_api = [
                        'parent_name' => 'profiles',
                        'urls' => [ 'get' => '/profiles/{id}/urls', 
                                    'post' => '/urls/post',
                                    'store' => '/urls',
                                    'delete' => '/urls/'
                                    ],
                        'social_accounts' => [ 'get' => '/profiles/{id}/social_accounts'
                                    ],
                        'notes' => [ 'get' => '/notes/profiles/{id}', 
                                    'post' => '/notes/post',
                                    'store' => '/notes',
                                    'update' => '/notes/{id}',
                                    'delete' => '/notes/{id}'
                                    ],
                        'profile_social_accounts' => [ 'get' => '/profiles/{id}/social_accounts',
                                    'link_multiple' => '/profiles/{id}/social_accounts/link_multiple',
                                    'update_multiple' => '/profiles_social_accounts/multiple/',
                                    'update' => '/profiles_social_accounts/{id}',
                                    'delete' => '/profiles_social_accounts/{id}',
                                    'unselected_list' => '/profiles/{id}/social_accounts/unselected_list'
                                    ],                                                                                                            
                        'profile_urls' => [ 'get' => '/profiles/{id}/urls',
                                    'delete' => '/profile_urls/{id}',
                                    'link_multiple' => '/profiles/{id}/urls/link_multiple',
                                    'unselected_list' => '/profiles/{id}/urls/unselected_list'                
                                    ]                                                            
                    ];

    	return view('seo.profiles.index', compact('profiles', 'urls_list', 'gender_group', 'user_social_accounts', 'profile_group', 'pivot_api'));
    }

    /**
     * Show details for PROFILE
     * 
     * @param  int $id : the PROFILE id
     * @return view
     */
    public function show($id){
        if (Gate::denies('profile-show')) {
            return redirect('/')->withErrors('You are not allowed to view this page.');
        }
        
        $method   = 'show';
        $disabled = 'disabled';

        $disabled = '';
        $profile  = Profile::findOrFail($id);

        $this->user->owns($profile);
        // $ids = $this->user->socialNetworks()->get()->lists('id')->all();

        $social_networks = SocialNetwork::get()->lists('name', 'id')->all();
        // get the social networks associated with this profile
        $profile_social_accounts = $profile->socialNetworks()->get();

        // get the urls associated with the currently authenticated user
        $user_urls = $this->user->urls()->lists('url', 'id');
        // get the urls associated with the profile
        $profile_urls = $profile->urls()->get()->lists('id')->all();

        $profile_notes_social_networks = isset($profile->user($this->user)->notes_social_networks) ? $profile->user($this->user)->notes_social_networks : null;

        $profile_notes_urls = isset($profile->user($this->user)->notes_urls) ? $profile->user($this->user)->notes_urls : null;

        return view ('seo.profiles.form', compact('method', 'disabled', 'profile', 'social_networks', 'user_urls', 'profile_urls', 'profile_notes_social_networks', 'profile_notes_urls', 'profile_social_accounts'));

    }

    /**
     * Show details for profile publicly
     * 
     * @param  int $id : the profile id
     * @return view
     */
    public function showPublic($profile_name, $id){
    	$method   = 'show';
    	$disabled = 'disabled';

        $disabled = '';
        $profile  = Profile::findOrFail($id);

        $pivot_api = [
                        'parent_name' => 'profiles',
                        'profile_social_accounts' => [ 'get' => '/profiles/{id}/social_networks'],     
                        'profile_urls' => [ 'get' => '/profiles/{id}/urls']             
                    ];

        $sanitized_name = $this->sanitizeName($profile->first_name . $profile->middle_name . $profile->last_name);
        //check if profile_name is equal to the retrieved profile 
        if (!($profile_name === $sanitized_name)) {
            return redirect('/')->withErrors('You are not allowed to view this page.');
        }

        $profile_social_accounts = $profile->socialNetworksDecrypted();
        
        $profile_urls = $profile->urls()->get();        

        return view ('seo.profiles.form2', compact('method', 'disabled', 'profile', 'profile_social_accounts', 'profile_urls', 'pivot_api', 'sanitized_name'));        
    }

    public function sanitizeName($name){
        $name = strtolower($name);
        $name = str_replace(' ', '', $name);
        return $name;
    }

    /**
     * Show a create form to add a new profile
     * @return view
     */
    public function create(){
        if (Gate::denies('profile-create')) {
            return redirect('/')->withErrors('You are not allowed to view this page.');
        } 
    	$method   = 'create';
    	$disabled = '';
        // get user urls
        // $user_urls = $this->user_urls->lists('url', 'id');

    	return view('seo.profiles.form', compact('method', 'disabled'));
    }

    /**
     * Adds/Creates a new profile
     * 
     * @param  UsersprofilesRequest $request : the data sent by the user to be added on the database
     * @return view
     */
    public function store(ProfileRequest $request){
        if (Gate::denies('profile-store')) {
            return redirect('/')->withErrors('Permission denied.');
        } 

       	DB::transaction(function () use($request) {
	        $profile = Profile::create($request->all());
            //change profile's avatar

            if (\Input::file('avatar')){
                
                $avatar             = \Input::file('avatar');
                $destination_path   = 'assets/img/profiles/'.$profile->id.'/';    
                $original_file_name = \Input::file('avatar')->getClientOriginalName();
                $file_name          = str_random(8) . '.' . pathinfo($original_file_name, PATHINFO_EXTENSION);
                $profile->avatar    = 'http://'.$_SERVER['SERVER_NAME'].'/'.$destination_path .$file_name;

                while(file_exists($profile->avatar)){
                    $file_name     = str_random(8) . '.' . \Input::file('avatar')->getClientOriginalExtension();
                    $profile->avatar  = 'http://'.$_SERVER['SERVER_NAME'].'/'.$destination_path . $file_name;
                } 
            
                \Input::file('avatar')->move($destination_path, $file_name);  
                $profile->save();   
            }    
			$this->user->profiles()->attach($profile);					
		});


        //\Flash::success('New Profile added!');
        //return redirect('/profiles/');
        return 'success';
    }

    /**
     * Shows an editable form to update profile details
     * 
     * @param  int $id
     * @return view
     */
    public function edit($id){
    	$method   = 'edit';
    	$disabled = '';
    	$profile  = Profile::findOrFail($id);
        
        if (!(Gate::allows('profile-edit') && $this->user->owns($profile))) {
            return redirect('/')->withErrors('You are not allowed to view this page.');
        } 


        // $ids = $this->user->socialNetworks()->get()->lists('id')->all();

        $social_networks = SocialNetwork::get()->lists('name', 'id')->all();

        // $user_social_networks = $this->user->socialNetworks()->get()->lists('id')->all();

        // get the urls associated with the currently authenticated user
        $user_urls = $this->user->urls()->lists('url', 'id');
        // get the urls associated with the profile
        $profile_urls = $profile->urls()->get()->lists('id')->all();

        // // get the urls associated with this profile      
        // $profile_urls = $profile->urls()->get()->lists('id')->all();

        // // get the social networks associated with the currently authenticated user
        // $user_social_networks = $this->user->socialNetworks()->get()->all();  

        // // get the social networks associated with this profile
        // $profile_social_accounts = $profile->socialNetworks()->get()->lists('id')->all();
        
        //this code below needs refactoring. returns the notes_social_networks field on user_profiles table
        $profile_notes_social_networks = isset($profile->user($this->user)->notes_social_networks) ? $profile->user($this->user)->notes_social_networks : null;

        return view ('seo.profiles.form', compact('method', 'disabled', 'profile', 'social_networks', 'user_urls', 'profile_urls', 'profile_notes_social_networks'));
    	// return view ('seo.profiles.form', compact('method', 'disabled', 'profile', 'user_urls', 'profile_urls','user_social_networks', 'profile_social_accounts', 'social_networks'));
    }

    /**
     * Update a profile
     * 
     * @param  UsersprofilesRequest $request : data sent by the user to be updated
     * @param  int $id : the profile id
     * @return view
     */
    public function update(ProfileRequest $request, $id){
		$profile      = Profile::findOrFail($id);

        if (!(Gate::allows('profile-update') && $this->user->owns($profile))) {
            return redirect('/')->withErrors('Permission denied.');
        } 

        DB::transaction(function () use($request, $profile, $id) {
            if (\Input::file('avatar')){
                $avatar             = \Input::file('avatar');
                $destination_path   = 'assets/img/profiles/'.$profile->id.'/';    
                $original_file_name = \Input::file('avatar')->getClientOriginalName();
                $file_name          = str_random(8) . '.' . pathinfo($original_file_name, PATHINFO_EXTENSION);
                $profile->avatar    = 'http://'.$_SERVER['SERVER_NAME'].'/'.$destination_path .$file_name;

                while(file_exists($profile->avatar)){
                    $file_name     = str_random(8) . '.' . \Input::file('avatar')->getClientOriginalExtension();
                    $profile->avatar  = 'http://'.$_SERVER['SERVER_NAME'].'/'.$destination_path . $file_name;
                } 
            
                \Input::file('avatar')->move($destination_path, $file_name);  
                $profile->save();   
                $request['avatar'] = $profile->avatar;
            } 

            $profile->first_name    = $request['first_name']; 
            $profile->middle_name   = $request['middle_name'];
            $profile->last_name     = $request['last_name'];
            $profile->phone         = $request['phone'];
            $profile->city_address  = $request['city_address'];
            $profile->tx            = $request['tx'];
            $profile->zip           = $request['zip'];
            $profile->save();
            //$profile->update($request->all());  
        });

        return "success";
    }

    /**
     * Delete a profile and all its related relationships.
     * 
     * @param  UsersProfilesRequest $request : the profile to be deleted
     * @param  int $id : profile id
     * @return view
     */
    public function destroy($id){
        $profile = Profile::findOrFail($id);
        if (!(Gate::allows('profile-destroy') && $this->user->owns($profile))) {
            return abort(403, 'Unauthorized action.');
        } 

       DB::transaction(function () use($id, $profile) {

            // delete related urls for this profile
            $profile->urls()->detach();
            // delete related social networks for this profile
            $profile->socialNetworks()->detach();
            // delete related profiles for this user
            $this->user->profiles()->detach($profile);      
            // delete the profile
            Profile::destroy($id);
        });

    	\Flash::success('Profile was deleted!');
    	return redirect('/profiles/');
    }    

    public function test($id, $field){
        $profile  = Profile::findOrFail($id);
        // $ids = $this->user->socialNetworks()->get()->lists('id')->all();
        $social_networks = SocialNetwork::get()->lists('name', 'id')->all();

        return view('seo.profiles.profiles_social_networks.test', compact('profile', 'social_networks'));
    }

    /**
     * Filter a set of data
     * 
     * @return \Illuminate\Pagination\LengthAwarePaginator : returns a paginator instance
     */
    public function api_filter(){
        if (Gate::denies('api')) {
            return redirect('/')->withErrors('Permission denied.');
        } 

        $params = Input::all();
        $data   = $this->user->profiles()->with('socialNetworks', 'urls');

        $input  = [
            'path'  => Input::url(),
            'query' => Input::query(),
            'page'  => Input::has('page') ? Input::get('page') : 1
        ];

        foreach ($params as $key => $value){ $key != 'page' ? $data->where($key, '=', $value) : ''; }

        return $this->api_paginate($data, $input);

    }

    /**
     * Sort a set of data.
     * 
     * @param  string : the field to be sorted
     * @param  string : the sorting type
     * @return \Illuminate\Pagination\LengthAwarePaginator : returns a paginator instance
     */
    public function api_sort($field, $type){
        if (Gate::denies('api')) {
            return redirect('/')->withErrors('Permission denied.');
        } 

        $data = Profile::with('socialNetworks', 'urls')->orderBy($field, $type)->paginate(env('ITEMS_PER_PAGE'));
        return $data;
    }

    /**
     * Search for a keyword then return a filtered set of data.
     * 
     * @return \Illuminate\Pagination\LengthAwarePaginator : returns a paginator instance
     */
    public function api_search(){
        if (Gate::denies('api')) {
            return redirect('/')->withErrors('Permission denied.');
        } 
        
        $params = Input::all();
        $data   = Profile::with('socialNetworks', 'urls');
        $input  = [
            'path'  => Input::url(),
            'query' => Input::query(),
            'page'  => Input::has('page') ? Input::get('page') : 1
        ];

        foreach ($params as $key => $value){ $key != 'page' ? $data->orWhere($key, 'LIKE', '%'.$value.'%') : ''; }

        return $this->api_paginate($data, $input);

    }

    /**
     * Paginate a data source.
     * 
     * @param  array : data to be paginated
     * @param  array : the input
     * @return \Illuminate\Pagination\LengthAwarePaginator : returns a paginator instance
     */
    public function api_paginate($data, $input){
        $items_all          = $data->get()->all();
        $items_per_page     = 100;//env('API_ITEMS_PER_PAGE');
        $items_retrieved    = $input['page'] * $items_per_page;
        $offset             = $items_retrieved - $items_per_page;         
       
        $current_items      = array_slice($items_all, $offset , $items_per_page);

        $items = new \Illuminate\Pagination\LengthAwarePaginator($current_items, count($items_all), $items_per_page, $input['page'], [
            'path'  => $input['path'],
            'query' => $input['query'],
        ]);

        return $items;
    }

    public function getPercentage($max_count, $count){
        return round(($count / $max_count) * 100, 2);
    }

    public function getGenderGroup(){
        $male_count = 0;
        $female_count = 0;
        
        $gender_group_db = DB::table('user_profiles')
            ->select('profiles.gender as gender', DB::raw('COUNT("profiles.gender") as value'))
            ->leftJoin('profiles', 'user_profiles.profile_id', '=', 'profiles.id')
            ->where('user_profiles.user_id', '=', $this->user->id)
            ->whereNotNull('profiles.gender')
            ->groupBy('profiles.gender')->get();
        
        foreach($gender_group_db as $key=>$value){
            switch ($value->gender){
                case 'F':
                    $female_count = $value->value;
                    break;
                case 'M':
                    $male_count = $value->value;
                    break;
            }
        }

        $total = $female_count + $male_count;
        $percentage = $this->getPercentage($total , $male_count);
        $gender_group = ['male' => $percentage,
                        'female' => 100 - $percentage];
        return $gender_group;

    }

}