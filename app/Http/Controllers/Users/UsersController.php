<?php

namespace App\Http\Controllers\Users;

use Gate;
use App\Models\Role;
use App\Models\User;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class UsersController extends Controller
{

    public function __construct()
	{
		$this->middleware('auth');
		$this->user = \Auth::user();
		$this->roles = Role::all();
	}
	
	/**
	 * Show all users.
	 *
	 * @return Illuminate\Contracts\View\Factory
	 */
	public function index()
	{

		if (Gate::denies('admin')) {
            return redirect('/')->withErrors('You are not allowed to view this page.');
        } 


		//else, get all users and display them on the view.
		$users = User::all();
        
		return view('system.users.index', compact('users'));
	}

	/**
	 * Shows the detailed information for a user.
	 * 
	 * @param  int $id  user's id
	 * @return Illuminate\Contracts\View\Factory
	 */
	public function show($id){
		$user = User::findOrFail($id);
		if (Gate::denies('user-show', $user)) {
            return redirect('/')->withErrors('You are not allowed to view this page.');
        } 
		$method = 'show';
		return view('system.users.show', compact('user', 'method'));
	}

	/**
	 * Shows an editable information for a user.
	 * 
	 * @param  int $id [description] user's id
	 * @return Illuminate\Contracts\View\Factory
	 */
	public function edit($id){
		$user = User::findOrFail($id);

        if (!(Gate::allows('user-edit') && $this->user->ownsProfile($user))) {
            return redirect('/')->withErrors('You are not allowed to view this page.');
        } 
        $method     = 'edit';
		$roles 		= $this->roles->lists('role', 'id');
		$user_roles = $user->roles()->lists('id')->toArray();

		return view('system.users.edit', compact('user', 'roles', 'user_roles', 'method'));
	}	

	/**
	 * Update a user.
	 * 
	 * @param  Request $request [description] the update request passed from the view, contains the new information from the user
	 * @param  int  $id      [description] user's id to be updated
	 * @return Illuminate\Contracts\View\Factory
	 */
	public function update(Request $request, $id){

		$user 				= User::findOrFail($id);

        if (!(Gate::allows('user-update') && $this->user->ownsProfile($user))) {
            return redirect('/')->withErrors('Permission denied.');
        } 

		$user->first_name 	= $request['first_name'];
		$user->last_name 	= $request['last_name'];
		
		//change user's role
		$user->roles()->sync($request['roles']);
	
		//change user's avatar
		if (($request->file('avatar'))){
			$avatar            	= $request->file('avatar');
            $destination_path   = 'assets/img/users/'.$user->id.'/';        
            $file_name 			= str_random(8) . '.' . $request->file('avatar')->getClientOriginalExtension();
            $user->avatar    	= 'http://'.$_SERVER['SERVER_NAME'].'/'.$destination_path . $file_name;                    
            
            while(file_exists($user->avatar)){
            	 $file_name 	= str_random(8) . '.' . $request->file('avatar')->getClientOriginalExtension();
            	 $user->avatar  = 'http://'.$_SERVER['SERVER_NAME'].'/'.$destination_path . $file_name;
            } 

            $request->file('avatar')->move($destination_path, $file_name);     
        }
        $user->save();
		\Flash::success('User updated successfully!');
		return redirect('/users/'.$user->id.'/edit');
	}

	/**
	 * Removes a user from the system
	 * @param  Request $request [description] the delete request passed from the view
	 * @param  int  $id      [description] user's id to be destroyed
	 * @return view           [description] redirects the user to the users list
	 */
	public function destroy(Request $request, $id){
        return redirect('/users');
		$user = User::findOrFail($id);

        if (!(Gate::allows('user-destroy') && $this->user->ownsProfile($user))) {
            return redirect('/')->withErrors('Permission denied.');
        } 

		User::destroy($id);
		\Flash::success('Successfully deleted user!');
		return redirect('/users');
	}


    /**
     * Filter a set of data
     * 
     * @return \Illuminate\Pagination\LengthAwarePaginator : returns a paginator instance
     */
    public function api_filter(){
        if (Gate::denies('api')) {
            return redirect('/')->withErrors('Permission denied.');
        } 

        $params = Input::all();
        $data   = User::with('roles', 'hostings', 'urls', 'whois', 'personas', 'socialNetworks');
        $input  = [
            'path'  => Input::url(),
            'query' => Input::query(),
            'page'  => Input::has('page') ? Input::get('page') : 1
        ];

        foreach ($params as $key => $value){ $key != 'page' ? $data->where($key, '=', $value) : ''; }

        return $this->api_paginate($data, $input);

    }

    /**
     * Sort a set of data.
     * 
     * @param  string : the field to be sorted
     * @param  string : the sorting type
     * @return \Illuminate\Pagination\LengthAwarePaginator : returns a paginator instance
     */
    public function api_sort($field, $type){
        if (Gate::denies('api')) {
            return redirect('/')->withErrors('Permission denied.');
        } 

        $data = User::with('roles', 'hostings', 'urls', 'whois', 'personas', 'socialNetworks')->orderBy($field, $type)->paginate(env('ITEMS_PER_PAGE'));

        return $data;
    }

    /**
     * Search for a keyword then return a filtered set of data.
     * 
     * @return \Illuminate\Pagination\LengthAwarePaginator : returns a paginator instance
     */
    public function api_search(){
        if (Gate::denies('api')) {
            return redirect('/')->withErrors('Permission denied.');
        } 
        
        $params = Input::all();
        $data   = User::with('roles', 'hostings', 'urls', 'whois', 'personas', 'socialNetworks');
        $input  = [
            'path'  => Input::url(),
            'query' => Input::query(),
            'page'  => Input::has('page') ? Input::get('page') : 1
        ];

        foreach ($params as $key => $value){ $key != 'page' ? $data->orWhere($key, 'LIKE', '%'.$value.'%') : ''; }

        return $this->api_paginate($data, $input);

    }

    /**
     * Paginate a data source.
     * 
     * @param  array : data to be paginated
     * @param  array : the input
     * @return \Illuminate\Pagination\LengthAwarePaginator : returns a paginator instance
     */
    public function api_paginate($data, $input){
        $items_all          = $data->get()->all();
        $items_per_page     = env('API_ITEMS_PER_PAGE');
        $items_retrieved    = $input['page'] * $items_per_page;
        $offset             = $items_retrieved - $items_per_page;         
       
        $current_items      = array_slice($items_all, $offset , $items_per_page);

        $items = new \Illuminate\Pagination\LengthAwarePaginator($current_items, count($items_all), $items_per_page, $input['page'], [
            'path'  => $input['path'],
            'query' => $input['query'],
        ]);

        return $items;
    }	
}
