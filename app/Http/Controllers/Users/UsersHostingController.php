<?php


/**
 * DEPRECATED CONTROLLER.. NOT USED ANYMORE
 *
 * Sept. 15, 2016
 */



namespace App\Http\Controllers\Users;

use DB;
use Gate;
use Validator;
use App\Http\Requests;
use App\Models\SEO\Hosting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\Users\UsersHostingRequest;


class UsersHostingController extends Controller
{

  /**
   *  Initialize the controller
   */
	public function __construct(){
		$this->middleware('auth');
        $this->user = \Auth::user();
	}

  /**
   * Returns the list of User's hosting
   * @return view
   */
 	public function index(){ 
        if (Gate::denies('hosting-all')) {
            return redirect('/')->withErrors('You are not allowed to view this page.');
        } 
        $method = 'index';
        $disabled = '';
    	$hostings = $this->user->hostings()->orderBy('updated_at', 'desc')->get();

    	return view('seo.hosting.index', compact('hostings', 'method', 'disabled'));
    }

    /**
     * Returns a view to create a new hosting
     * @return view
     */
    public function create(){
        if (Gate::denies('hosting-create')) {
            return redirect('/')->withErrors('You are not allowed to view this page.');
        } 

    	$method		= 'create';
		$disabled 	= '';
    	
    	return view('seo.hosting.form', compact('method', 'disabled'));
    }

    /**
     * Stores the new hosting to the database.
     * 
     * @param  UsersHostingRequest $request : the request that will be sent on the server
     * @return view
     */
    public function store(UsersHostingRequest $request){
        if (Gate::denies('hosting-store')) {
            return redirect('/')->withErrors('Permission denied.');
        } 

    	//add new hosting to the database and add a pivot table
		DB::transaction(function () use($request) {
			//get the maximum count of this hostname
  			$max = $this->user->hostings()
  				->where('host_name', $request['host_name'])
  				->max('host_number') + 1;			 	

  			$hosting = Hosting::create([
  				'host_name' 	=> $request['host_name'],
  				'host_number' 	=> $max,
  				'login_url' 	=> $request['login_url'],
  				'username'  	=> $request['username'],
  				'password'  	=> $request['password'],
                'notes'         => $request['notes']
  			]);

  			$hosting->users()->attach($hosting->id, ['user_id' => $this->user->id]);
  		});

		  return redirect('hosting');
    }

    /**
     * Shows a particular Hosting
     * 
     * @param  int $id : the id of the hosting
     * @return view
     */
    public function show($id){
        if (Gate::denies('hosting-show')) {
            return redirect('/')->withErrors('You are not allowed to view this page.');
        } 

    	$method		= 'show';
		$disabled 	= 'disabled';

    	$hosting = Hosting::findOrFail($id);

    	return view('seo.hosting.form', compact('hosting', 'method', 'disabled'));
    }

    /**
     * Returns a view to edit a hosting
     * 
     * @param  int $id : the id of the hosting
     * @return Illuminate\Contracts\View\Factory
     */
    public function edit($id){
        $hosting = Hosting::findOrFail($id);
        
        if (!(Gate::allows('hosting-edit') && $this->user->owns($hosting))) {
            return redirect('/')->withErrors('You are not allowed to view this page.');
        } 

    	$method		= 'edit';
		$disabled 	= '';

    	return view('seo.hosting.form', compact('hosting', 'method', 'disabled'));
    }

    /**
     * Updates the hosting
     * 
     * @param  UsersHostingRequest $request : the update request that will be sent on the server
     * @param  int $id : the id of the hosting
     * @return view
     */
    public function update(UsersHostingRequest $request, $id){
        $hosting = Hosting::findOrFail($id);

        if (!(Gate::allows('hosting-update') && $this->user->owns($hosting))) {
            return redirect('/')->withErrors('Permission denied.');
        } 

    	
    	$hosting->host_name 	= $request['host_name'];
        $hosting->login_url 	= $request['login_url'];
        $hosting->username  	= $request['username'];
    	$hosting->password  	= $request['password'];
        $hosting->notes         = $request['notes'];

    	$hosting->save();

    	\Flash::success('Hosting updated successfully!');
    	return redirect('/hosting/'.$hosting->id.'/edit');
    }   

    /**
     * Deletes a hosting
     * 
     * @param  int $id : the id of the hosting
     * @return view
     */
    public function destroy($id){
        $hosting = Hosting::findOrFail($id);
        
        if (!(Gate::allows('hosting-destroy') && $this->user->owns($hosting))) {
            return redirect('/')->withErrors('Permission denied.');
        } 

        DB::transaction(function () use($id) { 
            DB::table('urls')->where('hosting_id', '=', $id)
                ->update(['hosting_id' => null]);
            //delete pivot
            DB::table('user_hostings')
                ->where('user_id', '=', $this->user->id)
                ->where('hosting_id', '=', $id)
                ->delete();

            Hosting::destroy($id);
        });

        return redirect('/hosting');   
    }

    /**
     * Filter a set of data
     * 
     * @return \Illuminate\Pagination\LengthAwarePaginator : returns a paginator instance
     */
    public function api_filter(){
        if (Gate::denies('api')) {
            return redirect('/')->withErrors('Permission denied.');
        } 

        $params = Input::all();
        $data = $this->user->hostings();
        $input  = [
            'path'  => Input::url(),
            'query' => Input::query(),
            'page'  => Input::has('page') ? Input::get('page') : 1
        ];

        foreach ($params as $key => $value){ $key != 'page' ? $data->where($key, '=', $value) : ''; }

        return $this->api_paginate($data, $input);

    }

    /**
     * Sort a set of data.
     * 
     * @param  string : the field to be sorted
     * @param  string : the sorting type
     * @return \Illuminate\Pagination\LengthAwarePaginator : returns a paginator instance
     */
    public function api_sort($field, $type){
        if (Gate::denies('api')) {
            return redirect('/')->withErrors('Permission denied.');
        } 

        $data = $this->user->hostings()->orderBy($field, $type)->paginate(env('ITEMS_PER_PAGE'));

        return $data;
    }

    /**
     * Search for a keyword then return a filtered set of data.
     * 
     * @return \Illuminate\Pagination\LengthAwarePaginator : returns a paginator instance
     */
    public function api_search(){
        if (Gate::denies('api')) {
            return redirect('/')->withErrors('Permission denied.');
        } 
        
        $params = Input::all();
        $data   = $this->user->hostings();
        $input  = [
            'path'  => Input::url(),
            'query' => Input::query(),
            'page'  => Input::has('page') ? Input::get('page') : 1
        ];

        foreach ($params as $key => $value){ $key != 'page' ? $data->where($key, 'LIKE', '%'.$value.'%') : null; }

        return $this->api_paginate($data, $input);

    }

    /**
     * Paginate a data source.
     * 
     * @param  array : data to be paginated
     * @param  array : the input
     * @return \Illuminate\Pagination\LengthAwarePaginator : returns a paginator instance
     */
    public function api_paginate($data, $input){
        $items_all          = $data->get()->all();
        $items_per_page     = env('API_ITEMS_PER_PAGE');
        $items_retrieved    = $input['page'] * $items_per_page;
        $offset             = $items_retrieved - $items_per_page;         
       
        $current_items      = array_slice($items_all, $offset , $items_per_page);

        $items = new \Illuminate\Pagination\LengthAwarePaginator($current_items, count($items_all), $items_per_page, $input['page'], [
            'path'  => $input['path'],
            'query' => $input['query'],
        ]);

        return $items;
    }
}
