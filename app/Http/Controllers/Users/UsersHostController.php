<?php

namespace App\Http\Controllers\Users;

use Illuminate\Http\Request;

use DB;
use Gate;
use App\Http\Requests;
use App\Models\SEO\Host;
use App\Models\SEO\HostCompany;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\Users\UsersHostRequest;

class UsersHostController extends Controller
{
    public function __construct(){
    	$this->middleware('auth');
    	$this->items_per_page = intval(env('API_ITEMS_PER_PAGE'));
    	$this->user = \Auth::user();
    }

    public function index(){

        // if (Gate::denies('hosts-all')) {
     //        return redirect('/')->withErrors('You are not allowed to view this page.');
     //    } 
        $method = 'index';
        $disabled = '';
    	$hosts = $this->user->hosts()->orderBy('updated_at', 'desc')->get();
        
        $urls = $this->user->urls()->get();

        $ips = $this->user->urlIpCount()->get();

        $host_companies = HostCompany::get()->all();
        $host_companies_list = HostCompany::lists('name', 'id');
        
        $host_group = $this->user->hostCount()->get();
        
        $pivot_api = [
                        'parent_name' => 'hosts',
                        'urls' => [ 'get' => '/urls/test/host_urls/{id}', 
                                    'post' => '/urls/post',
                                    'store' => '/urls',
                                    'delete' => '/urls/',
                                    'unlink' => '/urls/unlink/' 
                                    ], 
                        'notes' => [ 'get' => '/notes/hosts/{id}', 
                                    'post' => '/notes/post',
                                    'store' => '/notes',
                                    'update' => '/notes/{id}',
                                    'delete' => '/notes/{id}'
                                    ]
                    ];

        return view('seo.hosts.index', compact('hosts', 'host_companies', 'host_companies_list', 'host_group', 'urls', 'ips', 'pivot_api', 'method', 'disabled'));
    }

    public function create(){
    	return 'create';
    }

    public function store(UsersHostRequest $request){
        
        // if (Gate::denies('hosting-store')) {
        //     return redirect('/')->withErrors('Permission denied.');
        // }

        $hosting_cost = $request['hosting_cost'];
        if ($request->payment_type['type'] == 'Annually'){
            $hosting_cost = $request['hosting_cost'] / 12;
        }
        //add new hosting to the database and add a pivot table
        DB::transaction(function () use($request) {
     
            $host = Host::create([
                'friendly_name' => $request['friendly_name'],
                'host_name'     => $request['host_name'],
                'hosting_cost'  => $request['hosting_cost'],
                'login_url'     => $request['login_url'],
                'username'      => $request['username'],
                'password'      => $request['password']
            ]);

            //create host company if not exists
            $host_company = HostCompany::find($request['host_company']);

            if (!$host_company){
                $host_company = HostCompany::create(['name'=>$request['host_company']]);
            } 

            $host->users()->attach($host->id, ['user_id' => $this->user->id, 'host_company_id' => $host_company->id]);
        });

        return redirect('hosts');
    }

    public function update(Request $request){

        $host = Host::findOrFail($request->id);
        $host->login_url    = $request->login_url;
        $host->username     = $request->username;
        $host->password     = $request->password;
        $host->save();

        return $host;
    }


    /**
     * Deletes a host
     * 
     * @param  int $id : the id of the host
     * @return view
     */
    public function destroy($id){
        $host = Host::findOrFail($id);

        if (!(Gate::allows('host-destroy') && $this->user->owns($host))) {
            return redirect('/')->withErrors('Permission denied.');
        } 

        DB::transaction(function () use($id) { 
            DB::table('urls')->where('host_id', '=', $id)
                ->update(['host_id' => null]);
            //delete pivot
            DB::table('user_hosts')
                ->where('user_id', '=', $this->user->id)
                ->where('host_id', '=', $id)
                ->delete();

            Host::destroy($id);
        });

        return 'success';   
    }


    /**
     * Filter a set of data
     * 
     * @return \Illuminate\Pagination\LengthAwarePaginator : returns a paginator instance
     */
    public function api_filter(){
        if (Gate::denies('api')) {
            return redirect('/')->withErrors('Permission denied.');
        } 

        $params = Input::all();
        $data = $this->user->hosts();
        $input  = [
            'path'  => Input::url(),
            'query' => Input::query(),
            'page'  => Input::has('page') ? Input::get('page') : 1
        ];

        foreach ($params as $key => $value){ $key != 'page' ? $data->where($key, '=', $value) : ''; }

        return $this->api_paginate($data, $input);

    }

     /**
     * Paginate a data source.
     * 
     * @param  array : data to be paginated
     * @param  array : the input
     * @return \Illuminate\Pagination\LengthAwarePaginator : returns a paginator instance
     */
    public function api_paginate($data, $input){
        $items_all          = $data->get()->all();
        $items_per_page     = 10;//$this->items_per_page; //env('API_ITEMS_PER_PAGE');
        $items_retrieved    = $input['page'] * $items_per_page;
        $offset             = $items_retrieved - $items_per_page;         
       
        $current_items      = array_slice($items_all, $offset , $items_per_page);

        $items = new \Illuminate\Pagination\LengthAwarePaginator($current_items, count($items_all), $items_per_page, $input['page'], [
            'path'  => $input['path'],
            'query' => $input['query'],
        ]);

        return $items;
    }
}
