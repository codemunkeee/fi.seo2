<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Input;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\SEO\Profile;
use Gate;
use DB;

class ApiController extends Controller
{

	public function __construct(){
		$this->dictionary = [
			'urls' 			=> '\App\Models\SEO\Url',
			'hosting' 		=> '\App\Models\SEO\Hosting',
			'permissions' 	=> '\App\Models\SEO\Permission',
			'profiles' 		=> '\App\Models\SEO\Profile',
			'roles' 		=> '\App\Models\SEO\Role',
			'whois' 		=> '\App\Models\SEO\Whois'
		];
	}

	public function api_filter($table, $column, $id)
	{
		//needs refactoring here
		if (Gate::denies('api')) {
            return redirect('/')->withErrors('You are not allowed to view this page.');
        } 

		// if ($table == 'personas') {
		// 	$persona = Persona::findOrFail($id);
  //   	    $persona_social_accounts = $persona->socialAccountsDecrypted();
  //       	return $persona_social_accounts;
		// }

		$items = \DB::table($table)->where($column, '=', $id)->get();
		return $items;
	}


	public function api($table)
	{	

		if (Gate::denies('api')) {
            return redirect('/')->withErrors('Permission denied.');
        } 
		$items = \DB::table($table)->orderBy('updated_at', 'desc')->paginate(20);
  	 	return $items;
	}

	public function filter($table, $keyword){
		if (Gate::denies('api')) {
            return redirect('/')->withErrors('Permission denied.');
        } 

		$query = DB::table($table);
		$columns = DB::getSchemaBuilder()->getColumnListing($table);
		foreach ($columns as $column){
			$query->where($column, $keyword);
		}
		return $query->paginate(20);

	}

	public function sort($table, $type){
		if (Gate::denies('api')) {
            return redirect('/')->withErrors('Permission denied.');
        } 
		$query 		= DB::table($table);
		$columns = DB::getSchemaBuilder()->getColumnListing($table);
		
		foreach ($columns as $column){
			$query->orderBy($column, $type);
		}
		
		return $query->paginate(20);
	}

	public function search($table, $keyword){
		if (Gate::denies('api')) {
            return redirect('/')->withErrors('Permission denied.');
        } 		
		$query 		= DB::table($table);
		$columns = DB::getSchemaBuilder()->getColumnListing($table);

		foreach ($columns as $column){
			$query->orWhere($column, 'LIKE',  '%'. $keyword . '%');
		}

		return $query->paginate(20) ;
	}

	public function api_decrypted($table, $id){
		//needs refactoring here
		if (Gate::denies('api')) {
            return redirect('/')->withErrors('You are not allowed to view this page.');
        } 

		$profile = Profile::findOrFail($id);
    	$profile_social_accounts = $profile->socialAccountsDecrypted();
        return $profile_social_accounts;
	}
	
	public function api_select($table)
	{	

		if (Gate::denies('api')) {
            return redirect('/')->withErrors('Permission denied.');
        } 
		$items = \DB::table($table)->orderBy('updated_at', 'desc')->get();
  	 	return $items;
	}

}
