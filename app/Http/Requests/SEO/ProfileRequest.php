<?php

namespace App\Http\Requests\SEO;

use App\Http\Requests\Request;

class ProfileRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //return false;
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'first_name' => 'required',
                    'email' => 'email'
                ];
            }
            case 'PUT':
            {
                return [
                    'first_name' => 'required',
                    'email' => 'email'
                ];
            }
            case 'PATCH':
            {
                return [
                    'first_name' => 'required',
                    'email' => 'email'
                ];
            }
            default:break;
        }
    }
}