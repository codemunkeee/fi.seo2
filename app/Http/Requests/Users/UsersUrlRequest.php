<?php

namespace App\Http\Requests\Users;

use App\Http\Requests\Request;

class UsersUrlRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'url'               => 'required|no_protocol',
            'cpanel_login_url'  => 'no_protocol',
            'ftp_login_address' => 'no_protocol'

        ];
    }

    public function messages()
    {
        return [
            'url.no_protocol' => 'Please provide a valid URL on the "URL" field.',
            'cpanel_login_url.no_protocol'  => 'Please provide a valid URL on the "Cpanel Login URL" field.',
            'ftp_login_address.no_protocol' => 'Please provide a valid URL on the "FTP Login Address" field.'
        ];
    }
}
