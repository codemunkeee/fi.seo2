<?php

namespace App\Http\Requests\Users;

use App\Http\Requests\Request;

class UsersPersonasRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'first_name' => 'required',
                    'email' => 'email'
                ];
            }
            case 'PUT':
            {
                return [
                    'first_name' => 'required',
                    'email' => 'email'
                ];
            }
            case 'PATCH':
            {
                return [
                    'first_name' => 'required',
                    'email' => 'email'
                ];
            }
            default:break;
        }
    }

    // public function messages()
    // {
    //     return [
    //         'name.required' => 'Please provide a brief link description',
    //         'url.required' => 'Please provide a URL',
    //         'url.url' => 'A valid URL is required',
    //         'category.required' => 'Please associate this link with a category',
    //         'category.min' => 'Please associate this link with a category'
    //     ];
    // }

}