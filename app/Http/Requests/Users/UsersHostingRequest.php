<?php

namespace App\Http\Requests\Users;

use App\Http\Requests\Request;

class UsersHostingRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'host_name' => 'required|max:255',
            'login_url' => 'required|no_protocol'
        ];
    }

    public function messages()
    {
        return [
            'login_url.no_protocol' => 'Please provide a valid URL for the "Login URL".'
        ];
    }
}