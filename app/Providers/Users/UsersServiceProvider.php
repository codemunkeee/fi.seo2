<?php

namespace App\Providers\Users;

use Validator;
use Illuminate\Support\ServiceProvider;

class UsersServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerValidationRules($this->app['validator']);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    protected function registerValidationRules(\Illuminate\Contracts\Validation\Factory $validator)
    {
        // $validator->extend('capitalize_first', '\App\Validation\Users\UsersWhoisValidator@validateCapitalizeFirst');
        $validator->extend('no_protocol', '\App\Validation\Users\UsersUrlValidator@validateNoProtocol');
    }
}
