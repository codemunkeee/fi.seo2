<?php

namespace App\Providers;

use App\Models\Users\Permission;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate);

        $gate->before(function ($user, $ability) {
            if ($user->hasRole('admin')) {
                return true;
            }
        });

        foreach ($this->getPermissions() as $permission){        
            $gate->define($permission->slug, function($user) use ($permission) {
                return $user->hasRole($permission->roles);
            });
        }
    }

    /**
     * Get all permissions.
     * 
     * @return Illuminate\Database\Eloquent\Collection 
     */
    protected function getPermissions(){
        return Permission::with('roles')->get();
    }

 
}