<?php

namespace App\Models\SEO;


use DB;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table="profiles";
    protected $fillable = [
    	'cpanel_address',
        'cpanel_username',
        'cpanel_password',
        'ftp_address',
        'ftp_user',
        'ftp_password',
        'wp_admin',
        'wp_password',
        'about',
        'description',            
        'topic',
        'email_login_link',
        'email',
        'email_password',
        'avatar',
        'main_username',
        'main_password',
        'first_name',
        'middle_name',
        'last_name',
        'gender',
        'birthday',
        'phone',
        'city_address',
        'tx',
        'zip'
    ];


    /**
     * Get the USER associated with this PROFILE
     * 
     * @return Illuminate\Database\Eloquent\Relations\BelongsToMany 
    */
    public function users(){
        return $this->belongsToMany('\App\Models\User', 'user_profiles', 'profile_id', 'user_id')->withTimestamps();
    }

    /**
     * Get the URLS associated with this PROFILE
     * 
     * @return Illuminate\Database\Eloquent\Relations\BelongsToMany 
    */
    public function urls(){
        return $this->belongsToMany('\App\Models\SEO\Url', 'profile_urls', 'profile_id', 'url_id')->withTimestamps();
    }

    /**
     * Get the SOCIAL_NETWORK with PROFILES_SOCIAL_ACCOUNT pivot associated with this PROFILE
     * 
     * @return Illuminate\Database\Eloquent\Relations\BelongsToMany 
    */
    public function socialNetworks(){
        return $this->belongsToMany('\App\Models\SEO\SocialNetwork', 'profile_social_accounts', 'profile_id', 'social_network_id')->withTimestamps()->withPivot('username', 'password', 'profile_url');
    }

    /**
     * Gets all the URLS that are not yet associated with a PROFILE
     * 
     * @return Illuminate\Database\Eloquent\Collection : a collection of urls
     */
    public function unselectedUrls(){
        return DB::table('user_urls')
                ->whereNotIn('user_urls.url_id', function($q){
                    $q->select('profile_urls.url_id as url_id')->from('profile_urls')
                    ->where('profile_urls.profile_id', '=', $this->id);
                })->join('urls', 'user_urls.url_id', '=', 'urls.id')
                ->where('user_urls.user_id', '=', \Auth::user()->id)
                ->select('urls.*');                                       

    }
    
     /**
     * Gets all the SOCIAL_NETWORKSs that are not yet associated with a PROFILE
     * 
     * @return Illuminate\Database\Eloquent\Collection : a collection of App\Models\SEO\SocialNetwork
     */
    public function unselectedSocialNetworks(){

        return DB::table('user_social_accounts')
                ->whereNotIn('user_social_accounts.social_account_id', function($q){
                    $q->select('profile_social_accounts.social_account_id as social_account_id')->from('profile_social_accounts')
                    ->where('profile_social_accounts.profile_id', '=', $this->id);
                })->join('social_accounts', 'user_social_accounts.social_account_id', '=', 'social_accounts.id')
                ->where('user_social_accounts.user_id', '=', \Auth::user()->id)
                ->select('social_accounts.*');

        // return DB::table('user_urls')
        //         ->whereNotIn('user_urls.url_id', function($q){
        //             $q->select('profile_urls.url_id as url_id')->from('profile_urls')
        //             ->where('profile_urls.profile_id', '=', $this->id);
        //         })->join('urls', 'user_urls.url_id', '=', 'urls.id')
        //         ->where('user_urls.user_id', '=', \Auth::user()->id)
        //         ->select('urls.*');                                                       

    }

     /**
     * Gets all the SOCIAL_ACCOUNTs that are associated with a PROFILE
     * @return Illuminate\Database\Eloquent\Collection : a collection of SOCIAL_ACCOUNT
     */
    // this code needs refactor. like, really.
    public function socialAccountsDecrypted(){
        $social_accounts = $this->belongsToMany('\App\Models\SEO\SocialNetwork', 'profile_social_accounts', 'profile_id', 'social_network_id')->withTimestamps()->withPivot('id', 'username', 'password', 'profile_url')->get()->all();

        for($i = 0; $i < count($social_accounts); $i++){
            $social_accounts[$i]->pivot->username       = (!isset($social_accounts[$i]->pivot->username) || trim($social_accounts[$i]->pivot->username)==='') ? '' :  \Crypt::decrypt($social_accounts[$i]->pivot->username);
            $social_accounts[$i]->pivot->password       = (!isset($social_accounts[$i]->pivot->password) || trim($social_accounts[$i]->pivot->password)==='') ? '' : \Crypt::decrypt($social_accounts[$i]->pivot->password);
            $social_accounts[$i]->pivot->profile_url    = (!isset($social_accounts[$i]->pivot->profile_url) || trim($social_accounts[$i]->pivot->profile_url)==='') ? '' : \Crypt::decrypt($social_accounts[$i]->pivot->profile_url);            
        }

        return $social_accounts;
    }

    public function socialAccount($social){
        foreach($this->socialAccounts as $social_account){
            if($social_account->id==$social->id){
                return $social_account->pivot;
            }
        }  
        return null;   
    }

    public function socialAccountGroup(){
        return DB::table('profile_social_accounts')
            ->select('social_accounts.name AS label', DB::raw('COUNT("profile_social_accounts.id") as value'))
            ->leftJoin('social_accounts', 'profile_social_accounts.social_account_id', '=', 'social_accounts.id')
            ->groupBy('social_accounts.id');
    }

    public function user($user){
        foreach($this->users as $_user){
            if($_user->id==$user->id){
                return $_user->pivot;
            }
        }  
        return null;   
    }


    public function setFirstNameAttribute($value){
        $this->attributes['first_name'] = ucfirst($value);
    }

    public function setMiddleNameAttribute($value){
        $this->attributes['middle_name'] = ucfirst($value);
    }

    public function setLastNameAttribute($value){
        $this->attributes['last_name'] = ucfirst($value);
    }

    public function setBirthdayAttribute($value){
      $date_parsed = $value ? \Carbon\Carbon::parse($value)->format('Y-m-d H:i:s') : null;

      $this->attributes['birthday'] = $date_parsed ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date_parsed) : null;
    }

    public function getBirthdayAttribute($value){
        $date = $value ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value) : null;
      return $this->attributes['birthday'] = $date;  
    }

    public function setCpanelAddressAttribute($value){
        $this->attributes['cpanel_address'] = $value === '' ? null :  \Crypt::encrypt($value); 
    }

    public function getCpanelAddressAttribute($value){
        return $this->attributes['cpanel_address'] = is_null($value) ? null : \Crypt::decrypt($value); 
    }

    public function setCpanelUsernameAttribute($value){
        $this->attributes['cpanel_username'] =  $value === '' ? null :  \Crypt::encrypt($value); 
    }

    public function getCpanelUsernameAttribute($value){
        return $this->attributes['cpanel_username'] = is_null($value) ? null : \Crypt::decrypt($value); 
    }

    public function setCpanelPasswordAttribute($value){
        $this->attributes['cpanel_password'] =  $value === '' ? null :  \Crypt::encrypt($value); 
    }

    public function getCpanelPasswordAttribute($value){
        return $this->attributes['cpanel_password'] = is_null($value) ? null : \Crypt::decrypt($value); 
    }

    public function setFtpAddressAttribute($value){
        $this->attributes['ftp_address'] =  $value === '' ? null :  \Crypt::encrypt($value);  
    }

    public function getFtpAddressAttribute($value){
        return $this->attributes['ftp_address'] = is_null($value) ? null : \Crypt::decrypt($value); 
    }

    public function setFtpUserAttribute($value){
        $this->attributes['ftp_user'] = $value === '' ? null :  \Crypt::encrypt($value); 
    }

    public function getFtpUserAttribute($value){
        return $this->attributes['ftp_user'] = is_null($value) ? null : \Crypt::decrypt($value); 
    }

    public function setFtpPasswordAttribute($value){
        $this->attributes['ftp_password'] = $value === '' ? null :  \Crypt::encrypt($value); 
    }

    public function getFtpPasswordAttribute($value){
        return $this->attributes['ftp_password'] = is_null($value) ? null : \Crypt::decrypt($value); 
    }

    public function setWpAdminAttribute($value){
        $this->attributes['wp_admin'] =  $value === '' ? null :  \Crypt::encrypt($value);  
    }

    public function getWpAdminAttribute($value){
        return $this->attributes['wp_admin'] = is_null($value) ? null : \Crypt::decrypt($value); 
    }    

    public function setWpPasswordAttribute($value){
        $this->attributes['wp_password'] =  $value === '' ? null :  \Crypt::encrypt($value);  
    }

    public function getWpPasswordAttribute($value){
        return $this->attributes['wp_password'] = is_null($value) ? null : \Crypt::decrypt($value); 
    }    

    public function setEmailLoginLinkAttribute($value){
        $this->attributes['email_login_link'] =  $value === '' ? null :  \Crypt::encrypt($value); 
    }

    public function getEmailLoginLinkAttribute($value){
        return $this->attributes['email_login_link'] = is_null($value) ? null : \Crypt::decrypt($value); 
    }    

    public function setEmailAttribute($value){
        $this->attributes['email'] = $value === '' ? null :  \Crypt::encrypt($value); 
    }

    public function getEmailAttribute($value){
        return $this->attributes['email'] = is_null($value) ? null : \Crypt::decrypt($value); 
    }    

    public function setEmailPasswordAttribute($value){
        $this->attributes['email_password'] =  $value === '' ? null :  \Crypt::encrypt($value); 
    }

    public function getEmailPasswordAttribute($value){
        return $this->attributes['email_password'] = is_null($value) ? null : \Crypt::decrypt($value); 
    }    

    public function setMainUsernameAttribute($value){
        $this->attributes['main_username'] =  $value === '' ? null :  \Crypt::encrypt($value); 
    }

    public function getMainUsernameAttribute($value){
        return $this->attributes['main_username'] = is_null($value) ? null : \Crypt::decrypt($value); 
    }    

    public function setMainPasswordAttribute($value){
        $this->attributes['main_password'] = $value === '' ? null :  \Crypt::encrypt($value); 
    }

    public function getMainPasswordAttribute($value){
        return $this->attributes['main_password'] = is_null($value) ? null : \Crypt::decrypt($value); 
    }    

}
