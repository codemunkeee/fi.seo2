<?php

namespace App\Models\SEO;

use Illuminate\Database\Eloquent\Model;

class ProfileUrl extends Model
{
    protected $table    = "profile_urls";

    protected $fillable = [
        'profile_id' , 
        'url_id'
        ];


}
