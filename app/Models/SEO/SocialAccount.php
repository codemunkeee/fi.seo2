<?php

namespace App\Models\SEO;

use DB;
use Illuminate\Database\Eloquent\Model;

class SocialAccount extends Model
{
    protected $table    = "profile_social_accounts";

    protected $fillable = ['profile_id', 'social_network_id', 'username', 'password', 'profile_url'];

    /**
     * Get the SOCIAL_ACCOUNTS associated with ths USER
     * 
     * @return Illuminate\Database\Eloquent\Relations\BelongsToMany 
    */
    public function get(){
        // return DB::table('profile_social_accounts AS A')
        //     ->select('A.*')
        //     ->join('user_profiles AS B', 'A.profile_id', '=', 'B.profile_id')
        //     ->where('B.user_id', '=', $this->id);
    }

    public function profiles(){
    	return $this->belongsToMany('\App\Models\SEO\Profile', 'profile_social_accounts', 'profile_id')->withTimestamps()->where('user_profiles.user_id', '=', \Auth::user()->id);
    }


    public function setUsernameAttribute($value){
        $this->attributes['username'] =  \Crypt::encrypt($value); 
    }

    public function setPasswordAttribute($value){
        $this->attributes['password'] =  \Crypt::encrypt($value); 
    }

    public function setProfileUrlAttribute($value){
        $this->attributes['profile_url'] =  \Crypt::encrypt($value); 
    }


}
