<?php

namespace App\Models\SEO;

use Illuminate\Database\Eloquent\Model;

class ProfileSocialAccount extends Model
{
    protected $table    = "profile_social_accounts";

    protected $fillable = [
        'profile_id' , 
        'social_network_id', 
        'username', 
        'password',
        'profile_url'];


    public function setUsernameAttribute($value){
    	$this->attributes['username'] =  \Crypt::encrypt($value); 
    }

    // public function getUsernameAttribute($value){
    //   	return $this->attributes['username'] = \Crypt::decrypt($value); 
    // }
    
    public function setPasswordAttribute($value){
    	$this->attributes['password'] =  \Crypt::encrypt($value); 
    }

    // public function getPasswordAttribute($value){
    //   	return $this->attributes['password'] = \Crypt::decrypt($value); 
    // }

    public function setProfileUrlAttribute($value){
        $this->attributes['profile_url'] =  \Crypt::encrypt($value); 
    }
}
