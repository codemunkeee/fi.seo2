<?php

namespace App\Models\SEO;

use Illuminate\Database\Eloquent\Model;

class HostCompany extends Model
{
    protected $table = 'host_company';

    protected $fillable = [
      'name'
      ];

    // public function hosts(){
    //   return $this->belongsToMany('\App\Models\SEO\Hosts', 'user_hosts', 'host_company_id', 'host_id')->withTimestamps();
    // }
    

 /*------------------------
  MUTATORS AND ACCESSORS 
  ------------------------*/

    public function setNameAttribute($value){
      $name = ucfirst(trim($value));

      $this->attributes['name'] = $name; 
    }

}
