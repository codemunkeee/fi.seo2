<?php

namespace App\Models\SEO;

use Illuminate\Database\Eloquent\Model;

class Host extends Model
{
    protected $table = 'hosts';

    protected $fillable = [
      'friendly_name', 
      'hosting_cost', 
      'login_url', 
      'username', 
      'password'
      ];
    
    protected $appends = ['host_name'];
    
    protected $encrypted_fields = [
      'login_url', 
      'username', 
      'password'
    ];

    public function users(){
      return $this->belongsToMany('\App\Models\User', 'user_hosts', 'host_id', 'user_id')->withTimestamps();
    }
 
    // public function urls(){
    //   return $this->belongsToMany('\App\Models\SEO\Url')->withTimestamps();
    // }

    /**
     * Checks to see if a field with encryption was changed.
     * Important Notes: Used only when the field is encrypted!
     * @param  [type]  $field : the database column/field
     * @return boolean        [description]
     */
    public function hasFieldEncryptionChanged($value){
        $changed = false;
        foreach ($this->encrypted_fields as $encrypted) {
          $decrypted_field = \Crypt::decrypt($this->attributes[$encrypted]);
          $changed = ($decrypted_field == $value[$encrypted]) ? false : true;
          if ($changed) break ;
        }
        return $changed;
    }


    public function host_company(){
      return $this->belongsToMany('\App\Models\SEO\HostCompany', 'user_hosts', 'host_id', 'host_company_id')->withTimestamps()->first();
    }

  /*------------------------
  MUTATORS AND ACCESSORS 
  ------------------------*/

    public function getHostNameAttribute($value){
      return $this->host_company()->name;
    }

    public function setPasswordAttribute($value){
     $this->attributes['password'] = \Crypt::encrypt($value); 
    }

    public function getPasswordAttribute($value){
      return $this->attributes['password'] = \Crypt::decrypt($value); 
    }

    public function setLoginUrlAttribute($value){
     $this->attributes['login_url'] = \Crypt::encrypt($value); 
    }

    public function getLoginUrlAttribute($value){
      return $this->attributes['login_url'] = \Crypt::decrypt($value);
    }

    public function setUsernameAttribute($value){
     $this->attributes['username'] = \Crypt::encrypt($value); 
    }

    public function getUsernameAttribute($value){
      return $this->attributes['username'] = \Crypt::decrypt($value); 
    }

    public function setFriendlyNameAttribute($value){
      $friendly_name = ucfirst(trim($value));

      $this->attributes['friendly_name'] = $friendly_name; 
    }

    public function getFriendlyNameAttribute($value){
      return $this->attributes['friendly_name'] = ucfirst($value); 
    }

    public function getHostingCostAttribute($value){
      return $this->attributes['hosting_cost'] = round($value, 2); 
    }
}
