<?php

namespace App\Models\SEO;

use DB;
use Illuminate\Database\Eloquent\Model;

class SocialNetwork extends Model
{
    protected $table    = "social_networks";

    protected $fillable = ['name'];

    //return the profiles associated with this social_network
    public function profiles(){
    	return $this->belongsToMany('\App\Models\SEO\Profile', 'profile_social_accounts', 'social_network_id', 'profile_id')->withTimestamps();
    }

    //return the profiles associated with this social_network and this user
    public function userProfiles(){
        $user_profiles = DB::table('profile_social_accounts AS A')
            ->select('C.*')
            ->join('user_profiles AS B', 'B.profile_id', '=', 'A.profile_id')
            ->join('profiles AS C', 'C.id', '=', 'A.profile_id')
            ->where('A.social_network_id', '=', $this->id)
            ->where('B.user_id', '=', \Auth::user()->id);
        return $user_profiles;
    }   

    public function socialAccounts(){
        $social_accounts = DB::table('profile_social_accounts AS A')
            ->select('A.*')
            ->join('user_profiles AS B', 'B.profile_id', '=', 'A.profile_id')
            ->where('A.social_network_id', '=', $this->id)
            ->where('B.user_id', '=', \Auth::user()->id);
        return $social_accounts;
    }

    public function socialAccountsDecrypted(){
        $social_accounts = DB::table('profile_social_accounts AS A')
            ->select('A.*')
            ->join('user_profiles AS B', 'B.profile_id', '=', 'A.profile_id')
            ->where('A.social_network_id', '=', $this->id)
            ->where('B.user_id', '=', \Auth::user()->id)->get();

        for($i = 0; $i < count($social_accounts); $i++){
            $social_accounts[$i]->username       = (!isset($social_accounts[$i]->username) || trim($social_accounts[$i]->username)==='') ? '' :  \Crypt::decrypt($social_accounts[$i]->username);
            $social_accounts[$i]->password       = (!isset($social_accounts[$i]->password) || trim($social_accounts[$i]->password)==='') ? '' : \Crypt::decrypt($social_accounts[$i]->password);
            $social_accounts[$i]->profile_url    = (!isset($social_accounts[$i]->profile_url) || trim($social_accounts[$i]->profile_url)==='') ? '' : \Crypt::decrypt($social_accounts[$i]->profile_url);            
        }
        return $social_accounts;
    }    

    public function setNameAttribute($value){
        $patterns   = array("/\s+/", "/\s([?.!])/");
        $replacer   = array(" ","$1");
        $str        = preg_replace( $patterns, $replacer, $value );
    	$this->attributes['name'] =  ucfirst($str); 
    }

    // public function getNameAttribute($value){
    //   	return $this->attributes['name'] = \Crypt::decrypt($value); 
    // }

}
