<?php

namespace App\Models\SEO;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    protected $table = 'notes';

    protected $fillable = [
      'title',
      'notes',
      'parent_id',
      'user_id',
      'parent_type'      
    ];
    



  /*------------------------
  MUTATORS AND ACCESSORS 
  ------------------------*/

    public function setTitleAttribute($value){
      $this->attributes['title'] = ucfirst($value); 
    }

}
