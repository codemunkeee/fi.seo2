<?php

namespace App\Models;

use DB;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $dates = ['last_login'];


    /**
     * Get the user's confirmation status
     * 
     * @return HasOne ;returns the confirmation status of the user.
     */
    public function confirm(){
        return $this->hasOne('\App\Models\UserConfirmation', 'user_id');
    }

    /**
     * Get all roles from a particular user.
     * Uses role_user as a pivot table.
     * 
     * @return [type] belongsToMany [description] returns all roles that was assigned to a user.
     */
    public function roles()
    {
        //A user can have many roles.
        return $this->belongsToMany('\App\Models\Role', 'role_user', 'user_id', 'role_id')->withTimestamps();
    }

    /**
     * Checks if user has a role given by $role_name
     * @param  string  $role_name [description] the role to be checked. (E.g., Admin, User, Moderator, etc.)
     * @return boolean            [description] returns true if $role_name exists, otherwise false.
     */
    public function is($role_name)
    {
        foreach ($this->roles()->get() as $role)
        {
            if ($role->role == $role_name)
            {
                return true;
            }
        }

        return false;
    }

    public function owns($related){
        $related_user_id = $related->users($this->user)->first()->id;
        
        return (\Auth::user()->id == $related_user_id) ? true : false;        
    }

    public function ownsProfile($user){        
        return ($this->id == $user->id) ? true : false;
    }

    public function hasRole($role){
        if (is_string($role)){
            return $this->roles->contains('slug', $role);
        }

        return !! $role->intersect($this->roles)->count();

    }

    public function hosts(){
        return $this->belongsToMany('\App\Models\SEO\Host', 'user_hosts', 'user_id', 'host_id')->withTimestamps()->withPivot('host_company_id');
    }


    /**
     * Get the count of HOSTS on every HOST_COMPANY owned by the USER
     * 
     * @return Array 
    */    
    public function hostCount(){
        return DB::table('user_hosts')
            ->select('host_company.name AS label', DB::raw('COUNT("user_hosts.host_company_id") as value'))
            ->join('host_company', 'user_hosts.host_company_id', '=', 'host_company.id')
            ->where('user_hosts.user_id', '=', $this->id)
            ->groupBy('user_hosts.host_company_id');
    }

    /**
     * Get the count of SOCIAL_ACCOUNTS on every SOCIAL_NETWORK owned by the USER
     * 
     * @return Array 
    */
    public function socialAccountsCount(){
       return DB::table('user_social_networks as A')
            ->select('B.name AS label', DB::raw('COUNT("A.id") as value'))
            ->join('social_networks as B', 'A.social_network_id', '=', 'B.id')
            ->join('profile_social_accounts as C', 'C.social_network_id', '=', 'B.id')            
            ->where('A.user_id', '=', $this->id)
            ->groupBy('A.id');          
    }

    public function urls(){
        return $this->belongsToMany('\App\Models\SEO\Url', 'user_urls', 'user_id', 'url_id')->withTimestamps();
    }

    /**
     * ????
     * 
     * @return Array 
    */
    public function qb_urls(){
        return DB::table('user_urls')
            ->select("urls.*", "hosts.id as pivot.host.id")
            ->join('users', 'user_urls.user_id', '=', 'users.id')
            ->join('urls', 'user_urls.url_id', '=', 'urls.id')
            ->join('hosts', 'urls.host_id', '=', 'hosts.id')
            ->where('users.id', '=', $this->id)
            //->where('hosts.id', '=', 'urls.host_id')
            ->where('urls.ip', '!=', 'No IP Found')
            ->get();
    }

    /**
     * The count of IPs of all URLs associated with this USER.
     * 
     * @return Array
    */
    public function urlIpCount(){
        return DB::table('user_urls')
            ->select('urls.ip')
            ->join('urls', 'user_urls.url_id', '=', 'urls.id')
            ->where('user_urls.user_id', '=', $this->id)
            ->where('urls.ip', '<>', 'No IP Found')
            ->groupBy('urls.ip');
    }

    /**
     * Get the WHOIS account of this USER
     * 
     * @return Illuminate\Database\Eloquent\Relations\BelongsToMany 
    */
    public function whois()
    {
        return $this->belongsToMany('\App\Models\SEO\Whois', 'user_whois', 'user_id', 'whois_id')->withTimestamps();
    }

    /**
     * Get the PROFILES associated with this USER
     * 
     * @return Illuminate\Database\Eloquent\Relations\BelongsToMany 
    */
    public function profiles(){
        return $this->belongsToMany('\App\Models\SEO\Profile', 'user_profiles', 'user_id', 'profile_id')->withTimestamps();
    }

    /**
     * Get the SOCIAL_NETWORKS associated with ths USER
     * 
     * @return Illuminate\Database\Eloquent\Relations\BelongsToMany 
    */
    public function socialNetworks(){
        return $this->belongsToMany('\App\Models\SEO\SocialNetwork', 'user_social_networks', 'user_id', 'social_network_id')->withTimestamps();
    }

    /**
     * Get the SOCIAL_ACCOUNTS associated with ths USER
     * 
     * @return Illuminate\Database\Eloquent\Relations\BelongsToMany 
    */
    public function socialAccounts(){
        return DB::table('profile_social_accounts AS A')
            ->select('A.*')
            ->join('user_profiles AS B', 'A.profile_id', '=', 'B.profile_id')
            ->where('B.user_id', '=', $this->id);
    }

    /**
      * Get the decrypted SOCIAL_ACCOUNTS associated with ths USER
      * 
      * @return Illuminate\Database\Eloquent\Relations\BelongsToMany 
    */
    public function socialAccountsDecrypted(){
        $social_accounts = DB::table('profile_social_accounts AS A')
            ->select('A.*', 'C.name AS social_network', 'D.first_name', 'D.middle_name', 'D.last_name' )
            ->join('user_profiles AS B', 'B.profile_id', '=', 'A.profile_id')
            ->join('social_networks AS C', 'C.id', '=', 'A.social_network_id')
            ->join('profiles AS D', 'D.id', '=', 'A.profile_id')
            ->where('B.user_id', '=', \Auth::user()->id)->get();

        for($i = 0; $i < count($social_accounts); $i++){
            $social_accounts[$i]->username       = (!isset($social_accounts[$i]->username) || trim($social_accounts[$i]->username)==='') ? '' :  \Crypt::decrypt($social_accounts[$i]->username);
            $social_accounts[$i]->password       = (!isset($social_accounts[$i]->password) || trim($social_accounts[$i]->password)==='') ? '' : \Crypt::decrypt($social_accounts[$i]->password);
            $social_accounts[$i]->profile_url    = (!isset($social_accounts[$i]->profile_url) || trim($social_accounts[$i]->profile_url)==='') ? '' : \Crypt::decrypt($social_accounts[$i]->profile_url);            
        }
        return $social_accounts;
    }    

    /**
     * Get the WHOIS account of a USER.
     * If USER doesn't have a WHOIS account, use the main account.
     * 
     * @return App\Models\SEO\Whois
    */    
    public function getWhoisAttribute(){
        $whois = $this->whois()->first();
        
        return (count($whois)==0) ? \App\Models\SEO\Whois::where('main', 1)->first() : $whois;

    }

    /**
     * Check if USER has a WHOIS account
     * 
     * @return Boolean
    */
    public function getHasWhoisAttribute(){
        $whois = $this->whois()->first();
        return count($whois)>0 ? true : false;
    }
}
