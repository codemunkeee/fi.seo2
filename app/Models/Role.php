<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    
    protected $table="roles";
    protected $fillable=['name', 'slug', 'description'];

     /**
     * many-to-many relationship method.
     *
     * @return QueryBuilder
     */
    public function permissions()
    {
        return $this->belongsToMany('App\Models\Users\Permission', 'permission_role', 'role_id', 'permission_id');
    }

    public function givePermissionTo(Permission $permission){
    	return $this->permissions()->save($permission);
    } 	
    
}