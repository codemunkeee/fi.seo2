<?php

/*
* RequestParser v1.0
*
* A package to parse request names that has :
*
  chr(32) ( ) (space)
  chr(46) (.) (dot)
  chr(91) ([) (open square bracket)
    
*
* May 4, 2016 12:34 AM GMT + 8:00
* Joe Carlo M. Boto - Fire & Ice Tech
*
*/

namespace App\Packages\Helpers;


Class RequestParser
{

  public function __construct($request){
    $this->request = $request;
    $this->set();
  }

  private function set(){
    $this->request = $this->parse();
  } 

  public function get(){
    return $this->request;
  }

  public function parse(){

    $value = $this->request;
    $value = str_replace(' ', '_', $value);
    $value = str_replace('.', '_', $value);
    $value = str_replace('[', '_', $value);
    
    return $value;
  }
}