<?php

/*
* Image v1.0
*
* A package to upload images from a request
*   
*
v
* April 4, 2016 5:49 PM GMT + 8:00
* Joe Carlo M. Boto - Fire & Ice Tech
*
*/

namespace App\Packages\Images;

use Illuminate\Http\Request;
use App\Http\Requests;

use Carbon\Carbon;

Class Image
{

  protected $image;
  protected $images;
  protected $request;
  protected $destination_path;

  /*
  *  App\Http\Requests\Request $request : this is the primary request used to get images from the user;
  */
  public function __construct($request, $destination_path){
    $this->request = $request;
    $this->destination_path = $destination_path;
    $this->init();
  } 

  private function init(){
    (is_array($this->request)) ? $this->setMultiple() : $this->setSingle();
  }

  private function setSingle(){
    $this->image = is_string($this->request) ? $this->setImageFromURL() : $this->setImage();       
  }

  private function setMultiple(){
    $this->images = 'under construction';
  }

  private function setImageFromURL(){
    //$url = $this->request;
    //$url_arr = explode ('/', $url);
        // $ct = count($url_arr);
        // $name = $url_arr[$ct-1];
        // $name_div = explode('.', $name);
        // $ct_dot = count($name_div);
        // $img_type = $name_div[$ct_dot -1];

        // $destinationPath = public_path().'/img/'.$name;
        // file_put_contents($destinationPath, file_get_contents($url));
  }

  private function setImage(){
    $avatar             = $this->request;
    //$destination_path   = 'assets/img/users/'.$user->id.'/';        
    // $user->avatar     = 'http://'.$_SERVER['SERVER_NAME'].'/'.$destination_path .$avatar->getClientOriginalName();
    // $request->file('avatar')->move($destination_path, $avatar->getClientOriginalName());     
    return $avatar;
  }

  public function get(){
    return isset($this->image) ? $this->image : $this->images;
  }

}