<?php

/*
* SEO v1.0
*
* A package to parse json data from whoisxmlapi.com.
*
* Requires a URL from www.whoisxmlapi.com
*   that returns a JSON format.
*   
*
v
* March 27, 2016 4:18 AM GMT + 8:00
* Joe Carlo M. Boto - Fire & Ice Tech
*
*/
namespace App\Packages\SEO;

use App\Packages\SEO\FIScraper;

use Carbon\Carbon;

Class SEO extends FIScraper
{

  public $JSON;

  protected $url;

  protected $url_raw;

  public function __construct($params, $test_mode = false){
    $this->setURL($params, $test_mode);
    $this->setRaw();
    $this->setJSON();
    $this->setWhois();
    $this->initialize();
    
  } 

  public function setURL($params, $test_mode){
    // whoisxmlapi.com/whoisserver/WhoisService?domainName=google.com&username=yourUserNmae&password=yourPassword&outputFormat=JSON
    
    if ($test_mode) {
      $this->url = 'L:/Work/sharp.json';
      //$this->url = 'http://carlofireice.esy.es/beststungun.json';
      //$this->url = 'http://carlofireice.esy.es/sharp.json';
      // $this->url = "https://www.whoisxmlapi.com/whoisserver/WhoisService?domainName=facebook.com&username=Fireicetech One&password=Password123!&outputFormat=JSON";
      //$this->url = "https://www.whoisxmlapi.com/whoisserver/WhoisService?domainName=facebook.com&username=dev.jcmboto&password=facedown21!&outputFormat=JSON";
      $this->setURLRaw($this->url);
    } else {
      $webapi       = "https://www.whoisxmlapi.com/whoisserver/WhoisService?";
      $domain_name  = urlencode($params['domain_name']);
      $username     = urlencode($params['username']);
      $password     = urlencode($params['password']);
      $outputFormat = 'JSON';
      
      $this->setURLRaw($domain_name);
      
      $this->url    = $webapi . 'domainName=' . $domain_name . '&username=' . $username . '&password=' . $password . '&outputFormat=' . $outputFormat; 
      
    }  
  }

  public function initialize(){
    if (isset($this->whois)){
      $this->setRegistryData();
      $this->setRegistrant();
      $this->setNameServers();
    } else {
      $this->setErrorMessage();
    }
  }
  public function getURL(){
    return $this->url;
  }

  public function setURLRaw($domain_name){
    $this->url_raw = $domain_name;
    //$parse          = //parse_url($domain_name);
    //$this->url_raw  = $parse['host'];    
  }

  public function getURLRaw(){
    return $this->url_raw;
  }

  public function setRaw(){
    //add exception error here
  
    $this->raw = file_get_contents($this->getURL());
  }

  public function getRaw(){
    return $this->raw;
  }

  public function setJSON(){
    $this->JSON =  json_decode($this->getRaw());
  }

  public function getJSON(){
    return $this->JSON;
  }

  public function setWhois(){
    $this->whois = isset($this->JSON->WhoisRecord) ? $this->JSON->WhoisRecord : null;
  }

  public function getWhois(){
    return $this->whois;
  }

  public function setErrorMessage(){
    $this->error_message = isset($this->whois) ? '' : $this->JSON->ErrorMessage->msg; 
  }

  public function getErrorMessage(){
    return $this->error_message;
  }

  public function setRegistryData(){
    $this->registry_data = isset($this->whois->registryData) ? $this->whois->registryData : null;

  }

  public function getRegistryData(){
    return $this->registry_data;
  }

  public function setRegistrant(){
    if (isset($this->whois->registrant)){
      $this->registrant = $this->whois->registrant;
    } else {
      $this->registrant = isset($this->registry_data->registrant) ? $this->registry_data->registrant : null; 
    }
  }

  public function getRegistrant(){
    return $this->registrant;
  }

  public function setNameServers(){
    if (isset($this->whois->nameServers->hostNames)){
      $this->name_servers = $this->whois->nameServers->hostNames;
    } else {
      $this->name_servers = isset($this->registry_data->nameServers->hostNames) ? $this->registry_data->nameServers->hostNames : null;
    }
  }

  public function getNameServers(){
    return $this->name_servers;
  }
  /* 
    ----------- HELPERS ---------------
  */

    public function getIP($host){
      $host = trim($host.'.'); // clean and add root .
      $ip   = gethostbyname($host);
      $ip   = ($ip == $host) ?  'No IP Found' : $ip ;// empty IP if there is no ip
      return $ip;
    }

  public function getRegistrar(){
    $registrar =  (isset($this->registry_data->registrarName)) ? $this->registry_data->registrarName : null;
    return $registrar;
  }

  /* 
    Used Laravel's mutator and accessors for dates
  */
  public function getUpdateDate(){
    $update_date    = (isset($this->registry_data->updatedDateNormalized)) ? $this->registry_data->updatedDateNormalized : null;

      return $update_date;      
  }

  /* 
    Used Laravel's mutator and accessors for dates
  */

  public function getCreateDate(){
    $create_date    = (isset($this->registry_data->createdDateNormalized)) ? $this->registry_data->createdDateNormalized : null;

      return $create_date;      
  }

  public function getRegistrantName(){
    $registrant_name = (isset($this->registrant->name)) ? $this->registrant->name : null;
    return $registrant_name;
  }

  public function getRegistrantOrganization(){
    $registrant_organization = (isset($this->registrant->organization)) ? $this->registrant->organization : null;
    return $registrant_organization;
  }

  public function getRegistrantStreet(){
    $registrant_street = (isset($this->registrant->street1)) ? $this->registrant->street1 : null;
    return $registrant_street;
  }

  public function getRegistrantCity(){
    $registrant_city = (isset($this->registrant->city)) ? $this->registrant->city : null;
    return $registrant_city;    
  }

  public function getRegistrantState(){
    $registrant_state = (isset($this->registrant->state)) ? $this->registrant->state : null;
    return $registrant_state;    
  }

  public function getRegistrantZip(){
    $registrant_zip = (isset($this->registrant->postalCode)) ? $this->registrant->postalCode : null;
    return $registrant_zip;    
  }

  public function getRegistrantPhone(){
    $registrant_phone = (isset($this->registrant->telephone)) ? $this->registrant->telephone : null;
    return $registrant_phone;    
  }

  public function getRegistrantEmail(){
    $registrant_email = (isset($this->registrant->email)) ? $this->registrant->email : null;
    return $registrant_email;    
  }  

}