angular.module('FI', [
	'datatables',  
	'datatables.buttons', 
	'ui.bootstrap', 
	'ngclipboard', 
	'ngSanitize', 
	'toaster', 
	'ngAnimate', 
	'summernote',
	'ngFileUpload'
	]);

angular.module('FI').
  filter('htmlToPlaintext', function() {
    return function(text) {
      return  text ? String(text).replace(/<[^>]+>/gm, '') : '';
    };
  }
);
  