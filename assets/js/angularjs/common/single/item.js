angular.module('FI').controller('MainCtrl', function($scope , $http, $filter, $timeout, DTOptionsBuilder, toaster,  Upload, CSRF_TOKEN){ 
    var url = window.location.href + '/get';
    $scope.pivot    = { data : {}, method : {}};
    

    $scope.initialize = function(){
        $scope.getItem();
    };

    $scope.test = function(){
        console.log($scope.pivot);
    }
    //get all items
    $scope.getItem = function(){
        $http.get(url).then(function (response) { 
            console.log(response.data);
            $scope.item_details = response.data;
            //$scope.initializePivots();
        }, function(response){
        }); 
    }; 

    $scope.initialize(); 

    //PIVOT  METHODS
    // pivot.method.name = method property, here we define all the methods for all pivot
    // pivot.data[pivot_name].items      = the storage for pivot items
    // pivot.data[pivot_name].selected   = the selected pivot item
    // pivot.data[pivot_name].urls       = the urls needed for the api to get pivot data
    // pivot.data[pivot_name].parent_id  = appended on the base url, the parent model's id (e.g., /hosts/{id})

    // initialize all pivots
    $scope.initializePivots = function(item_id){
        angular.forEach($scope.pivot_api, function(value, key) {
            if (key != 'parent_name') {
                $scope.pivot.method.init(value, item_id, parent_name, key);                
            } else {
                parent_name = value;
            }
        });   
    }

    /**
     * Initialize pivot items
     * @param  Request $request : User's request
     * @param  [type]  $id      : PROFILE Id
     * @return [type]           [description]
     */
    $scope.pivot.method.init = function(urls, item_id, parent_type, pivot_type){
        $scope.pivot.data[pivot_type]                   = {};
        $scope.pivot.data[pivot_type].urls              = {};
        $scope.pivot.data[pivot_type].items             = {};
        $scope.pivot.data[pivot_type].parent_id         = item_id;
        $scope.pivot.data[pivot_type].parent_type       = parent_type;        
        $scope.pivot.data[pivot_type].pivot_type        = pivot_type;        
        $scope.pivot.data[pivot_type]._token            = CSRF_TOKEN;
        $scope.pivot.data[pivot_type].user_id           = $scope.user_id;
        $scope.pivot.data[pivot_type].selected          = {};

        // initialize multiple selections
        $scope.pivot.data[pivot_type].multiple                  = {};
        $scope.pivot.data[pivot_type].multiple.selected         = {};
        $scope.pivot.data[pivot_type].multiple.unselected       = {};
        // $scope.pivot.data[pivot_type].multiple.selected.data    = {};
        // $scope.pivot.data[pivot_type].multiple.unselected.data  = {};        

        //initialize new item properties
        $scope.pivot.data[pivot_type].new_item                = {};
        $scope.pivot.data[pivot_type].new_item._token         = $scope.pivot.data[pivot_type]._token;
        $scope.pivot.data[pivot_type].new_item.user_id        = $scope.pivot.data[pivot_type].user_id;
        $scope.pivot.data[pivot_type].new_item.parent_id      = $scope.pivot.data[pivot_type].parent_id;
        $scope.pivot.data[pivot_type].new_item.parent_type    = $scope.pivot.data[pivot_type].parent_type;


        $scope.pivot.method.setUrls(urls, pivot_type);
    }

    //check if item is selected
    $scope.pivot.method.isSelected = function(pivot_name, item){
        if ($scope.pivot.data[pivot_name].selected){
            if ($scope.pivot.data[pivot_name].selected.id === item.id){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    };

    // set the selected item for a pivot
    $scope.pivot.method.selected = function(pivot_name, pivot_item){
        $scope.pivot.data[pivot_name].selected = $filter('filter')($scope.pivot.data[pivot_name].items, function (d) {return d.id === pivot_item.id;})[0];
    }

    $scope.pivot.method.get = function(pivot_name, url){
        url = $scope.replaceUrlStringId(url, $scope.pivot.data[pivot_name].parent_id);
        $http.get(window.location.origin + url).then(function (response){
            $scope.pivot.data[pivot_name].items = response.data;
        }, function(response){
            $scope.showErrors(response);
        });      
    }

    // url must start with "/"
    $scope.pivot.method.post = function(){
        title = pivot_friendly_name.charAt(0).toUpperCase() + pivot_friendly_name.slice(1);
        $scope.pivot.method.init(pivot_name);

        // $http.post(window.location.origin + url, $scope.pivot[pivot_name]).then(function (response){
        //     console.log(response.data);
        //     $scope.showToast('success', title + 'Added!', title  + ' was successfully created.'); 
        // }, function(response){
        //     $scope.showErrors(response);
        // });
        console.log($scope.pivot);
    }

    // store a single pivot item
    $scope.pivot.method.store = function(pivot_table_name){
        url = $scope.pivot.data[pivot_table_name].urls.store;
       
        $http.post(window.location.origin + url, $scope.pivot.data[pivot_table_name].new_item).then(function (response){
            $scope.pivot.method.getAllPivot(pivot_table_name);
            //$scope.showToast('success', title + 'Added!', title  + ' was successfully created.'); 
        }, function(response){
            $scope.showErrors(response);
        });
    }

    // store a single pivot item
    $scope.pivot.method.update = function(pivot_table_name, pivot_item){
        url = $scope.pivot.data[pivot_table_name].urls.update;
        url = $scope.replaceUrlStringId(url, $scope.pivot.data[pivot_table_name].parent_id);
    
        $http.put(window.location.origin + url, pivot_item).then(function (response){
            $scope.pivot.method.getAllPivot(pivot_table_name);
            $scope.showToast('success',  'Item Updated!', 'Item was successfully updated.'); 
        }, function(response){
            $scope.showErrors(response);
        });
    }

    // delete a single pivot item
    $scope.pivot.method.delete = function(pivot_table_name, pivot_item){
        url = $scope.pivot.data[pivot_table_name].urls.delete;
        url = $scope.replaceUrlStringId(url, pivot_item.id);

        $http.delete(window.location.origin + url).then(function (response){
            $scope.pivot.method.getAllPivot(pivot_table_name);
            $scope.showToast('success',  'Item Deleted!', 'Item was deleted successfully.'); 
        }, function(response){
            $scope.showErrors(response);
        });
    }

    // link multiple pivot items to the parent
    $scope.pivot.method.linkMultiple = function(pivot_table_name, pivot_item){
        url = $scope.pivot.data[pivot_table_name].urls.link_multiple;
        url = $scope.replaceUrlStringId(url, $scope.pivot.data[pivot_table_name].parent_id);
        // url = $scope.pivot.data[pivot_table_name].urls.unlink + pivot_table_name + '/' + $scope.pivot.data[pivot_table_name].selected.id;
        // $scope.pivot.data[pivot_table_name].multiple.selected._token = $scope.pivot.data[pivot_table_name]._token;
        // $scope.pivot.data[pivot_table_name].multiple.selected.user_id = $scope.pivot.data[pivot_table_name].user_id;                

        $http.post(window.location.origin + url, pivot_item).then(function (response){
            //intialize again
            $scope.pivot.method.getAllPivot(pivot_table_name);
            console.log(response);
            $scope.showToast('success', 'Items linked!', 'Items were successfully created.'); 
        }, function(response){
            $scope.showErrors(response);
         });
    }


    // unlink a pivot item from the parent
    $scope.pivot.method.unlink = function(pivot_table_name){
        url = $scope.pivot.data[pivot_table_name].urls.unlink + pivot_table_name + '/' + $scope.pivot.data[pivot_table_name].selected.id;
        $scope.pivot.data[pivot_table_name].selected._token = $scope.pivot.data[pivot_table_name]._token;
        $scope.pivot.data[pivot_table_name].selected.user_id = $scope.pivot.data[pivot_table_name].user_id;                

        $http.post(window.location.origin + url, $scope.pivot.data[pivot_table_name].selected).then(function (response){
            //intialize again
            $scope.pivot.method.getAllPivot(pivot_table_name);

            //$scope.showToast('success', title + 'Added!', title  + ' was successfully created.'); 
        }, function(response){
            $scope.showErrors(response);
        });
    }


    // unlink multiple pivot items from the parent
    $scope.pivot.method.multipleUnlink = function(pivot_table_name){
        url = $scope.pivot.data[pivot_table_name].urls.unlink + pivot_table_name + '/' + $scope.pivot.data[pivot_table_name].selected.id;
        $scope.pivot.data[pivot_table_name].selected._token = $scope.pivot.data[pivot_table_name]._token;
        $scope.pivot.data[pivot_table_name].selected.user_id = $scope.pivot.data[pivot_table_name].user_id;                

        $http.post(window.location.origin + url, $scope.pivot.data[pivot_table_name].selected).then(function (response){
            //intialize again
            $scope.pivot.method.getAllPivot(pivot_table_name);

            //$scope.showToast('success', title + 'Added!', title  + ' was successfully created.'); 
        }, function(response){
            $scope.showErrors(response);
        });
    }

   
    // get the unselected list of pivot items, for select2 boxes only
    // pivot_name = the name of the pivot to be used
    $scope.pivot.method.unselectedList = function(pivot_name, url){
        url = $scope.replaceUrlStringId(url, $scope.pivot.data[pivot_name].parent_id);
        $scope.pivot.data[pivot_name].multiple.selected = {};
        $scope.pivot.data[pivot_name].multiple.unselected = {};

        // must refactor this
        // used to reset value
        $timeout(function() {$("#" + pivot_name + "_select").select2("val", "");}, 0);
        
            
        $http.get(window.location.origin + url).then(function (response){
            $scope.pivot.data[pivot_name].multiple.unselected = response.data;
            //$scope.showToast('success', title + 'Added!', title  + ' was successfully created.'); 
        }, function(response){
            $scope.showErrors(response);
        });
    }

    // 
    $scope.pivot.method.setUrls = function(urls, pivot_name){
        angular.forEach(urls, function(value, key) {
            $scope.pivot.data[pivot_name].urls[key] = value;
        });

        //get pivot data
        
        $scope.pivot.method.getAllPivot(pivot_name);
    }

    $scope.pivot.method.getAllPivot = function(pivot_name){
        angular.forEach($scope.pivot.data[pivot_name].urls, function(value, key) {
            switch (key){
                case 'get' :
                    $scope.pivot.method.get(pivot_name, value);
                    break;
                case 'unselected_list' :
                    $scope.pivot.method.unselectedList(pivot_name, value);
                    break;                    
                default :
                    break;
            }
        });        
    } 

   //HELPERS
    
    //convert item string to date
    $scope.convertItemDateFromString = function(date_string){
        return new Date(date_string);
    };

    //convert item string to date
    $scope.convertStringToInt = function(string){
        return parseInt(string);
    };    

    // replace a given url string to the original id
    // example : string = profiles/{id}/social_accounts
    // output : string = profiles/1/social_accounts
    $scope.replaceUrlStringId = function(string, id){
        return string.replace("{id}", id);
    }

});

