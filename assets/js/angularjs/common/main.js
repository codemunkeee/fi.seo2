angular.module('FI', [
	'datatables',  
	'datatables.buttons', 
	'ui.bootstrap', 
	'ngclipboard', 
	'ngSanitize', 
	'toaster', 
	'ngAnimate', 
	'oc.lazyLoad',
	'angular.morris-chart',
	'summernote',
	'ngFileUpload'
	]);

angular.module('FI').
  filter('htmlToPlaintext', function() {
    return function(text) {
      return  text ? String(text).replace(/<[^>]+>/gm, '') : '';
    };
  }
);
  

// angular.module('FI').service('sharedProperties', function () {
//     var item_details = {};

//     return {
//         getProperty: function () {
//             return item_details;
//         },
//         setProperty: function(value) {
//             item_details = value;
//         }
//     };
// });


/*global angular */
// (function (ng) {
//   'use strict';

//   var app = ng.module('ngLoadScript', []);

//   app.directive('script', function() {
//     return {
//       restrict: 'E',
//       scope: false,
//       link: function(scope, elem, attr) {
//         if (attr.type === 'text/javascript-lazy') {
//           var code = elem.text();
//           var f = new Function(code);
//           f();
//         }
//       }
//     };
//   });

// }(angular));

// angular.module('FI').config(function(uiSelectConfig) {
//   uiSelectConfig.theme = 'select2';
// });

// angular.module('FI').directive("select2", function($timeout, $parse) {
//   return {
//     restrict: 'AC',
//     require: 'ngModel',
//     link: function(scope, element, attrs) {
//       console.log(attrs);
//       $timeout(function() {
//         element.select2();
//         element.select2Initialized = true;
//       });

//       var refreshSelect = function() {
//         if (!element.select2Initialized) return;
//         $timeout(function() {
//           element.trigger('change');
//         });
//       };
      
//       var recreateSelect = function () {
//         if (!element.select2Initialized) return;
//         $timeout(function() {
//           element.select2('destroy');
//           element.select2();
//         });
//       };

//       scope.$watch(attrs.ngModel, refreshSelect);

//       if (attrs.ngOptions) {
//         var list = attrs.ngOptions.match(/ in ([^ ]*)/)[1];
//         // watch for option list change
//         scope.$watch(list, recreateSelect);
//       }

//       if (attrs.ngDisabled) {
//         scope.$watch(attrs.ngDisabled, refreshSelect);
//       }
//     }
//   };
// });