angular.module('FI').controller('MainCtrl', function($scope , $http, $filter, $timeout, DTOptionsBuilder, toaster, $uibModal, CSRF_TOKEN){ 

    //CONFIGS
    

    //DATATABLES
    
    $scope.datatable = {};
    
    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'File'}
        ])
        .withOption('scrollY', '48vh');

    $scope.dtOptionsAll = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'File'},
            {extend: 'pdf', title: 'File'},
            {extend: 'print',
                customize: function (win){
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table')
                        .addClass('compact')
                        .css('font-size', 'inherit');
                }
            }
        ])
        .withOption('scrollY', '48vh');

    $scope.dtOptionsOnClickRightSide = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withOption('lengthMenu', [3, 10, 50, 100])
        .withOption('scrollY', '48vh');

    $scope.dtOptionsOnClickLeftSide = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withOption('scrollY', '48vh');        
});
