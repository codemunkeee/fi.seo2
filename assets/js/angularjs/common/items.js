angular.module('FI').controller('MainCtrl', function($scope , $http, $filter, $timeout, DTOptionsBuilder, toaster, $uibModal, Upload,  CSRF_TOKEN){ 
    var url = window.location.href + '/api/filter';
    $scope.is_loading = false;
    $scope.payment_types = [{type:'Monthly'},{type:'Annually'}];
    $scope.new_item = {};
    $scope.chart = {};
    // $scope.new_note = {};
    // $scope.new_url  = {};
    $scope.pivot    = { data : {}, method : {}};
    var dt_options_counter = 1;

    $scope.initialize = function(){
        $scope.is_loading = true;
        $scope.new_item.payment_type = $scope.payment_types[0];
        $scope.getItems();
        //$scope.test();

    };

    $scope.test = function(){


        console.log($scope.pivot.data);
        // $http.get(window.location.origin + '/profiles/54/social_accounts').then(function (response){
        //     //console.log(response.data);
        //     //$scope.pivot.data[pivot_name].items = response.data;
        // }, function(response){
        //     $scope.showErrors(response);
        // });

        // $http.get(window.location.origin + '/profiles/54/social_accounts/unselected_list').then(function (response){
        //     console.log(response.data);
        //     //$scope.pivot.data[pivot_name].items = response.data;
        // }, function(response){
        //     $scope.showErrors(response);
        // });

    }
    //get all items
    $scope.getItems = function(){
        $http.get(url).then(function (response) { 
            $scope.items_full   = response.data; 
            $scope.items        = response.data.data;
            //$scope.chart = $scope.morrisChart;
            setTimeout(function() {
              $scope.$apply(function (){
                $scope.chart = $scope.morrisChart;
              });
            });

            //$scope.selectFirstitemData();
            $scope.is_loading = false;
        }, function(response){
            $scope.is_loading = false;
            //$scope.showErrors(response);
        }); 
    };

    //get first item in the array
    $scope.selectFirstitemData = function(){
        if ($scope.items.length>0) {
            $scope.item_details     = $scope.items[0];  
        }
    };

    //when an item is clicked
    $scope.clickedItemData = function(item_id){
        $scope.setItemData(item_id);
        $scope.initializePivots(item_id);
        //$scope.getDatatableOptions();
    };

    //set selected item
    $scope.setItemData = function(item_id) {
        $scope.item_details = $filter('filter')($scope.items, function (d) {return d.id === item_id;})[0];
    };

    //unset selected item
    $scope.unsetItemData = function(){
        $scope.item_details = null;
    };

    //check if item is selected
    $scope.isItemSelected = function(item){
        if ($scope.item_details){
            if ($scope.item_details.id === item.id){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    };

    $scope.isItemSet = function(){
        if ($scope.item_details){
            return true;
        } else {
            return false;
        }
    }
    $scope.setPaymentType = function(selected){
        angular.forEach($scope.payment_types,function(value,index){ 
            if (value === selected) {
                $scope.new_item.payment_type = value;
            }
        });
    };

    $scope.submit = function(){
        $scope.new_item._token  = CSRF_TOKEN;
        $scope.new_item.user_id = $scope.user_id;
        
        $http.post(window.location.origin + '/' + $scope.pivot_api.parent_name, $scope.new_item).then(function (response){
            $scope.initialize();
            $scope.new_item = {};
            $scope.showToast('success', 'Item added!', 'The item was successfully created.'); 
        }, function(response){
            $scope.showErrors(response);
        });
    }

    $scope.submitWithFile = function(file){
        $scope.new_item._token = CSRF_TOKEN;
        $scope.new_item.user_id = $scope.user_id;

        Upload.upload({
            url: window.location.origin + '/' + $scope.pivot_api.parent_name,
            data: $scope.new_item
        }).then(function (response) {
            $scope.initialize();
            $scope.new_item = {};
            $scope.showToast('success', 'Item added!', 'The item was successfully created.'); 
        }, function (response) {
            $scope.showErrors(response);
        }, function (evt) {
            file.progress = parseInt(100.0 * evt.loaded / evt.total);
        });
    }

    $scope.update = function(){
        $scope.item_details._token = CSRF_TOKEN;
        $scope.item_details.user_id = $scope.user_id;

        $http.put(window.location.origin + '/' + $scope.pivot_api.parent_name + '/' + $scope.item_details.id, $scope.item_details).then(function (response){
            console.log(response.data);
            //$scope.initialize();
            $scope.showToast('success', 'Item updated!', 'The item was successfully updated.'); 
        }, function(response){
            $scope.showErrors(response);
        });
    }
    
    $scope.delete = function(){
        // $scope.item_details._token = CSRF_TOKEN;
        // $scope.item_details.user_id = $scope.user_id;

        $http.delete(window.location.origin + '/' + $scope.pivot_api.parent_name + '/' + $scope.item_details.id).then(function (response){
            //console.log(response.data);
            $scope.items.splice($scope.items.indexOf($scope.item_details), 1);
            $scope.showToast('success', 'Item updated!', 'The item was successfully updated.'); 
        }, function(response){
            $scope.showErrors(response);
        });
    }
    
    $scope.link = function(item){
        console.log(item);
    }

    // type of the item returned
    $scope.itemType = function (type){
        var item = {};
        switch (type){
            case 'create' :
                item = $scope.new_item_details;
                break;
            case 'edit' || 'delete':
                item = $scope.item_details;
                break;
        }
        return item;
    };


    //PLUGINS
    
    $scope.getDatatableOptions = function(){
        if (dt_options_counter > 0) {
            $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDOM('<"html5buttons"B>lTfgitp')
            .withButtons([])            
            .withOption('scrollY', '48vh')
            .withOption('retrieve', true);    

        }
        dt_options_counter = 0;

    }

    // DATATABLES
    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'File'}
        ])
        .withOption('scrollY', '48vh')
        .withOption('retrieve', true);

    $scope.dtOptionsAll = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'File'},
            {extend: 'pdf', title: 'File'},
            {extend: 'print',
                customize: function (win){
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table')
                        .addClass('compact')
                        .css('font-size', 'inherit');
                }
            }
        ])
        .withOption('retrieve', true)
        .withOption('scrollY', '48vh');

    $scope.dtOptionsOnClickRightSide = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([])
        .withOption('lengthChange', false)
        .withOption('searching', false)
        .withOption('lengthMenu', [3, 10, 50, 100])
        .withOption('retrieve', true);

   $scope.dtOptionsOnClickRightSide10 = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([])
        .withOption('lengthChange', false)
        .withOption('searching', false)
        .withOption('lengthMenu', [10, 50, 100])
        .withOption('retrieve', true);

        //.withOption('scrollY', '48vh');


    
    $scope.searchTable = function(search_text){
        $scope.table_instance.DataTable.search(search_text);
        $scope.table_instance.DataTable.search(search_text).draw();
    }

    $scope.dtInstanceCallback = function(dt_instance)
    {
        var datatable_obj = dt_instance;
        $scope.table_instance = datatable_obj;
    }

    // CLIPBOARD
    $scope.clipboardSuccess = function(e){
        $scope.showToast('success', 'Copied!', 'Items are added to the clipboard.')
    };

    $scope.clipboardError = function(e){
        $scope.showErrors(response);
    };


    // TOASTER
    $scope.showToast = function(type, title, message ){
         toaster.pop({
            type: type,
            title: title,
            body: message,
            showCloseButton: true,
            timeout: 4000,
            showEasing: 'swing',
            hideEasing: 'linear',
            showDuration: '400',
            hideDuration: 400,
            extendedTimeOut: 1000,
            showMethod: 'fadeIn',
            hideMethod: 'fadeOut',
            bodyOutputType : 'trustedHtml'
        }); 
    }


    //ERRORS
    
     $scope.showErrors = function(response){
        switch (response.status){
            case 422:
                $scope.error422(response);        
                break;
            default :
                $scope.showToast('error', 'ERROR ' + response.status + ' - Something went wrong!', 'Something went wrong while sending the data to our server.');
                break;
        }

    };

    // shown when server throws 422 exception
    $scope.error422 = function(response){
        var error_text = '';
        angular.forEach(response.data, function(value, key) {
          error_text += '<br>' + value[0];            
        });
        $scope.showToast('error',  'ERROR ' + response.status + ' - Something went wrong!', error_text);
    };

    //HELPERS
    
    //convert item string to date
    $scope.convertItemDateFromString = function(date_string){
        return new Date(date_string);
    };

    //convert item string to date
    $scope.convertStringToInt = function(string){
        return parseInt(string);
    };    

    // replace a given url string to the original id
    // example : string = profiles/{id}/social_accounts
    // output : string = profiles/1/social_accounts
    $scope.replaceUrlStringId = function(string, id){
        return string.replace("{id}", id);
    }

    $scope.initialize();


    //PIVOT  METHODS
    // pivot.method.name = method property, here we define all the methods for all pivot
    // pivot.data[pivot_name].items      = the storage for pivot items
    // pivot.data[pivot_name].selected   = the selected pivot item
    // pivot.data[pivot_name].urls       = the urls needed for the api to get pivot data
    // pivot.data[pivot_name].parent_id  = appended on the base url, the parent model's id (e.g., /hosts/{id})

    // initialize all pivots
    $scope.initializePivots = function(item_id){
        angular.forEach($scope.pivot_api, function(value, key) {
            if (key != 'parent_name') {
                $scope.pivot.method.init(value, item_id, parent_name, key);                
            } else {
                parent_name = value;
            }
        });   
    }

    /**
     * Initialize pivot items
     * @param  Request $request : User's request
     * @param  [type]  $id      : PROFILE Id
     * @return [type]           [description]
     */
    $scope.pivot.method.init = function(urls, item_id, parent_type, pivot_type){
        $scope.pivot.data[pivot_type]                   = {};
        $scope.pivot.data[pivot_type].urls              = {};
        $scope.pivot.data[pivot_type].items             = {};
        $scope.pivot.data[pivot_type].parent_id         = item_id;
        $scope.pivot.data[pivot_type].parent_type       = parent_type;        
        $scope.pivot.data[pivot_type].pivot_type        = pivot_type;        
        $scope.pivot.data[pivot_type]._token            = CSRF_TOKEN;
        $scope.pivot.data[pivot_type].user_id           = $scope.user_id;
        $scope.pivot.data[pivot_type].selected          = {};

        // initialize multiple selections
        $scope.pivot.data[pivot_type].multiple                  = {};
        $scope.pivot.data[pivot_type].multiple.selected         = {};
        $scope.pivot.data[pivot_type].multiple.unselected       = {};
        // $scope.pivot.data[pivot_type].multiple.selected.data    = {};
        // $scope.pivot.data[pivot_type].multiple.unselected.data  = {};        

        //initialize new item properties
        $scope.pivot.data[pivot_type].new_item                = {};
        $scope.pivot.data[pivot_type].new_item._token         = $scope.pivot.data[pivot_type]._token;
        $scope.pivot.data[pivot_type].new_item.user_id        = $scope.pivot.data[pivot_type].user_id;
        $scope.pivot.data[pivot_type].new_item.parent_id      = $scope.pivot.data[pivot_type].parent_id;
        $scope.pivot.data[pivot_type].new_item.parent_type    = $scope.pivot.data[pivot_type].parent_type;


        $scope.pivot.method.setUrls(urls, pivot_type);
    }

    //check if item is selected
    $scope.pivot.method.isSelected = function(pivot_name, item){
        if ($scope.pivot.data[pivot_name].selected){
            if ($scope.pivot.data[pivot_name].selected.id === item.id){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    };

    // set the selected item for a pivot
    $scope.pivot.method.selected = function(pivot_name, pivot_item){
        $scope.pivot.data[pivot_name].selected = $filter('filter')($scope.pivot.data[pivot_name].items, function (d) {return d.id === pivot_item.id;})[0];
    }

    $scope.pivot.method.get = function(pivot_name, url){
        url = $scope.replaceUrlStringId(url, $scope.pivot.data[pivot_name].parent_id);
        $http.get(window.location.origin + url).then(function (response){
            $scope.pivot.data[pivot_name].items = response.data;
        }, function(response){
            $scope.showErrors(response);
        });

        // $http.get(window.location.origin + url + $scope.pivot.data[pivot_name].parent_id).then(function (response){
        //     $scope.pivot.data[pivot_name].items = response.data;
        // }, function(response){
        //     $scope.showErrors(response);
        // });        
    }

    // url must start with "/"
    $scope.pivot.method.post = function(){
        title = pivot_friendly_name.charAt(0).toUpperCase() + pivot_friendly_name.slice(1);
        $scope.pivot.method.init(pivot_name);

        // $http.post(window.location.origin + url, $scope.pivot[pivot_name]).then(function (response){
        //     console.log(response.data);
        //     $scope.showToast('success', title + 'Added!', title  + ' was successfully created.'); 
        // }, function(response){
        //     $scope.showErrors(response);
        // });
        console.log($scope.pivot);
    }

    // store a single pivot item
    $scope.pivot.method.store = function(pivot_table_name){
        url = $scope.pivot.data[pivot_table_name].urls.store;
        $http.post(window.location.origin + url, $scope.pivot.data[pivot_table_name].new_item).then(function (response){
            $scope.pivot.method.getAllPivot(pivot_table_name);
            $scope.pivot.data[pivot_table_name].new_item = {};
            //$scope.showToast('success', title + 'Added!', title  + ' was successfully created.'); 
        }, function(response){
            $scope.showErrors(response);
        });
    }

    // update a single pivot item
    $scope.pivot.method.update = function(pivot_table_name, pivot_item){
        url = $scope.pivot.data[pivot_table_name].urls.update;
        url = $scope.replaceUrlStringId(url, $scope.pivot.data[pivot_table_name].parent_id);
        if (pivot_table_name ==  'notes'){
            url = '/notes/' + pivot_item.id;            
        }

        $http.put(window.location.origin + url, pivot_item).then(function (response){
            $scope.pivot.method.getAllPivot(pivot_table_name);
            $scope.showToast('success',  'Item Updated!', 'Item was successfully updated.'); 
        }, function(response){
            $scope.showErrors(response);
        });
    }

    // delete a single pivot item
    $scope.pivot.method.delete = function(pivot_table_name, pivot_item){
        url = $scope.pivot.data[pivot_table_name].urls.delete;
        url = $scope.replaceUrlStringId(url, pivot_item.id);

        $http.delete(window.location.origin + url).then(function (response){
            $scope.pivot.method.getAllPivot(pivot_table_name);
            $scope.showToast('success',  'Item Deleted!', 'Item was deleted successfully.'); 
        }, function(response){
            $scope.showErrors(response);
        });
    }

    // link a single pivot item to a parent item
    // $scope.pivot.method.link = function(pivot_table_name){
    //     url = $scope.pivot.data[pivot_table_name].urls.link;

    //     $scope.pivot.data[pivot_table_name].multiple.selected._token = $scope.pivot.data[pivot_table_name]._token;
    //     $scope.pivot.data[pivot_table_name].multiple.selected.user_id = $scope.pivot.data[pivot_table_name].user_id;                

    //     console.log($scope.pivot.data[pivot_table_name].multiple.selected);
    //     $http.post(window.location.origin + url + $scope.item_details.id, $scope.pivot.data[pivot_table_name].multiple.selected).then(function (response){
    //         //intialize again
    //         $scope.pivot.method.getAllPivot(pivot_table_name);
    //         console.log(response);
    //         $scope.showToast('success', 'Items linked!', 'Items were successfully created.'); 
    //     }, function(response){
    //         $scope.showErrors(response);
    //      });
    // }

    // link multiple pivot items to the parent
    $scope.pivot.method.linkMultiple = function(pivot_table_name, pivot_item){
        url = $scope.pivot.data[pivot_table_name].urls.link_multiple;
        url = $scope.replaceUrlStringId(url, $scope.pivot.data[pivot_table_name].parent_id);
        // url = $scope.pivot.data[pivot_table_name].urls.unlink + pivot_table_name + '/' + $scope.pivot.data[pivot_table_name].selected.id;
        // $scope.pivot.data[pivot_table_name].multiple.selected._token = $scope.pivot.data[pivot_table_name]._token;
        // $scope.pivot.data[pivot_table_name].multiple.selected.user_id = $scope.pivot.data[pivot_table_name].user_id;                

        $http.post(window.location.origin + url, pivot_item).then(function (response){
            //intialize again
            $scope.pivot.method.getAllPivot(pivot_table_name);
            console.log(response);
            $scope.showToast('success', 'Items linked!', 'Items were successfully created.'); 
        }, function(response){
            $scope.showErrors(response);
         });
    }


    // unlink a pivot item from the parent
    $scope.pivot.method.unlink = function(pivot_table_name){
        url = $scope.pivot.data[pivot_table_name].urls.unlink + pivot_table_name + '/' + $scope.pivot.data[pivot_table_name].selected.id;
        $scope.pivot.data[pivot_table_name].selected._token = $scope.pivot.data[pivot_table_name]._token;
        $scope.pivot.data[pivot_table_name].selected.user_id = $scope.pivot.data[pivot_table_name].user_id;                

        $http.post(window.location.origin + url, $scope.pivot.data[pivot_table_name].selected).then(function (response){
            //intialize again
            $scope.pivot.method.getAllPivot(pivot_table_name);

            //$scope.showToast('success', title + 'Added!', title  + ' was successfully created.'); 
        }, function(response){
            $scope.showErrors(response);
        });
    }


    // unlink multiple pivot items from the parent
    $scope.pivot.method.multipleUnlink = function(pivot_table_name){
        url = $scope.pivot.data[pivot_table_name].urls.unlink + pivot_table_name + '/' + $scope.pivot.data[pivot_table_name].selected.id;
        $scope.pivot.data[pivot_table_name].selected._token = $scope.pivot.data[pivot_table_name]._token;
        $scope.pivot.data[pivot_table_name].selected.user_id = $scope.pivot.data[pivot_table_name].user_id;                

        $http.post(window.location.origin + url, $scope.pivot.data[pivot_table_name].selected).then(function (response){
            //intialize again
            $scope.pivot.method.getAllPivot(pivot_table_name);

            //$scope.showToast('success', title + 'Added!', title  + ' was successfully created.'); 
        }, function(response){
            $scope.showErrors(response);
        });
    }

   
    // get the unselected list of pivot items, for select2 boxes only
    // pivot_name = the name of the pivot to be used
    $scope.pivot.method.unselectedList = function(pivot_name, url){
        url = $scope.replaceUrlStringId(url, $scope.pivot.data[pivot_name].parent_id);
        $scope.pivot.data[pivot_name].multiple.selected = {};
        $scope.pivot.data[pivot_name].multiple.unselected = {};

        // must refactor this
        // used to reset value
        $timeout(function() {$("#" + pivot_name + "_select").select2("val", "");}, 0);
        
            
        $http.get(window.location.origin + url).then(function (response){
            $scope.pivot.data[pivot_name].multiple.unselected = response.data;
            //$scope.showToast('success', title + 'Added!', title  + ' was successfully created.'); 
        }, function(response){
            $scope.showErrors(response);
        });
    }

    // 
    $scope.pivot.method.setUrls = function(urls, pivot_name){
        angular.forEach(urls, function(value, key) {
            $scope.pivot.data[pivot_name].urls[key] = value;
        });

        //get pivot data
        
        $scope.pivot.method.getAllPivot(pivot_name);
    }

    $scope.pivot.method.getAllPivot = function(pivot_name){
        angular.forEach($scope.pivot.data[pivot_name].urls, function(value, key) {
            switch (key){
                case 'get' :
                    $scope.pivot.method.get(pivot_name, value);
                    break;
                case 'unselected_list' :
                    $scope.pivot.method.unselectedList(pivot_name, value);
                    break;                    
                default :
                    break;
            }
        });        
    }
});

